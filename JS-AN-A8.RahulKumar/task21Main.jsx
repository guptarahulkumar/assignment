import React,{Component} from "react";
import {Route,Switch,Redirect } from"react-router-dom";
import Products from "./products";
import TaskNavBar from "./task2NavBar"
import NewBill from "./task2NewBill";
import NewOrder from "./task2NewOrder";
class Task21Main extends Component{
    state={
        products :		[
            {"code":"PEP221","prod":"Pepsi","price":12,"instock":"Yes","category":"Beverages"},
            {"code":"COK113","prod":"Coca Cola","price":18,"instock":"Yes","category":"Beverages"},
            {"code":"MIR646","prod":"Mirinda","price":15,"instock":"No","category":"Beverages"},
            {"code":"SLI874","prod":"Slice","price":22,"instock":"Yes","category":"Beverages"},
            {"code":"MIN654","prod":"Minute Maid","price":25,"instock":"Yes","category":"Beverages"},
            {"code":"APP652","prod":"Appy","price":10,"instock":"No","category":"Beverages"},
            {"code":"FRO085","prod":"Frooti","price":30,"instock":"Yes","category":"Beverages"},
            {"code":"REA546","prod":"Real","price":24,"instock":"No","category":"Beverages"},
            {"code":"DM5461","prod":"Dairy Milk","price":40,"instock":"Yes","category":"Chocolates"},
            {"code":"KK6546","prod":"Kitkat","price":15,"instock":"Yes","category":"Chocolates"},
            {"code":"PER5436","prod":"Perk","price":8,"instock":"No","category":"Chocolates"},
            {"code":"FST241","prod":"5 Star","price":25,"instock":"Yes","category":"Chocolates"},
            {"code":"NUT553","prod":"Nutties","price":18,"instock":"Yes","category":"Chocolates"},
            {"code":"GEM006","prod":"Gems","price":8,"instock":"No","category":"Chocolates"},
            {"code":"GD2991","prod":"Good Day","price":25,"instock":"Yes","category":"Biscuits"},
            {"code":"PAG542","prod":"Parle G","price":5,"instock":"Yes","category":"Biscuits"},
            {"code":"MON119","prod":"Monaco","price":7,"instock":"No","category":"Biscuits"},
            {"code":"BOU291","prod":"Bourbon","price":22,"instock":"Yes","category":"Biscuits"},
            {"code":"MAR951","prod":"MarieGold","price":15,"instock":"Yes","category":"Biscuits"},
            {"code":"ORE188","prod":"Oreo","price":30,"instock":"No","category":"Biscuits"}
            ],
            json :[],
				filterjson:[],
				 categorys : ['Beverages','Chocolates','Biscuits'],
				 instocks : ['Yes','No'],
				 priceranges : ['<10','10-20','>20'],
				Code:'Code',
				 Product : 'Product',
				 Category:'Category',
				 Price : 'Price',
				 InStock : 'In Stock',
    }
    handleAddOrder=(index1)=>{
        let s1 ={...this.state};
        const pr = s1.products[index1];
			pr.qty=1;
			pr.value=parseInt(pr.qty)*parseInt(pr.price);
		let index = s1.json.findIndex(opt=>{return opt.code===s1.products[index1].code});
		console.log(s1.json[index]);

			if(index>=0){
						let qty =s1.json[index].qty;
						qty++;
						s1.json[index].qty = qty;
						s1.json[index].value = (parseInt(s1.json[index].price)*parseInt(s1.json[index].qty));	
			}
			else{
			s1.json.push(pr);
			}
            this.setState(s1);

    }
    render(){
        const{products,categorys,instocks,priceranges,Code,Category,Price,Product,InStock,json} =this.state;
        return (
            <div className="container">

            <TaskNavBar />
            <NewOrder products={products} categorys={categorys} instocks={instocks} 
                priceranges={priceranges} Code={Code} Price={Price} 
                Product={Product} Category={Category}
                InStock={InStock} json={json}/>
            <Switch>
                <Route path="/task2NewBill" render={(props)=> <NewBill products={products} categorys={categorys} instocks={instocks} 
                priceranges={priceranges} Code={Code} Price={Price} 
                Product={Product} Category={Category}
                InStock={InStock}
                AddOrder={this.handleAddOrder}/>}/>
                <Redirect to="#"/>
                    </Switch>
            </div>
        );
    }
    }
    export default Task21Main;
