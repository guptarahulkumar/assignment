import React,{Component} from "react";
import {Route,Switch,Redirect } from"react-router-dom";
import TaskNavBar from "./task22NavBar"
import TaskNonVeg from "./task22NonVeg";
import TaskVeg from "./task22Veg";
import SideDishes from "./task22SideDishes";
import OtherItem from "./task22OtherItem";
class Task22Main extends Component{
    state={
        items:[
            {"id":"MIR101","image":"https://i.ibb.co/SR1Jzpv/mirinda.png","type":"Beverage","name":"Mirinda","desc":"Mirinda","veg":"Yes"},
            {"id":"PEP001","image":"https://i.ibb.co/3vkKqsF/pepsiblack.png","type":"Beverage","name":"Pepsi Black Can","desc":"Pepsi Black Can","veg":"Yes"},
            {"id":"LIT281","image":"https://i.ibb.co/27PvTng/lit.png","type":"Beverage","name":"Lipton IcedTea","desc":"Lipton Iced Tea","veg":"Yes"},
            {"id":"PEP022","image":"https://i.ibb.co/1M9xDZB/pepsi-new20190312.png","type":"Beverage","name":"Pepsi New","desc":"Pepsi New","veg":"Yes"},
            {"id":"BPCNV1","image":"https://i.ibb.co/R0VSJjq/Burger-Pizza-Non-Veg-nvg.png","type":"Pizza","name":"Classic Non Veg","desc":"Oven-baked buns with cheese, peri-peri chicken, tomato &capsicum in creamy mayo","veg":"No"},
            {"id":"BPCV03","image":"https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel1.png","type":"Pizza","name":"Classic Veg","desc":"Oven-baked buns with cheese, tomato &capsicum in creamy mayo","veg":"Yes"},
            {"id":"BPPV04","image":"https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel2.png","type":"Pizza","name":"Premium Veg","desc":"Oven-baked buns with cheese, paneer,tomato, capsicum & red paprika in creamy mayo","veg":"Yes"},
            {"id":"DIP033","image":"https://i.ibb.co/0mbBzsw/new-cheesy.png","type":"SideDish","name":"Cheesy Dip","desc":"An all-time favorite with your Garlic Breadsticks & Stuffed GarlicBread for a Cheesy indulgence","veg":"Yes"},
            {"id":"DIP072","image":"https://i.ibb.co/fY52zBw/new-jalapeno.png","type":"SideDish","name":"Cheesy Jalapeno Dip","desc":"A spicy, tangy flavored cheese dip is a an absolutedelight with your favourite Garlic Breadsticks","veg":"Yes"},
            {"id":"GAR952","image":"https://i.ibb.co/BNVmfY9/Garlic-bread.png","type":"SideDish","name":"Garlic Breadsticks","desc":"Baked to perfection. Your perfect pizza partner! Tastesbest with dip","veg":"Yes"},
            {"id":"PARCH1","image":"https://i.ibb.co/prBs3NJ/Parcel-Nonveg.png","type":"SideDish","name":"Chicken Parcel","desc":"Snacky bites! Pizza rolls with chicken sausage & creamyharissa sauce","veg":"No"},
            {"id":"PARVG7","image":"https://i.ibb.co/JHhrM7d/Parcel-Veg.png","type":"SideDish","name":"VegParcel","desc":"Snacky bites! Pizza rolls with paneer & creamy harissa sauce","veg":"Yes"},
            {"id":"PATNV7","image":"https://i.ibb.co/0m89Jw9/White-Pasta-Nvg.png","type":"SideDish","name":"White Pasta Italiano Non-Veg","desc":"Creamy white pasta with pepper barbecuechicken","veg":"No"},
            {"id":"PATVG4","image":"https://i.ibb.co/mv8RFbk/White-Pasta-Veg.png","type":"SideDish","name":"White Pasta Italiano Veg","desc":"Creamy white pasta with herb grilledmushrooms","veg":"Yes"},
            {"id":"DES044","image":"https://i.ibb.co/gvpDKPv/Butterscotch.png","type":"Dessert","name":"Butterscotch Mousse Cake","desc":"Sweet temptation! Butterscotch flavored mousse","veg":"Yes"},
            {"id":"DES028","image":"https://i.ibb.co/nm96NZW/ChocoLava.png","type":"Dessert","name":"Choco Lava Cake","desc":"Chocolate lovers delight! Indulgent,gooey molten lava inside chocolate cake","veg":"Yes"},
            {"id":"PIZVDV","image":"https://i.ibb.co/F0H0SWG/deluxeveg.png","type":"Pizza","name":"DeluxeVeggie","desc":"Veg delight - onion, capsicum, grilled mushroom, corn & paneer","veg":"Yes"},
            {"id":"PIZVFH","image":"https://i.ibb.co/4mHxB5x/farmhouse.png","type":"Pizza","name":"Farmhouse","desc":"Delightful combination of onion, capsicum, tomato & grilled mushroom","veg":"Yes"},
            {"id":"PIZVIT","image":"https://i.ibb.co/sRH7Qzf/Indian-TandooriPaneer.png","type":"Pizza","name":"Indi Tandoori Paneer","desc":"It is hot. It is spicy. It is oh-soIndian. Tandoori paneer with capsicum, red paprika & mint mayo","veg":"Yes"},
            {"id":"PIZVMG","image":"https://i.ibb.co/MGcHnDZ/mexgreen.png","type":"Pizza","name":"Mexican Green Wave","desc":"Mexican herbs sprinkled on onion, capsicum, tomato &jalapeno","veg":"Yes"},
            {"id":"PIZVPP","image":"https://i.ibb.co/cb5vLX9/peppypaneer.png","type":"Pizza","name":"PeppyPaneer","desc":"Flavorful trio of juicy paneer, crisp capsicum with spicy red paprika","veg":"Yes"},
            {"id":"PIZVVE","image":"https://i.ibb.co/gTy5DTK/vegextra.png","type":"Pizza","name":"VegExtravaganza","desc":"Black olives, capsicum, onion, grilled mushroom, corn, tomato, jalapeno &extra cheese","veg":"Yes"},
            {"id":"PIZNCP","image":"https://i.ibb.co/b5qBJ9d/cheesepepperoni.png","type":"Pizza","name":"Chicken Pepperoni","desc":"A classic American taste! Relish the delectable flavor of Chicken Pepperoni,topped with extra cheese","veg":"No"},
            {"id":"PIZNCD","image":"https://i.ibb.co/GFtkbB1/ChickenDominator10.png","type":"Pizza","name":"Chicken Dominator","desc":"Loaded with double pepperbarbecue chicken, peri-peri chicken, chicken tikka & grilled chicken rashers","veg":"No"},
            {"id":"PIZNPB","image":"https://i.ibb.co/GxbtcLK/Pepper-Barbeque-OnionC.png","type":"Pizza","name":"Pepper Barbecue & Onion","desc":"A classic favourite with pepperbarbeque chicken & onion","veg":"No"},
            {"id":"PIZNIC","image":"https://i.ibb.co/6Z5wBqr/Indian-Tandoori-ChickenTikka.png","type":"Pizza","name":"Indi Chicken Tikka","desc":"The wholesome flavour of tandoorimasala with Chicken tikka, onion, red paprika & mint mayo","veg":"No"}
            ],
                     sizes : ["Regular","Medium","Large"],
                    crusts : ["New Hand Tossed","Wheat Thin Crust","Cheese Burst","Fresh Pan Pizza","Classic HandTossed"],
                    json:[],
                    form1:[],
    }
 
    render(){
        const{items,sizes,crusts,json,form1} =this.state;
        return (
            <div className="container">
            <TaskNavBar />
           
            <Switch>
                <Route path="/task2Veg" render={(props)=> <TaskVeg PizzaTable={this.PizzaTable} items={items} sizes={sizes} crusts={crusts} json={json} form1={form1}/>}/>
                <Route path="/task2NonVeg" render={(props)=> <TaskNonVeg items={items} sizes={sizes} crusts={crusts} json={json} form1={form1}/>}/>
                <Route path="/task22SideDishes" render={(props)=> <SideDishes items={items} sizes={sizes} crusts={crusts} json={json} form1={form1}/>}/>
                <Route path="/task22OtherItem" render={(props)=> <OtherItem items={items} sizes={sizes} crusts={crusts} json={json} form1={form1}/>}/>
                <Redirect to="#"/>
                    </Switch>
            </div>
        );
    }
    }
    export default Task22Main;
