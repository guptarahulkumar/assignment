import React,{Component} from "react";
class TaskNavBar extends Component{
render(){
    return(
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
           <a className="navbar-brand " href="/#">MyFavPizza</a>
           <button className="navbar-toggler mr-2" type="button" data-toggle="collapse" data-target="#navbarResponsive">
           <span className="navbar-toggler-icon"></span>
           </button>
           <div className="collapse navbar-collapse" id="navbarResponsive">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link" href="/task2Veg">
                           Veg Pizza
                        </a>
                        </li>
                        <li className="nav-item">
                        <a className="nav-link" href="/task2NonVeg">
                           Non-Veg Pizza
                        </a>
                        </li>
                        <li className="nav-item">
                        <a className="nav-link" href="/task22SideDishes">
                           Side Dishes
                        </a>
                        </li>
                        <li className="nav-item">
                        <a className="nav-link" href="/task22OtherItem">
                           Others Item
                        </a>
                        </li>
                </ul>
            </div>
        </nav>
    );
}
}
export default TaskNavBar;