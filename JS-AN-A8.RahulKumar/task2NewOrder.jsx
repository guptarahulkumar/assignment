import React,{Component} from "react";
import {Link} from "react-router-dom";
class NewOrder extends Component{
    state={
         products:this.props.products,
         categorys:this.props.categorys,
         instocks:this.props.instocks,
         priceranges:this.props.priceranges,
         Code:this.props.Code,
         Price:this.props.Price,
         Product:this.props.Product,
         InStock:this.props.InStock,
         Category:this.props.Category,
         json:this.props.json,
         qty:0,
         amount:0,
         btn:false,

    }
    plusOrder=(index)=>{
        //console.log('rahul',index);
        let s1={...this.state};
    (s1.json[index].qty)++;
    //console.log(json);
    s1.json[index].value = (parseInt(s1.json[index].price)*parseInt(s1.json[index].qty));	
this.setState(s1);
}
minusOrder=(index)=>{
    let s1={...this.state};		
    s1.json[index].qty= parseInt(s1.json[index].qty)-1;
    s1.json[index].value = (parseInt(s1.json[index].price)*parseInt(s1.json[index].qty));
    if(s1.json[index].qty==0){
        this.remove(index);
    }else{	
        this.setState(s1);
    }
    this.setState(s1);
}
remove=(index)=>{
    let s1={...this.state};		
    s1.json.splice(index,1);
    this.setState(s1);
}
cancel=()=>{
    let s1={...this.state};
    let txt='';
    if (window.confirm('Bill is cancelled!')) {
          txt = "You pressed OK!";
          s1.json=[];
          this.setState(s1);
        } else {
          txt = "You pressed Cancel!";
          this.setState(s1);
        }   
}

    add=()=>{
        let s1 = {...this.state};
        s1.qty =s1.json.reduce((acc,curr)=>{return acc+curr.qty},0);
        return s1.qty;
    }
    add1=()=>{
        let s1 = {...this.state};
		s1.amount =s1.json.reduce((acc,curr)=>{return acc+curr.value;},0);
        return s1.amount;
    }
    btn=()=>{
        let s1={...this.state};
        if(s1.json.length>0){
            s1.btn=true;
        }
        return s1.btn;
    }
    render(){
        const{products,categorys,instocks,priceranges,Code,Product,Price,InStock,Category,json,qty,amount,btn} =this.state;
      
        return(
            <div className="container">
                <h2 class="mt-5">Details of Current Bill</h2>
                
            
                
                Items :  {json.length} ,
                Quantity : {this.add()},
                Amount :  {this.add1()}<br/>

                {json.map((pr,index)=>{
                    let {code='',prod='',price='',category='',instock='',qty='',value=''}=pr;
                    return(
                    <div className="row border bg-light">
                        <div className="col-1">{code}</div>
                        <div className="col-2">{prod}</div>
                        <div className="col-2">Price : {price}</div>
                        <div className="col-2">Quantity : {qty}</div>
                        <div className="col-2">Value : {value}</div>
                        <div className="col-2"><button className="btn btn-success btn-sm m-1" onClick={()=>this.plusOrder(index)}>+</button>
                        <button className="btn btn-warning btn-sm m-1" onClick={()=>this.minusOrder(index)}>-</button>
                        <button className="btn btn-danger btn-sm m-1"onClick={()=>this.remove(index)}>X</button>
                        </div>
                        </div>
                    );
                })}<br/>
                {this.btn()?
                <button className="btn btn-primary " onClick={()=>this.cancel()}>Cancel</button>:""}
            </div>
        );
    }
}
export default NewOrder;