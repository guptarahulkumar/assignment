import React,{Component} from "react";
import {Link} from "react-router-dom";
class NewBill extends Component{
    state={
         products:this.props.products,
         categorys:this.props.categorys,
         instocks:this.props.instocks,
         priceranges:this.props.priceranges,
         Code:this.props.Code,
         Price:this.props.Price,
         Product:this.props.Product,
         InStock:this.props.InStock,
         Category:this.props.Category,
         str:'',

    }
    showDropdown=(label,arr,name,selArr='')=>{
        return(
            <React.Fragment>

            <select className="form-control form-group"  name={name} onChange={this.handleChange}>
            <option value="">Select {label}</option>
            {arr.map((opt)=>(
              <option>{opt}</option>
            ))}
            </select>
        </React.Fragment>
        );
    }
    sort=(colNo)=>{
     
       let s1 ={...this.state};
       s1.str='';
       s1.Code='Code';
	   s1.Product = 'Product';
	   s1.Category='Category';
	   s1.Price = 'Price';
	   s1.InStock = 'In Stock';
       console.log(s1.str);
        //console.log(str);
        switch(colNo){
            case 0: s1.products.sort((p1,p2)=>p1.code.localeCompare(p2.code));s1.str='(X)';s1.Code=s1.Code+s1.str;break;
            case 1: s1.products.sort((p1,p2)=>p1.prod.localeCompare(p2.prod));s1.str='(X)';s1.Product = s1.Product+s1.str;break;
            case 2: s1.products.sort((p1,p2)=>p1.category.localeCompare(p2.category));s1.str='(X)';s1.Category = s1.Category+s1.str;break;
            case 3: s1.products.sort((p1,p2)=>(+p1.price)-(+p2.price)); s1.str='(X)';s1.Price = s1.Price + s1.str;break;
            case 4: s1.products.sort((p1,p2)=>p1.instock.localeCompare(p2.instock));s1.str='(X)';s1.InStock = s1.InStock+s1.str;break;
        }
        this.setState(s1);
   
}
    render(){
        const{products,categorys,instocks,priceranges,Code,Product,Price,InStock,Category} =this.state;
        const {AddOrder} = this.props;
      
        return(
            <div className="container">
                <h2 className="text-center">Product List</h2>
                <div className="row m-3">
                    Filter Products by : 
                    
                    <div class="col-3">
                        {this.showDropdown('Category',categorys,'Select Category')}
                    </div>
                    <div class="col-3">
                        {this.showDropdown('Instock',instocks,'Select In Stock')}
                    </div>
                    <div class="col-3">
                        {this.showDropdown('PriceRange',priceranges,'Select Price Range')}
                    </div>
                    </div>
                    <div className="row bg-dark text-light">
                        <div className="col-2"  onClick={()=>this.sort(0)}>
                          {Code}
                            </div>
                        <div className="col-2"  onClick={()=>this.sort(1)}>
                            {Product}
                            </div>
                        <div className="col-2"  onClick={()=>this.sort(2)}>
                            {Category}
                            </div>
                            <div className="col-2"  onClick={()=>this.sort(3)}>
                            {Price}
                            </div>
                            <div className="col-2"  onClick={()=>this.sort(4)}>
                            {InStock}
                            </div>
                        <div className="col-2">
                            </div>
                         
                    </div>
                {products.map((opt,index)=>(
                    <div className="row border">
                        <div className="col-2">
                          {opt.code}
                            </div>
                        <div className="col-2">
                            {opt.prod}
                            </div>
                        <div className="col-2">
                            {opt.category}
                            </div>
                            <div className="col-2">
                            {opt.price}
                            </div>
                            <div className="col-2">
                            {opt.instock}
                            </div>
                        <div className="col-2">
                      <button className="btn btn-secondary" onClick={()=>AddOrder(index)}>Add to Bill</button>
                            </div>
                         
                    </div>
                ))}

            </div>
        );
    }
}
export default NewBill;