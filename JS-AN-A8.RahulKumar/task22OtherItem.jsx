import React,{Component} from "react";
class OtherItem extends Component{
    render(){
        const {items,sizes,crusts,json,form1,PizzaTable}=this.props;
        return (
            <React.Fragment>
               <div class="container border">
               <div class="row mt-3">
               <div class="col-8">
               <div class="row ">
               {items.map((opt,index)=>{
                    let {id,image,type,name,desc,veg} = opt;
                    if((type!="Pizza")&&(type!="SideDish")){
                    return(
                    <div class="col-6 border text-center">
                        <img  src={image} className="img-fluid" />
                        <h4 className="text-center">{name}</h4>
                        <p className="text-center">{desc}</p>
                        
                   
                    <button class="btn btn-primary " >Add to Cart</button>
                        </div>
                    );
                    }
               }
               )
            }
    
                   </div>
                   </div>
                   <div class="col-4" >
                   <h2 class="text-center border bg-light">Cart</h2>
                       </div>
                   </div>
                   </div>
            </React.Fragment>
            
        );
    }
}
export default OtherItem;