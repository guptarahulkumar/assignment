import React,{Component} from "react";
class TaskNonVeg extends Component{
    showDropdown=(label,arr,name,selArr='')=>{
        return(
            <React.Fragment>

            <select className="form-control form-group"  name={name} onChange={this.handleChange}>
            <option value="">Select {label}</option>
            {arr.map((opt)=>(
              <option>{opt}</option>
            ))}
            </select>
        </React.Fragment>
        );
    }
    render(){
        const {items,sizes,crusts,json,form1,PizzaTable}=this.props;
        return (
            <React.Fragment>
               <div class="container border">
               <div class="row mt-3">
               <div class="col-8">
               <div class="row ">
               {items.map((opt,index)=>{
                    let {id,image,type,name,desc,veg} = opt;
                    if((type==="Pizza")&&(veg==='No')){
                    return(
                    <div class="col-6 border text-center">
                        <img  src={image} className="img-fluid" />
                        <h4 className="text-center">{name}</h4>
                        <p className="text-center">{desc}</p>
                        {this.showDropdown('size',sizes,'Select Size')}
                    {this.showDropdown('crust',crusts,'Select Size')}
                    <button class="btn btn-primary" onclick={()=>this.addToCart({index},1)}>Add to Cart</button>
                        </div>
                    );
                    }
               }
               )
            }
    
                   </div>
                   </div>
                   <div class="col-4" >
                   <h2 class="text-center border bg-light">Cart</h2>
                       </div>
                   </div>
                   </div>
            </React.Fragment>
            
        );
    }
}
export default TaskNonVeg;