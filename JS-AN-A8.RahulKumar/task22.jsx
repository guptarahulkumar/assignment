import React,{Component} from "react";
class Task22 extends Component{
    state = {
        secA: [{ roll: 1, sec: "A", name: "Jack", maths: 67, eng: 71, comp: 61 },
                        { roll: 2, sec: "A", name: "Mary", maths: 79, eng: 74, comp: 51 },
                        { roll: 3, sec: "A", name: "Steve", maths: 61, eng: 78, comp: 46 },
                        { roll: 4, sec: "A", name: "Bob", maths: 75, eng: 67, comp: 68 },
                        { roll: 5, sec: "A", name: "Kathy", maths: 70, eng: 63, comp: 74 },
                        { roll: 6, sec: "A", name: "Meg", maths: 46, eng: 61, comp: 63 },
                        { roll: 7, sec: "A", name: "Tom", maths: 72, eng: 85, comp: 65 },
                        { roll: 8, sec: "A", name: "David", maths: 85, eng: 71, comp: 85 }
                    ],
        secB: [{ roll: 1, sec: "B", name: "Arthur", maths: 67, eng: 55, comp: 78 },
                       { roll: 2, sec: "B", name: "Kevin", maths: 69, eng: 64, comp: 68 },
                       { roll: 3, sec: "B", name: "Harry", maths: 61, eng: 88, comp: 80 },
                       { roll: 4, sec: "B", name: "Martin", maths: 65, eng: 60, comp: 48 },
                       { roll: 5, sec: "B", name: "Pam", maths: 80, eng: 53, comp: 54 },
                       { roll: 6, sec: "B", name: "Nicky", maths: 76, eng: 71, comp: 83 },
                       { roll: 7, sec: "B", name: "Robert", maths: 82, eng: 65, comp: 75 },
                       { roll: 8, sec: "B", name: "Susan", maths: 65, eng: 81, comp: 50 }
                    ],
        click: -1,
        codeABC123:false,
        code09:false,
        codeMN012:false,
        btn:true,
        
    };
    ABC123=()=>{
        let s1 = {...this.state};
        s1.codeABC123 = true;
        s1.code09 = false;
        s1.codeMN012 = false;
        this.setState(s1);
       }
        C09=()=>{
        let s1 = {...this.state};
        s1.code09 = true;
        s1.codeABC123 = false;
        s1.codeMN012 = false;
        this.setState(s1);
       }
       MN012=()=>{
        let s1 = {...this.state};
        s1.codeMN012 = true;
        s1.codeABC123 = false;
        s1.code09 = false;
        this.setState(s1);
       }
       total=(arr)=>{
           
           return arr.maths+arr.eng+arr.comp;
       }

    render(){
        const{secA,secB,codeABC123,btn,codeMN012,code09,}=this.state;
        return (
        <React.Fragment> 
            <div className="container">
           
            {btn?(
                 <React.Fragment>
                <button className="btn btn-primary m-4" onClick={()=>this.ABC123()}>SecA by Total</button>
                <button className="btn btn-primary m-4" onClick={()=>this.C09()}>SecB by Total</button>
                <button className="btn btn-primary m-4" onClick={()=>this.MN012()}>All Sec by Total</button>
                <button className="btn btn-primary m-4" onClick={()=>this.ABC123()}>SecA by Name</button>
                <button className="btn btn-primary m-4" onClick={()=>this.C09()}>SecB by Name</button>
                <button className="btn btn-primary m-4" onClick={()=>this.MN012()}>All Sec by Name</button>
                </React.Fragment>
             ):""}

                {codeABC123?(
                 <React.Fragment>
                     <h3 className="text-center text-bold text-bold">Performance Report for Section A- Sorted by Total</h3>
                     <div className="row border bg-dark text-light ">
                <div className="col-1 border">Roll No</div>
                <div className="col-2 border">Section</div>
                <div className="col-2 border">Name</div>
                <div className="col-2 border">Maths</div>
                <div className="col-2 border">English</div>
                <div className="col-2 border">Computers</div>
                <div className="col-1 border">Total</div>
            </div>
                     
            {secA.map((opt,index)=>{
                let {roll,sec,name,maths,eng,comp,total=0}=opt;
                return (
                    <div className="row border ">
                <div className="col-1 border bg-light">{roll}</div>
                <div className="col-2 border bg-light">{sec}</div>
                <div className="col-2 border">{name}</div>
                <div className="col-2 border">{maths}</div>
                <div className="col-2 border">{eng}</div>
                <div className="col-2 border">{comp}</div>
                <div className="col-1 border">{this.total(opt)}</div>
            </div>
               );
            })}
                 </React.Fragment>
             ):""}
             {code09?(
                 <React.Fragment>
                     <h3 className="text-center text-bold text-bold">Performance Report for Section B- Sorted by Total</h3>
                     <div className="row border bg-dark text-light ">
                <div className="col-1 border ">Roll No</div>
                <div className="col-2 border ">Section</div>
                <div className="col-2 border">Name</div>
                <div className="col-2 border">Maths</div>
                <div className="col-2 border">English</div>
                <div className="col-2 border">Computers</div>
                <div className="col-1 border">Total</div>
            </div>
                    {secB.map((opt,index)=>{
                let {roll,sec,name,maths,eng,comp,total=0}=opt;
                return (
                    <div className="row border ">
                    <div className="col-1 border bg-light">{roll}</div>
                <div className="col-2 border bg-light">{sec}</div>
                    <div className="col-2 border">{name}</div>
                    <div className="col-2 border">{maths}</div>
                    <div className="col-2 border">{eng}</div>
                    <div className="col-2 border">{comp}</div>
                    <div className="col-1 border">{this.total(opt)}</div>
                </div>
               );
            })}
                 </React.Fragment>
             ):""}
             {codeMN012?(
                 <React.Fragment>
                     <h3 className="text-center text-bold text-bold">Performance Report for All Sections- Sorted by Total</h3>
                     <div className="row border bg-dark text-light ">
                <div className="col-1 border ">Roll No</div>
                <div className="col-2 border ">Section</div>
                <div className="col-2 border">Name</div>
                <div className="col-2 border">Maths</div>
                <div className="col-2 border">English</div>
                <div className="col-2 border">Computers</div>
                <div className="col-1 border">Total</div>
            </div>
                      {secA.map((opt,index)=>{
                let {roll,sec,name,maths,eng,comp,total=0}=opt;
                return (
                    <div className="row border ">
                <div className="col-1 border bg-light">{roll}</div>
                <div className="col-2 border bg-light">{sec}</div>
                <div className="col-2 border">{name}</div>
                <div className="col-2 border">{maths}</div>
                <div className="col-2 border">{eng}</div>
                <div className="col-2 border">{comp}</div>
                <div className="col-1 border">{this.total(opt)}</div>
            </div>
               );
            })}
            {secB.map((opt,index)=>{
                let {roll,sec,name,maths,eng,comp,total=0}=opt;
                return (
                    <div className="row border ">
                <div className="col-1 border bg-light">{roll}</div>
                <div className="col-2 border bg-light">{sec}</div>
                <div className="col-2 border">{name}</div>
                <div className="col-2 border">{maths}</div>
                <div className="col-2 border">{eng}</div>
                <div className="col-2 border">{comp}</div>
                <div className="col-1 border">{this.total(opt)}</div>
            </div>
               );
            })}
                 </React.Fragment>
             ):""}
                
            </div>
            
        </React.Fragment>
        );

    }
}

export default Task22;