import React,{Component} from "react";
import {Route,Switch,Redirect } from"react-router-dom";
import TaskNavBar from "./task3NavBar"
import TaskAll from "./task3All";
import TaskNewDelhi from "./task3NewDelhi";
import TaskNoida from "./task3Noida";
class Task3Main extends Component{
    state={
        design:["Manager","Trainee","President"],
        depart:["Finance","Technology","Operations","HR"],
    }
    showCheckboxes=(label,arr,name,selArr="")=>{
        return (
            <React.Fragment>
                <label className="form-check-label font-weight-bold">
                   {label}
                </label>
                {arr.map((opt,index)=>(
                    <div className="form-check">
                        <input 
                        className="form-check-input"
                        value={opt}
                        type="checkbox"
                        name={name}
                                                          
                        />
                        <label className="form-check-label">{opt}</label>
                    </div>
                ))}
            </React.Fragment>
        );
    }
    showRadios=(label,arr,name,selArr)=>{
        return (
            <React.Fragment>
                <label className="form-check-label font-weight-bold">
                   {label}
                </label>
                {arr.map((opt)=>(
                    <div className="form-check">
                        <input 
                        className="form-check-input"
                        value={opt}
                        type="radio"
                        name={name}
                        checked ={name===opt}
                        onChange={this.handleChange}
                        />
                        <label className="form-check-label">{opt}</label>
                    </div>
                ))}
            </React.Fragment>
        );
    }
    render(){
      const {depart,design}=this.state;
        return (
            <div className="container">
            <TaskNavBar />
            {this.showRadios("Designation",design,"designation")}
                {this.showCheckboxes("Department",depart,"department")}
           
            <Switch>
                <Route path="/task3All" render={(props)=> <TaskAll />}/>
                <Route path="/task3NewDelhi" render={(props)=> <TaskNewDelhi />}/>
                <Route path="/task3Noida" render={(props)=> <TaskNoida />}/>
                <Redirect to="#"/>
                    </Switch>
            </div>
        );
    }
    }
    export default Task3Main;
