import React,{Component} from "react";
class TaskVeg extends Component{
    showDropdown=(label,arr,name,selArr='')=>{
        return(
            <React.Fragment>

            <select className="form-control form-group"  name={name} onChange={this.handleChange}>
            <option value="">Select {label}</option>
            {arr.map((opt)=>(
              <option>{opt}</option>
            ))}
            </select>
        </React.Fragment>
        );
    }
render(){
    const {items,sizes,crusts,json,form1,PizzaTable}=this.props;
    return (
        <React.Fragment>
           <div class="container border">
           <div class="row mt-3">
           <div class="col-8">
           <div class="row ">
           {items.map((opt,index)=>{
				let {id,image,type,name,desc,veg} = opt;
                if((type==="Pizza")&&(veg==='Yes')){
                return(
                <div class="col-6 border text-center">
                    <img  src={image} className="img-fluid" />
                    <h4 className="text-center">{name}</h4>
                    <p className="text-center">{desc}</p>
                    {this.showDropdown('size',sizes,'Select Size')}
                    {this.showDropdown('crust',crusts,'Select Size')}
                    <button class="btn btn-primary " >Add to Cart</button>
                    
                    </div>
                );
                }
           }
           )
        }

               </div>
               </div>
               <div class="col-4" >
               <h2 class="text-center border bg-light">Cart</h2>
               <button class="btn btn-danger btn-sm" onclick="minusOrder('+index+')"><i class="fas fa-minus text-white"></i></button>
                    <button class="btn btn-secondary btn-sm" >1</button>
                    <button class="btn btn-success btn-sm" onclick="plusOrder('+index+')"><i class="fas fa-plus text-white"></i></button>
                   </div>
               </div>
               </div>
        </React.Fragment>
        
    );
}
}
export default TaskVeg;