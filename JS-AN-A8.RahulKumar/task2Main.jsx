import React,{Component} from "react";
class Task2Main extends Component{
state={
    code:[
        {code: "A101", price : 150},
        {code: "A452", price : 450},
        {code: "B671", price : 52},
        {code: "H887", price : 17},
        {code: "V693", price : 188},
        {code: "A645", price : 306},
        {code: "J034", price : 109},
        {code: "N299", price : 75},
        {code: "M472", price : 250},
        {code: "R077", price : 300},
        {code: "B297", price : 150},
        {code: "A489", price : 160},
        {code: "A507", price : 25},
        {code: "K563", price : 45},
        {code: "M833", price : 80},
        {code: "T672", price : 100},
        {code: "B934", price : 200},
       ],
    }
    fun=(str,code)=>{
        let s1 = {...this.state};
        let subStr=parseInt(str.substring(1));
        let res=[];
        if(str[0]==='>')
        res = code.filter((opt)=>(opt.price>subStr));
        else if(str[0]==='<')
        res = code.filter((opt)=>(opt.price<subStr));
        else {
            let index= str.indexOf('-');
            let num1 = parseInt(str.substring(0,index));
            let num2 = parseInt(str.substring(index+1));
            console.log(num1,num2);
        res = code.filter((opt)=>((opt.price>num1)&&(opt.price<num2)));
        }
        return res;
    }
       render(){
           let {res,code} = this.state;
           let s1 = [];
           let str="<160";
            s1 = this.fun(str,code);
         console.log(s1);
         return(
             <React.Fragment>
                 <div className="container">
                     <h3>Filter with given String : {str} </h3>
                 <div className="row bg-dark text-white">
                     <div className="col-4">
                         Code
                         </div>
                         <div className="col-4">
                         Price
                         </div>
                     </div>
             {
             s1.map(opt=>(
                 <div className="row border">
                     <div className="col-4">
                         {opt.code}
                         </div>
                         <div className="col-4">
                         {opt.price}
                         </div>
                     </div>
                     ))
         }
         </div>
         </React.Fragment>
         );
       }
}
export default Task2Main;