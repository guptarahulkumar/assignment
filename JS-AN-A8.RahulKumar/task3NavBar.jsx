import React,{Component} from "react";
class TaskNavBar extends Component{
render(){
    return(
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
           <a className="navbar-brand " href="/#">MyCompany</a>
           <button className="navbar-toggler mr-2" type="button" data-toggle="collapse" data-target="#navbarResponsive">
           <span className="navbar-toggler-icon"></span>
           </button>
           <div className="collapse navbar-collapse" id="navbarResponsive">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link" href="/task3All">
                          All
                        </a>
                        </li>
                        <li className="nav-item">
                        <a className="nav-link" href="/task3NewDelhi">
                           New Delhi
                        </a>
                        </li>
                        <li className="nav-item">
                        <a className="nav-link" href="/task3Noida">
                          Noida
                        </a>
                        </li>
                        
                </ul>
            </div>
        </nav>
    );
}
}
export default TaskNavBar;