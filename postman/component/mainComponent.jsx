import React,{Component} from "react";
import {Route,Switch,Redirect } from"react-router-dom";
import { PersistGate } from "redux-persist/integration/react";

import store, { persistor } from "../store";
import { Provider } from "react-redux";

import Navbar from "./navbar";
import PostHome from "./postHome";
class MainComponent extends Component{
    componentDidMount(){
        console.log("Main Component DId Mount");
    }
    componentDidUpdate(){
        console.log("Main Component Did update");
    }
    componentDidCatch(){
        console.log("Main Component Did catch");
    }
    shouldComponentUpdate(){
        console.log("Main ShouldComponent will update");
        return true;
    }
        render(){
            console.log("Main Component Render");
            return(
                    <Provider store={store}>
                        <PersistGate loading={null} persistor={persistor}>
                            <Navbar  />
                            <PostHome />
                        </PersistGate>
                    </Provider>
            );
        }
}
export default MainComponent;