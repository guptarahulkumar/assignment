import React,{Component} from "react";
import http from "../server/http";
import Params from "./params";
import Response from "./response";
import Header from "./header";
import Body from "./body";
import Auth from "./auth";
import {connect} from  "react-redux";
import{addAction} from '../action';
import queryString from "query-string";
class PostHome extends Component{
    state={
        methods:["GET","POST","PUT","DELETE"],
        objData:{method:"GET",searchUrl:"",body:"",json:{},query:[]},
        paramBtn:true,
        headerBtn:false,
        bodyBtn:false,
        authBtn:false,
        result:"",
        response1:"",
        historyBtn:false,
        ind:-1,
        header:{key:"",value:"",description:"",checked:true},
        headerTable:[
            {key:"Postman-Token",value:"<calculated when request is sent>",description:"",checked:false},
            {key:"Host",value:"<calculated when request is sent>",description:"",checked:false},
            {key:"User-Agent",value:"PostmanRuntime/7.28.4",description:"",checked:false},
            {key:"Accept",value:"*/*",description:"",checked:false},
            {key:"Accept-Encoding",value:"gzip,deflate,br",description:"",checked:false},
            {key:"Connection",value:"keep-alive",description:"",checked:false},
        ],
        params:{key:"",value:"",description:"",checked:true},
        paramsTable:[],
        btn:true,
        queryParams:{},
        keyParams:[],
        valueParams:[],
        checkBtn:true,
    };

    handleHeader=(index,checked,key)=>{
        let s1 ={...this.state};
        console.log(key);
        if(checked===1){
            s1.headerTable[index].checked?
            s1.headerTable[index].checked=false:
            s1.headerTable[index].checked=true;
        }
        else if(checked===0){
            s1.headerTable[index].key=key;
        }
        else if(checked===2){
            s1.headerTable[index].value=key;
        }
        else if(checked===3){
            s1.headerTable[index].description=key;
        }
        s1.objData.json={};
        s1.headerTable.map(p=>{
            if(p.checked===true)
                s1.objData.json[p.key]=p.value;
            }
        );
        this.setState(s1);
    };
    handleParams=(index,checked,key)=>{
        let s1 ={...this.state};
        if(checked===1){
            s1.paramsTable[index].checked?
            s1.paramsTable[index].checked=false:
            s1.paramsTable[index].checked=true;
        }
        else if(checked===0){
            s1.paramsTable[index].key=key;
        }
        else if(checked===2){
            s1.paramsTable[index].value=key;
        }
        else if(checked===3){
            s1.paramsTable[index].description=key; 
        }
        s1.objData.query=[];
        s1.paramsTable.map(p=>{
            if(p.checked===true)
                s1.objData.query.push(p.key+'='+p.value);
        });
        this.setState(s1);
    };
    handleChange = (e)=>{
        const {currentTarget : input}=e;
        let s1 = {...this.state};
        if(input.name==="url"){
            s1.objData.searchUrl = input.value;
            let indexUrl = input.value.indexOf("?",0);
            if(indexUrl>=0){
                let url = new URL(s1.objData.searchUrl);
            s1.queryParams =  queryString.parse(url.search);
            console.log(s1.queryParams);
            s1.keyParams=Object.keys(s1.queryParams);
            s1.valueParams = Object.values(s1.queryParams);
            console.log(s1.valueParams,s1.keyParams);
            }
        }
        else if(input.name==="method"){
            s1.objData.method= input.value;
            //console.log(s1.objData.method);
        }
        else{
            let ugly = input.value;
            s1.objData.body = JSON.parse(ugly);
        }
        s1.historyBtn=false;
        s1.paramsTable=[];
        s1.btn=false;
        s1.checkBtn = true;
        this.setState(s1);
    }
    async postData(url,obj){
        let response = await http.post(url,obj);
        let s1={...this.state};
        if(obj.searchUrl && obj.method){
            //s1.history.unshift(obj);
            this.props.addAction(obj);
        }
        s1.objData={method:obj.method,searchUrl:obj.searchUrl,body:obj.body,json:obj.json};
        s1.response1=response;
        s1.result = JSON.stringify(response.data);
        this.setState(s1);
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        let {objData} = this.state;
        this.postData("/get",objData);
    };
    makeDropdown=(arr,value,name,label)=>(
        <div className="dropdown w-100" key={name}>
            <select
            className = "form-select form-select-sm "
            name={name}
            value={value}
            data-testid="select"
            onChange = {this.handleChange}
            >
                {arr.map(opt=>(
                    <option key={opt}>{opt}</option>
                    ))
                }
            </select>
        </div>
    );
    responseFunction=(str)=>{
        let s1 = {...this.state};
        if(str==="param"){
            s1.paramBtn=true;
            s1.headerBtn=false;
            s1.bodyBtn=false;
            s1.authBtn=false;
        }else 
        if(str==="auth"){
            s1.paramBtn=false;
            s1.headerBtn=false;
            s1.bodyBtn=false;
            s1.authBtn=true;
        }else 
        if(str==="body"){
            s1.paramBtn=false;
            s1.headerBtn=false;
            s1.bodyBtn=true;
            s1.authBtn=false;
        }else
        if(str==="header"){
            s1.paramBtn=false;
            s1.headerBtn=true;
            s1.bodyBtn=false;
            s1.authBtn=false;
        }
        this.setState(s1);
    }
    handleHistory=(index)=>{
        let s1 = {...this.state};
        s1.historyBtn=true;
        s1.ind = index;
        this.setState(s1);
    }
    addHeader=()=>{
        let s1 = {...this.state};
        s1.headerTable.push(s1.header);
        s1.objData.json={};
        s1.headerTable.map(p=>{
            if(p.checked===true)
                s1.objData.json[p.key]=p.value;
        }
        );
        s1.header={key:"",value:"",description:"",checked:true};
        this.setState(s1);
    }
    addParams=()=>{
        let s1 = {...this.state};
        console.log(s1.params);
        s1.paramsTable.push(s1.params);
        s1.params={key:"",value:"",description:"",checked:true};
        s1.btn=true;
        s1.objData.query=[];
        s1.paramsTable.map(p=>{
            if(p.checked===true)
            s1.objData.query.push(p.key+'='+p.value);
        });
        this.setState(s1);
    }
    handleHeaderChange=(e)=>{
        const {currentTarget  : input}=e;
        let s1 = {...this.state};
        s1.header[input.name] = input.value;
        this.setState(s1);
    }
    handleParamsChange=(e)=>{
        const {currentTarget : input}=e;
        let s1 = {...this.state};
        //s1.queryParams[input.name] = input.value;
        s1.params[input.name] = input.value;
        console.log(s1.queryParams);
        this.setState(s1);
    }
    componentDidMount(){
        //console.log("Posthome Component DId Mount");
    }
    componentDidUpdate(){
        //console.log("Posthome Component Did update");
    }
    componentDidCatch(){
        //console.log("Posthome Component Did catch");
    }
    shouldComponentUpdate(){
        //console.log("Posthome ShouldComponent will update");
        return true;
    }
   checkChange = (indexCheck1)=>{
       console.log(indexCheck1);
       let s1 = {...this.state};
        s1.keyParams.map((p,index)=>{
           if(indexCheck1===index){
           s1.paramsTable.push({key:p,value:s1.queryParams[p],checked:false});
           }
           else
           s1.paramsTable.push({key:p,value:s1.queryParams[p],checked:true});
       });
       s1.btn=true;
       s1.queryParams={};
       s1.checkBtn=false;
       this.setState(s1);
   }
    keyChange=(index,key)=>{
        let s1 = {...this.state};
        let ele = s1.keyParams[index];
        console.log(ele);
        let str = JSON.stringify(s1.queryParams);
        
        //console.log(str);
       if(ele.length>=1)
        str = str.replace(ele, key);
        else{
            key='"'+key+'"';
            str = str.replace(/\"\"/s,key);
        }
        console.log(str);

        s1.queryParams = JSON.parse(str);
       // s1.queryParams[ele] = value;
        let url  = new URL(s1.objData.searchUrl);
        //console.log(s1.queryParams,url.host);
        s1.objData.searchUrl=url.protocol+'//'+url.host+url.pathname+'?'+queryString.stringify(s1.queryParams);
        s1.keyParams=Object.keys(s1.queryParams);
        //console.log(s1.objData.searchUrl);
        this.setState(s1);
    }
    valueChange=(index,value)=>{
        let s1 = {...this.state};
        let ele = s1.keyParams[index];
        s1.queryParams[ele] = value;
        let url  = new URL(s1.objData.searchUrl);
        //console.log(s1.queryParams,url.host);
        s1.objData.searchUrl=url.protocol+'//'+url.host+url.pathname+'?'+queryString.stringify(s1.queryParams);
        s1.valueParams=Object.values(s1.queryParams);
        
        //console.log(s1.objData.searchUrl);
        this.setState(s1);
    }
    render(){
        //console.log("PostHome COmponent Render");
        const {checkBtn,keyParams,valueParams,methods,objData,paramBtn,bodyBtn,queryParams,authBtn,headerBtn,result,response1,historyBtn,ind,header,headerTable,params,paramsTable} = this.state;
        const{state} = this.props;
        const{persistedStore} = state;
        const {history} = persistedStore;
        // console.log(this.props.history);
        let len = history.length;
        let arr1=paramsTable.filter((p,index)=>{return p.checked===true});
        let len1=arr1.length;
        let para=arr1.map(p=>p.key+'='+p.value);
        //console.log(queryParams);
        let searchUrl1 = objData.searchUrl;
        if(objData.searchUrl.length>6){
        let url  = new URL(objData.searchUrl);
        //console.log(s1.queryParams,url.host);
        searchUrl1=url.protocol+'//'+url.host+url.pathname;
        }

        return(
        <React.Fragment>
            <hr className ="w-100 m-0" style={{border:"1px solid orange"}}/>
            <div className="container-fluid w-100">
                <div className="row" >
                        <div className="col-sm-3 col-3 d-none d-sm-block">
                            <div className="row bg-dark text-warning text-center " >
                                <h5 title="history">History</h5>
                            </div>
                            {history.length!=0?
                                <React.Fragment>
                                    {history.map((p,index)=>(
                                                <div className={index%2==0?"row border-bottom bg-white ":"row border-bottom bg-light"} style={{paddingLeft:"10px"}} onClick={()=>this.handleHistory(index)} key={index}>
                                                    <div className="col-3 col-sm-2">
                                                        {p.method==="GET"?
                                                            <span className="text-success w-100 " style={{position:"relative",top:"10px"}}>{p.method}</span>
                                                        :p.method==="POST"?
                                                            <span className="text-warning w-100"style={{position:"relative",top:"10px"}}>{p.method}</span>
                                                        :p.method==="PUT"?
                                                            <span className="text-info w-100"style={{position:"relative",top:"10px"}}>{p.method}</span>
                                                        :p.method==="DELETE"?
                                                            <span className="text-danger w-100"style={{position:"relative",top:"10px"}}
                                                            >{p.method.substring(0,3)}</span>
                                                        :""
                                                        } 
                                                    </div>
                                                    <div className="col text-break" >
                                                        {p.searchUrl
                                                        }
                                                    </div>
                                                </div>
                                            ))
                                    }
                                </React.Fragment>
                            :""
                            }    
                        </div>
                        <div className="col-sm-9  col-12 border border-warning">
                            <div className="row bg-dark p-1">
                                <div className="col-3">
                                    {historyBtn?this.makeDropdown(methods,history[ind].method,"method","Methods"):
                                    this.makeDropdown(methods,objData.method,"method","Methods")}
                                </div>
                                <div className="col-6 ">
                                    <div className="form-group w-100">
                                        <input className= "form-control form-control-sm"
                                            type="text"
                                            name="url"
                                            id="url"
                                            placeholder="Enter request URL"
                                            value={historyBtn?history[ind].searchUrl:this.state.btn?(len1>1?(keyParams.length>0?
                                                searchUrl1+'?'+para.join("&"):searchUrl1+'?'+para.join("&")):
                                            (len1>0?(keyParams.length>0?
                                            searchUrl1+'?'+para.join("&"):searchUrl1+'?'+para.join("&")):searchUrl1)): objData.searchUrl}
                                            onChange = {this.handleChange}
                                           //onMouseOver={this.handleChange}
                                        />
                                    </div>  
                                </div>
                                <div className="col-3 ">
                                    <button className="btn btn-warning btn-sm w-100" 
                                    onClick={this.handleSubmit} title="send">Send</button>
                                </div>
                            </div>
                            <div className="row bg-dark mt-1 p-1" >
                                <div className="col-12">
                                    <button style={{marginRight:"6px",marginLeft:"0px",paddingLeft:"5px"}} className={paramBtn?"mt-1 mb-1 ml-1 btn btn-outline-warning btn-sm active":" mt-1 mb-1 ml-1 btn btn-outline-warning btn-sm "}  onClick={()=>this.responseFunction("param")} title="params">Params</button>
                                    <button className={authBtn?"mt-1 mb-1 ml-1 btn btn-outline-warning btn-sm active":" mx-1  btn btn-outline-warning btn-sm "} onClick={()=>this.responseFunction("auth")} title="auth">Authorization</button>
                                    <button className={headerBtn?"mx-1 btn btn-outline-warning btn-sm active":"mx-1   btn btn-outline-warning btn-sm "} onClick={()=>this.responseFunction("header")} title="header">Headers({headerTable.length})</button>
                                    <button className={bodyBtn?"mx-1 btn btn-outline-warning btn-sm active":"mx-1   btn btn-outline-warning btn-sm "} onClick={()=>this.responseFunction("body")} title="body">Body</button>
                               </div>
                            </div>
                            <div className="row mt-2">
                                <div className="col">
                                {paramBtn?<Params checkBtn={checkBtn} checkChange={this.checkChange} keyChange={this.keyChange} valueChange={this.valueChange} valueParams={valueParams} keyParams={keyParams} queryParams={queryParams} params={params} addParams={this.addParams} len={paramsTable.length} paramsTable={paramsTable}  handleParams={this.handleParams} handleParamsChange={this.handleParamsChange}/>:""}
                                {headerBtn?<Header header={header} addHeader={this.addHeader} headerTable={headerTable} handleChange={this.handleChange} handleHeader={this.handleHeader} handleHeaderChange={this.handleHeaderChange}/>:""}
                                {bodyBtn?<Body history={history} historyBtn={historyBtn} ind={ind} objData={objData} handleChange={this.handleChange}/>:""}    
                                {authBtn?<Auth />:""}     
                                </div>                                                           
                            </div>
                            <div className="row mt-5  ">
                                <span className="text-center text-warning  bg-dark" title="response">
                                    Response
                                </span>
                                <br/>
                                    {response1!=""?
                                        (response1.data.error?
                                            (
                                            <span>
                                                <small style={{fontWeight:"bold",fontSize:"12px"}} >Status:</small>
                                                <small className=" m-2 text-danger">404 Not Found</small>
                                            </span>
                                            )
                                            :
                                            response1.data.message?
                                            (
                                            <span>
                                                <small >Status:</small>
                                                <p className=" m-2 text-danger">{response1.data.stack}</p>
                                            </span>
                                            )
                                            :
                                            response1.data==="Not Found"?
                                            (
                                            <span>
                                                <small>Status:</small>
                                                <small className=" m-2 text-danger">404 Not Found</small>
                                            </span>
                                            )
                                            :
                                            <span>
                                                <small>Status:</small>
                                                <small className=" m-2 text-success">{response1.status} OK</small>
                                            </span>
                                        )
                                    
                                    :""
                                    }
                            </div>
                            <div className="row">
                                <Response result={result}/>
                            </div>
                        </div>
                    </div>
            </div>
        </React.Fragment>
        );
        }
    }

    const mapStateToProps=(state)=>{
        //console.log(state.persistedStore.history);
        return {state:state};
      }; 
export default connect(mapStateToProps,{addAction})(PostHome);