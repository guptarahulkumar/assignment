import React,{Component} from "react";
class Body extends Component{
    componentDidMount(){
        console.log("Body Component DId Mount");
    }
    componentDidUpdate(){
        console.log("Body Component Did update");
    }
    componentDidCatch(){
        console.log("Body Component Did catch");
    }
    shouldComponentUpdate(){
        console.log("Body ShouldComponent will update");
        return true;
    }
    render(){
        console.log("Body Component render");
        const {history,historyBtn,ind,handleChange} = this.props;
        return(
           <React.Fragment>
                <h6 data-testid="bodyTitle">Body</h6>
                <p title="para" data-testid="rawTitle">raw - JSON</p>
                <div className="row bg-white">
                    <div className="col-12 text-center"> 
                        {historyBtn?
                            <textarea title="textarea" className="form-control" rows="10" value={JSON.stringify(history[ind].body)}  onChange={handleChange}></textarea>
                            :
                            <textarea title="textarea" className="form-control" rows="10"  onChange={handleChange}></textarea>
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export default Body;