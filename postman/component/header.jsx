import React,{Component} from "react";
class Header extends Component{
    componentDidMount(){
        console.log("Header Component DId Mount");
    }
    componentDidUpdate(){
        console.log("Header Component Did update");
    }
    componentDidCatch(){
        console.log("Header Component Did catch");
    }
    shouldComponentUpdate(){
        console.log("Header ShouldComponent will update");
        return true;
    }
    render(){
        console.log("Header Component render");
        const {handleChange,headerTable,handleHeader,addHeader,handleHeaderChange,header} = this.props;
        return(
            <React.Fragment>
                <h6 title="headers" data-testid="head">Headers</h6>
                <div className="row ">
                    <div className="col-sm-1 col-2 border"></div>
                    <div className="col-sm-3 col-5 border" ><small >KEY</small></div>
                    <div className="col-sm-3 col-5 border"><small >VALUE</small></div>
                    <div className="col border d-none d-sm-block "><small >DESCRIPTION</small></div>
                    <div className="col-1 border text-center d-none d-block " ><small title="...">...</small></div>
                    <div className="col-2 border d-none d-block "><small >Bulk Edit</small></div>
                </div>
                {headerTable.map((p,index)=>(
                    <div className="row  text-center" key={index} data-testid="hMap">
                        <div className="col-sm-1 col-2 border ">  
                            {p.checked & (p.key!="")?
                                <button className="btn btn-sm p-0 m-0" onClick={()=>handleHeader(index,1)}><i className="fas fa-check-square text-primary text-center"></i></button>
                                :
                                <button className="btn btn-sm p-0 m-0" onClick={()=>handleHeader(index,1)}><input type="checkbox" /></button>
                            }
                        </div>
                        <div className="col-sm-3 col-5 border text-muted" >
                            <div className="form-group">
                                <input className= "form-control m-1 p-0"
                                    type="text"
                                    name="key"
                                    id={"key"+index}
                                    placeholder="Key"
                                    value={p.key}
                                    onChange={()=>handleHeader(index,0,document.getElementById("key"+index).value)}
                                />
                            </div>
                        </div>
                        <div className="col-sm-3 col-5 border">
                            <div className="form-group">
                                <input className= "form-control m-1 p-0"
                                    type="text"
                                    name="value"
                                    id={"value"+index}
                                    placeholder="Value"
                                    value={p.value}
                                    onChange={()=>handleHeader(index,2,document.getElementById("value"+index).value)}
                                />
                            </div>
                        </div>
                        <div className="col-5 d-none d-sm-block border">
                            <div className="form-group">
                                <input className= "form-control m-1 p-0"
                                    type="text"
                                    name="description"
                                    id={"description"+index}
                                    value={p.description}
                                    placeholder="Description"
                                    onChange={()=>handleHeader(index,3,document.getElementById("description"+index).value)}
                                />
                            </div>
                        </div>
            
                    </div>
                    ))
                }
                <div className="row ">
                    <div className="col-sm-1 col-2 border"></div>
                    <div className="col-sm-3 col-5 border">
                        <div className="form-group">
                            <input className= "form-control m-1 p-0"
                                type="text"
                                name="key"
                                id="key"
                                placeholder="KeyH"
                                value={header.key}
                                onChange = {handleHeaderChange} 
                            />
                        </div>
                    </div>
                    <div className="col-sm-3 col-5 border">
                        <div className="form-group">
                            <input className= "form-control m-1 p-0"
                                type="text"
                                name="value"
                                id="value"
                                placeholder="ValueH"
                                value={header.value}
                                onChange = {handleHeaderChange} 
                            />
                        </div>
                    </div>
                    <div className="col-5 d-none d-sm-block border">
                        <div className="form-group">
                            <input className= "form-control m-1 p-0"
                                type="text"
                                name="description"
                                id="description"
                                placeholder="DescriptionH"
                                value={header.description}
                                onChange = {handleHeaderChange} 
                            />
                        </div>
                    </div>
        
                </div>
                <div className="row">
                    {header.key==""?""
                        :
                        <button className="btn btn-outline-primary mt-1 p-1" onClick={()=>addHeader()} data-testid="Hadd">Add Header</button>
                    }
                </div>
            </React.Fragment>
        );
    }
}
export default Header;