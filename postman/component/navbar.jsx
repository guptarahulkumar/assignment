import React,{Component,useState} from "react";
import {Link} from "react-router-dom";
import {NavDropdown} from 'react-bootstrap';
import Offcanvas from 'react-bootstrap/Offcanvas';
import OffcanvasHeader from 'react-bootstrap/OffcanvasHeader';
import OffcanvasTitle from 'react-bootstrap/OffcanvasTitle';
import OffcanvasBody from 'react-bootstrap/OffcanvasBody';
import Button from 'react-bootstrap/Button';
import ToggleButtonGroup from 'react-bootstrap/ToggleButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';
import {connect} from  "react-redux";
//import {getHistory} from '../reducer/addHistoryReducer';

function OffCanvasExample({ name,hBtnFun, ...props }) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
      <>
        <Button onClick={handleShow} >
          <span className="navbar-toggler-icon" ></span>
        </Button>
        <Offcanvas show={show} onHide={handleClose} {...props}>
          <Offcanvas.Header closeButton>
            <Offcanvas.Title>History</Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body>
             {/* {console.log(props.history.history)} */}
              {props.history.history.length!=0? 
                    <React.Fragment>
                        {props.history.history.map((p,index)=>(
                            <div className={index%2==0?"row border-bottom bg-white align-text-top":"row border-bottom align-text-top bg-light"} style={{paddingLeft:"10px"}} onClick={()=>handleHistory(index)} key={index}>
                              <div className=" col-2">
                                  {p.method==="GET"?
                                      <span className="text-success" style={{position:"relative",top:"10px"}}>{p.method}</span>
                                  :p.method==="POST"?
                                      <span className="text-warning" style={{position:"relative",top:"10px"}}>{p.method}</span>
                                  :p.method==="PUT"?
                                      <span className="text-info" style={{position:"relative",top:"10px"}}>{p.method}</span>
                                  :p.method==="DELETE"?
                                      <span className="text-danger" style={{position:"relative",top:"10px"}}>{p.method.substring(0,3)}</span>
                                  :""
                                  } 
                              </div>
                              <div className="col-9 text-break" >
                              {p.searchUrl}
                              </div>
                            </div>
                          ))
                        }
                    </React.Fragment>
                    :"No History , Search URL"
                    }  
          </Offcanvas.Body>
        </Offcanvas>
      </>
    );
  }
  function handleHistory(ind){
    return "Rahul";
  }
  function Example(history) {
    return (
      <>
        {['start'].map((placement, idx) => (
          <OffCanvasExample key={idx} placement={placement} name={placement} history={history}/>
        ))}
      </>
    );
  }
class Navbar extends Component{
  componentDidMount(){
    console.log("Navbar Component DId Mount");
}
componentDidUpdate(){
    console.log("Navbar Component Did update");
}
componentDidCatch(){
    console.log("Navbar Component Did catch");
}
shouldComponentUpdate(){
    console.log("Navbar ShouldComponent will update");
    return true;
}
    render(){
      console.log("Navbar Component Render");
      const{state} = this.props;
      const{persistedStore} = state;
      const {history} = persistedStore;
        // console.log(history);
        return(
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
              <h5 className="m-1 text-warning" tittle="Postman">Postman</h5>
              <span className=" d-block d-sm-none " style={{marginRight:"2%"}} >
              <Example history={history}/>
              </span>     
            </nav>
        );
      }                               
} 
const mapStateToProps=(state)=>{
  //console.log(state.persistedStore.history);
  return {state:state};
}; 
export default connect(mapStateToProps)(Navbar);
