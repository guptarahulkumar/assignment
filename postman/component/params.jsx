import React,{Component} from "react";
class Params extends Component{
    componentDidMount(){
        //console.log("Params Component DId Mount");
    }
    componentDidUpdate(){
       // console.log("Params Component Did update");
    }
    componentDidCatch(){
       // console.log("Params Component Did catch");
    }
    shouldComponentUpdate(){
       // console.log("Params ShouldComponent will update");
        return true;
    }
    keyChangeParams=(e)=>{
        const {currentTarget : input}=e;
        console.log(input,input.id,input.value);
        this.props.keyChange(parseInt(input.id),input.value);
    }
    valueChangeParams=(e)=>{
        const {currentTarget : input}=e;
        console.log(input,input.id,input.value);
        this.props.valueChange(parseInt(input.id),input.value);
    }
    render(){
       // console.log("params Component render");
        const {checkBtn,checkChange,paramsTable,handleParams,addParams,handleParamsChange,params,len,queryParams,keyParams,valueParams} = this.props;
        console.log(paramsTable);
        return(
            <React.Fragment>
                <h6 title="query">Query Params</h6>
                <div className="row">
                    <div className="col-sm-1 col-2 border"></div>
                    <div className="col-sm-3 col-5 border" ><small data-testid="keyP1">KEY</small></div>
                    <div className="col-sm-3 col-5 border"><small data-testid="valueP1">VALUE</small></div>
                    <div className="col border d-none d-sm-block "><small data-testid="descP1">DESCRIPTION</small></div>
                    <div className="col-1 border text-center d-none d-block " ><small title="...">...</small></div>
                    <div className="col-2 border d-none d-block "><small data-testid="bulkP1">Bulk Edit</small></div>
                </div>
                {(len>0)?paramsTable.map((p,index)=>(
                    <div className="row  text-center" key={index}  data-testid={"pMap"}>
                        <div className="col-sm-1 col-2 border" >
                            {p.checked & (p.key!="")?
                                <button className="btn btn-sm p-0 m-0" onClick={()=>handleParams(index,1)}><i className="fas fa-check-square text-primary text-center"></i></button>
                                :
                                <button className="btn btn-sm p-0 m-0" onClick={()=>handleParams(index,1)}><input type="checkbox" /></button>
                            }
                        </div>
                        <div className="col-sm-3 col-5 border text-muted" >
                            <div className="form-group w-100">
                                <input className= "form-control m-1 p-0"
                                    type="text"
                                    name="key"
                                    id={"key"+index}
                                    placeholder="Key"
                                    value={p.key}
                                    data-testid={"key"+index}
                                    onChange={()=>handleParams(index,0,document.getElementById("key"+index).value)}
                                />
                            </div>
                        </div>
                        <div className="col-sm-3 col-5 border">
                            <div className="form-group">
                                <input className= "form-control m-1 p-0"
                                    type="text"
                                    name="value"
                                    id={"value"+index}
                                    placeholder="Value"
                                    value={p.value}
                                    data-testid={"value"+index}
                                    onChange={()=>handleParams(index,2,document.getElementById("value"+index).value)}
                                />
                            </div>
                        </div>
                        <div className="col-5 border d-none d-sm-block">
                            <div className="form-group">
                                <input className= "form-control m-1 p-0"
                                    type="text"
                                    name="description"
                                    id={"description"+index}
                                    value={p.description}
                                    data-testid={"desc"+index}
                                    placeholder="Description"
                                    onChange={()=>handleParams(index,3,document.getElementById("description"+index).value)}
                                />
                            </div>
                        </div>
            
                    </div>
                    ))
                    :""
                }
                     {checkBtn & (keyParams!="")?keyParams.map((p,index)=>(
                   <div className="row text-center" key={index}>
                   <div className="col-sm-1 col-2 border">
                   <button className="btn btn-sm p-0 m-0" onClick={()=>checkChange(index)}><i className="fas fa-check-square text-primary text-center"></i></button>
                   </div>
                    <div className="col-sm-3 col-5 border">
                    <div className="form-group">
                        <input className= "form-control m-1 p-0"
                            type="text"
                            name="key"
                            id={index}
                            value={keyParams[index]}
                            placeholder="Key"  
                            data-testid="keyP3"
                            onChange={this.keyChangeParams}
                        />
                    </div>
                </div>
                <div className="col-sm-3 col-5 border">
                <div className="form-group">
                    <input className= "form-control m-1 p-0"
                        type="text"
                        name="value"
                        id={index}
                        value={valueParams[index]}
                        data-testid="value"
                        placeholder="Value"
                        data-testid="valueP3"
                        onChange={this.valueChangeParams}
                    />
                </div>
            </div>
            <div className="col-5 border d-none d-sm-block">
                <div className="form-group">
                    <input className= "form-control m-1 p-0"
                        type="text"
                        name="description"
                        data-testid="description"
                        id="description"
                        placeholder="Description"
                        data-testid="descP3" 
                    />
                </div>
            </div>
        </div>

               )):""}
                <div className="row">
                    <div className="col-sm-1 col-2 border"></div>
                    <div className="col-sm-3 col-5 border">
                        <div className="form-group">
                            <input className= "form-control m-1 p-0"
                                type="text"
                                name="key"
                                id="key"
                                placeholder="Key"  
                                value={params.key}
                                onChange = {handleParamsChange} 
                                data-testid="keyP3"
                            />
                        </div>
                    </div>
                    <div className="col-sm-3 col-5 border">
                        <div className="form-group">
                            <input className= "form-control m-1 p-0"
                                type="text"
                                name="value"
                                id="value"
                                data-testid="value"
                                placeholder="Value"
                                value={params.value}
                                data-testid="valueP3"
                                onChange = {handleParamsChange} 
                            />
                        </div>
                    </div>
                    <div className="col-5 border d-none d-sm-block">
                        <div className="form-group">
                            <input className= "form-control m-1 p-0"
                                type="text"
                                name="description"
                                data-testid="description"
                                id="description"
                                placeholder="Description"
                                value={params.description}
                                data-testid="descP3"
                                onChange = {handleParamsChange} 
                            />
                        </div>
                    </div>
                </div>
                <div className="row">
                    {params.key==""?""
                        :<button className="btn btn-outline-primary mt-1 p-1" onClick={()=>addParams()} data-testid="add">Add Params</button>
                    }
                </div>
            </React.Fragment>
        );
    }
}
export default Params;