import React,{Component} from "react";
class Auth extends Component{
    componentDidMount(){
        console.log("Auth Component DId Mount");
    }
    componentDidUpdate(){
        console.log("Auth Component Did update");
    }
    componentDidCatch(){
        console.log("Auth Component Did catch");
    }
    shouldComponentUpdate(){
        console.log("Auth ShouldComponent will update");
        return true;
    }
    render(){
        console.log("Auth Component render");
        return(
            <React.Fragment>
                 <h6 data-testid="authorization">Authorization</h6>
                 <div className="row bg-white" >
                    <div className="col-sm-10 ml-4 test-center">
                        <p className="text-justify" title="para" data-testid="para">This request is not inheriting any authorization helper at the moment. Save it in a collection to use the parent's authorization helper.</p>
                    </div>
                </div>
          </React.Fragment>
        );
    }
}
export default Auth;