import React,{Component} from "react";
import JSONPretty from 'react-json-pretty';
class Response extends Component{
    componentDidMount(){
        //console.log("Response Component DId Mount");
    }
    componentDidUpdate(){
        //console.log("Response Component Did update");
    }
    componentDidCatch(){
        //console.log("Response Component Did catch");
    }
    shouldComponentUpdate(){
        //console.log("Response ShouldComponent will update");
        return true;
    }
    render(){
        //console.log("Response component Render");
        const {result} = this.props;
        //console.log(result,"rahul");
        return(
            <React.Fragment>
                {result.length>0?
                <React.Fragment>
                    <div className="row w-100">
                <div className="col-sm-8 mt-1 border">
                    <button className="btn btn-sm btn-success ">String Response</button>
                    <div className="row mt-3">
                        <div className="col-sm-12" >
                        <p className="text-justify"  data-testid="result" title="rahul">{result}</p>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4 mt-1 border">
                    <button className="btn btn-sm btn-warning ">JSON Pretty Response</button>
                    <div className="row mt-3">
                        <div className="col-12">
                        <JSONPretty  id="json-pretty" data={result}></JSONPretty>
                        </div>
                    </div>
                </div>
                </div>
           </React.Fragment>
           :
           <div className="row ">
               <div className="col-sm text-center mt-4 ">
               <h6 >{result.stack}</h6>
                   <img
                   className="img-fluid" 
                   style={{borderRadius:"50%"}} 
                   alt="response image"
                   src="https://image.shutterstock.com/image-vector/exclamation-mark-icon-design-illustration-260nw-1497956390.jpg"/>
                   <p title="para" className="text-muted">Enter the URL and click send to get a response</p>
                   </div>
          </div>
    }
          </React.Fragment>
        );

    }
}
export default Response;