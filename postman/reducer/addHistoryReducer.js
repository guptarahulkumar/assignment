import { Add } from "../action/types";
const initialState = {
   history:[],
};
const addHistoryReducer = (state=initialState,action)=>{
  switch(action.type){
    case Add:
      //console.warn("reducer",action);
        return {...state,
                history:[action.payload,...state.history]
        }
        default:
          return state;
        }
};
export default addHistoryReducer;