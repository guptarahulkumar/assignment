import React from 'react';
import { 
  render, 
  screen, 
  waitFor ,
  fireEvent,
} from '@testing-library/react';
import "@testing-library/jest-dom";
import Body from '../component/body';

 const mockedHandleChange = jest.fn()

describe("Body Button",()=>{
    test('body, raw Json, textField renders with correct title', async() => {
     render(<Body 
        history={[]} 
        handleChange={mockedHandleChange} 
         />);
         const headEle = screen.getByRole('heading');
        const paraEle = screen.getByTitle("para");
        const textFieldEle = screen.getByTitle("textarea");
        expect(headEle).toBeInTheDocument();
        expect(paraEle).toBeInTheDocument();
     });

    });
