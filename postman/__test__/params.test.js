import React from 'react';
import { 
  render, 
  screen, 
  waitFor ,
  fireEvent,
} from '@testing-library/react';
import "@testing-library/jest-dom";
import Params from '../component/params';

const mockedAddParams=jest.fn()
const mockedHandleParams=jest.fn()
const mockedHandleParamsChange=jest.fn()

describe("Params Button",()=>{
    test('Key, Value, Description and bulk edit renders with correct title', async() => {
     render(<Params 
        params={{}} 
        addParams={mockedAddParams} 
        keyParams = {[]}
        paramsTable={[]} 
        handleParams={mockedHandleParams} 
        handleParamsChange={mockedHandleParamsChange}
         />);
     const queryEle = screen.getByText(/Query Params/i);
    const keyEle = screen.getByText(/KEY/i);
    const valueEle = screen.getByText(/VALUE/i);
    const descEle = screen.getByText(/DESCRIPTION/i);
    const bulkEle = screen.getByText(/BULK EDIT/i);
    expect(queryEle).toBeInTheDocument();
    expect(keyEle).toBeInTheDocument();
    expect(valueEle).toBeInTheDocument();
    expect(descEle).toBeInTheDocument();
    expect(bulkEle).toBeInTheDocument();
    });

    test('key, value, description should be change the value ', async() => {
        render(<Params 
           params={{}} 
           addParams={mockedAddParams} 
           paramsTable={[]} 
           keyParams = {[]}
           handleParams={mockedHandleParams} 
           handleParamsChange={mockedHandleParamsChange}
            />);
        const keyEle = screen.getByPlaceholderText(/Key/i);
        const valueEle = screen.getByPlaceholderText(/Value/i);
        const descEle = screen.getByPlaceholderText(/Description/i);
        fireEvent.change(keyEle,{target:{value:"product"}})
        expect(keyEle.value).toBe("product");
          const addBtnEle = screen.getByTestId("add");
          /*fireEvent.click(addBtnEle);
              const keyEle2 =await screen.findByTestId("key0");
              const valueEle2 =await screen.findByTestId("value0");
              const descEle2 =await screen.findByTestId("desc0");
              expect(keyEle2).toBeInTheDocument();
              expect(valueEle2).toBeInTheDocument();
              expect(descEle2).toBeInTheDocument();
              */
       fireEvent.change(valueEle,{target:{value:"25"}})
       expect(valueEle.value).toBe("25");
       fireEvent.change(descEle,{target:{value:"nothing"}})
       expect(descEle.value).toBe("nothing");
       
       });
    
});