import React from 'react';
import { 
  render, 
  screen, 
  waitFor ,
  fireEvent,
} from '@testing-library/react';
import "@testing-library/jest-dom";
import Header from '../component/header';

const mockedAddHeader=jest.fn()
const mockedHandleHeader=jest.fn()
const mockedHandleChange=jest.fn()
const mockedHandleHeaderChange=jest.fn()

describe("Header Button",()=>{
    test('Key, Value, Description and bulk edit renders with correct title', async() => {
     render(<Header 
        header={{}} 
        addHeader={mockedAddHeader} 
        headerTable={[]} 
        handleChange={mockedHandleChange} 
        handleHeader={mockedHandleHeader} 
        handleHeaderChange={mockedHandleHeaderChange}
         />);
         const headTextEle = screen.getByText(/Headers/i);
         const keyEle = screen.getByText(/KEY/i);
         const valueEle = screen.getByText(/VALUE/i);
         const descEle = screen.getByText(/DESCRIPTION/i);
         const bulkEle = screen.getByText(/BULK EDIT/i);
         expect(headTextEle).toBeInTheDocument();
         expect(keyEle).toBeInTheDocument();
         expect(valueEle).toBeInTheDocument();
         expect(descEle).toBeInTheDocument();
         expect(bulkEle).toBeInTheDocument();
     });
     test('key, value, description should be change the value ', async() => {
        render(<Header 
            header={{}} 
            addHeader={mockedAddHeader} 
            headerTable={[]} 
            handleChange={mockedHandleChange} 
            handleHeader={mockedHandleHeader} 
            handleHeaderChange={mockedHandleHeaderChange}
            />);
        const keyEle = screen.getByPlaceholderText(/Key/i);
        const valueEle = screen.getByPlaceholderText(/Value/i);
        const descEle = screen.getByPlaceholderText(/Description/i);
        fireEvent.change(keyEle,{target:{value:"authorization"}})
       expect(keyEle.value).toBe("authorization");
       fireEvent.change(valueEle,{target:{value:"124569878"}})
       expect(valueEle.value).toBe("124569878");
       fireEvent.change(descEle,{target:{value:"nothing"}})
       expect(descEle.value).toBe("nothing");
       });
       test('key, value, description should be change the value ', async() => {
        render(<Header 
            header={{}} 
            addHeader={mockedAddHeader} 
            headerTable={[]} 
            handleChange={mockedHandleChange} 
            handleHeader={mockedHandleHeader} 
            handleHeaderChange={mockedHandleHeaderChange}
            />);
        const keyEle = screen.getByPlaceholderText(/Key/i);
        const valueEle = screen.getByPlaceholderText(/Value/i);
        const descEle = screen.getByPlaceholderText(/Description/i);
        fireEvent.change(keyEle,{target:{value:"authorization"}})
       expect(keyEle.value).toBe("authorization");
       fireEvent.change(valueEle,{target:{value:"124569878"}})
       expect(valueEle.value).toBe("124569878");
       fireEvent.change(descEle,{target:{value:"nothing"}})
       expect(descEle.value).toBe("nothing");
       });
      
    
    });
          