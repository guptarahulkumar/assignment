import React from 'react';
import PostHome from '../component/postHome';
import Response from '../component/response';
import {Provider} from "react-redux";
import JSONPretty from 'react-json-pretty';
import { 
  render as rtlRender, 
  screen, 
  waitFor ,
  fireEvent
} from '@testing-library/react';
import "@testing-library/jest-dom/extend-expect"
import store from '../store';
const render=component=>rtlRender(
  <Provider store={store}>{component}</Provider>
)

describe("PostHome - should be change the value of search URL & Dropdown & send button",()=>{
  
  it('change value of Select GET works correctly', async() => {
    render(<PostHome />);
    const hostEle = screen.getByDisplayValue('GET');
    expect(hostEle).toBeInTheDocument();
    expect(await screen.findByText(/Query Params/i)).toBeInTheDocument();
    const selectElement = screen.getByTestId("select");
    fireEvent.change(selectElement,{target:{value:"GET"}});
    expect(selectElement.value).toEqual("GET");
    const inputElement = screen.getByPlaceholderText(/Enter request URL/i);
    fireEvent.change(inputElement,{target:{value:"https://jsonplaceholder.typicode.com/posts/9"}});
    expect(inputElement.value).toEqual("https://jsonplaceholder.typicode.com/posts/9");

    //Query arams
    const paramsElement = screen.getByRole("button",{name:"params"});
    fireEvent.click(paramsElement);
    expect(paramsElement.textContent).toBe("Params");
    const queryEle1 = screen.getByTitle("query");
    expect(queryEle1.textContent).toBe("Query Params");
    const keyEle = screen.getByText(/KEY/i);
    const valueEle = screen.getByText(/VALUE/i);
    const descEle = screen.getByText(/DESCRIPTION/i);
    const bulkEle = screen.getByText(/BULK EDIT/i);
    expect(keyEle).toBeInTheDocument();
    expect(valueEle).toBeInTheDocument();
    expect(descEle).toBeInTheDocument();
    expect(bulkEle).toBeInTheDocument();
    const keyFieldEle = screen.getByPlaceholderText(/Key/i);
    const valueFieldEle = screen.getByPlaceholderText(/Value/i);
    const descFieldEle = screen.getByPlaceholderText(/Description/i);
    fireEvent.change(keyFieldEle,{target:{value:"product"}})
    expect(keyFieldEle.value).toBe("product");
    fireEvent.change(valueFieldEle,{target:{value:"25"}})
    expect(valueFieldEle.value).toBe("25");
    fireEvent.change(descFieldEle,{target:{value:"nothing"}})
    expect(descFieldEle.value).toBe("nothing");
    const addBtnFieldEle = screen.getByTestId("add");
    expect(descFieldEle.value).toBe("nothing");
    fireEvent.click(addBtnFieldEle);
    const checkEle = screen.getAllByTestId("pMap");
    expect(checkEle.length).toBe(1);
    const key0Ele = screen.getByTestId("key0");
    expect( key0Ele.value).toEqual("product");
    const value0Ele = screen.getByTestId("value0");
    expect( value0Ele.value).toEqual("25");
    const desc0Ele = screen.getByTestId("desc0");
    expect( desc0Ele.value).toEqual("nothing");
  
    // Headers
    const headerElement = screen.getByTitle("header");
    fireEvent.click(headerElement);
    expect(headerElement.textContent).toEqual("Headers(6)");
    const headTextEle = screen.getByTestId("head");
    expect(headTextEle.textContent).toEqual("Headers");
    const keyHEle = screen.getByText(/KEY/i);
    const valueHEle = screen.getByText(/VALUE/i);
    const descHEle = screen.getByText(/DESCRIPTION/i);
    const bulkHEle = screen.getByText(/BULK EDIT/i);
    expect(keyHEle).toBeInTheDocument();
    expect(valueHEle).toBeInTheDocument();
    expect(descHEle).toBeInTheDocument();
    expect(bulkHEle).toBeInTheDocument();
    const keyFieldHEle = screen.getByPlaceholderText(/KeyH/i);
    const valueFieldHEle = screen.getByPlaceholderText(/ValueH/i);
    const descFieldHEle = screen.getByPlaceholderText(/DescriptionH/i);
    const checkHeaderEle = screen.getAllByTestId("hMap");
    expect(checkHeaderEle.length).toBe(6);
    fireEvent.change(keyFieldHEle,{target:{value:"authorization"}})
    expect(keyFieldHEle.value).toBe("authorization");
    fireEvent.change(valueFieldHEle,{target:{value:"1640573604969"}})
    expect(valueFieldHEle.value).toBe("1640573604969");
    fireEvent.change(descFieldHEle,{target:{value:"nothing"}})
    expect(descFieldHEle.value).toBe("nothing");
    const addBtnFieldHEle = screen.getByTestId("Hadd");
    fireEvent.click(addBtnFieldHEle);
    expect(headerElement.textContent).toEqual("Headers(7)");
    console.log(checkHeaderEle);
   
    
    
    //Authorization
    const authElement = screen.getByRole("button",{name:"auth"});
    fireEvent.click(authElement);
    expect(authElement.textContent).toEqual("Authorization");
    const authTitleEle = screen.getByTestId("authorization");
    expect(authTitleEle.textContent).toEqual("Authorization");
    const paraEle = screen.getByTestId("para");
    expect(paraEle.textContent).toEqual("This request is not inheriting any authorization helper at the moment. Save it in a collection to use the parent's authorization helper.");

    //Body JSON
    const bodyJSON = JSON.stringify({
      "userId": 1000,
      "title": "Rahul",
      "body": "Rahul"
    });

    // Body
    const bodyElement = screen.getByTitle("body");
    fireEvent.click(bodyElement);
    expect(bodyElement.textContent).toBe("Body");
    const bodyTitleEle = screen.getByTestId("bodyTitle");
    expect(bodyTitleEle.textContent).toBe("Body");
    const rawTitleEle = screen.getByTestId("rawTitle");
    expect(rawTitleEle.textContent).toBe("raw - JSON");
    const bodyTextEle = screen.getByTitle("textarea");
    fireEvent.change(bodyTextEle,{target:{value:bodyJSON}})
    expect(bodyTextEle.value).toBe(bodyJSON);

    //Response JSON
    const responseJSON = JSON.stringify({
      "userId": 1,
      "id": 9,
      "title": "nesciunt iure omnis dolorem tempora et accusantium",
      "body": "consectetur animi nesciunt iure dolore\nenim quia ad\nveniam autem ut quam aut nobis\net est aut quod aut provident voluptas autem voluptas"
    });

    //Response
   const sendElement = screen.getByRole("button",{name:/Send/i});
   expect(sendElement.textContent).toEqual("Send");
   fireEvent.click(sendElement);
   const responseEle = screen.findByTestId("result");
   const response1 = (await responseEle).textContent;
  //console.log(response1);
   expect(response1).toEqual(responseJSON);     
 
});

// Post
it('change value of Select POST works correctly', async() => {
  render(<PostHome />);
  const hostEle = screen.getByDisplayValue('GET');
  expect(hostEle).toBeInTheDocument();
  expect(await screen.findByText(/Query Params/i)).toBeInTheDocument();
  const selectElement = screen.getByTestId("select");
  fireEvent.change(selectElement,{target:{value:"POST"}});
  expect(selectElement.value).toEqual("POST");
  const inputElement = screen.getByPlaceholderText(/Enter request URL/i);
  fireEvent.change(inputElement,{target:{value:"https://jsonplaceholder.typicode.com/posts"}});
  expect(inputElement.value).toEqual("https://jsonplaceholder.typicode.com/posts");

  //Query Params
  const paramsElement = screen.getByRole("button",{name:"params"});
  fireEvent.click(paramsElement);
  expect(paramsElement.textContent).toBe("Params");
  const queryEle1 = screen.getByTitle("query");
  expect(queryEle1.textContent).toBe("Query Params");
  const keyEle = screen.getByText(/KEY/i);
  const valueEle = screen.getByText(/VALUE/i);
  const descEle = screen.getByText(/DESCRIPTION/i);
  const bulkEle = screen.getByText(/BULK EDIT/i);
  expect(keyEle).toBeInTheDocument();
  expect(valueEle).toBeInTheDocument();
  expect(descEle).toBeInTheDocument();
  expect(bulkEle).toBeInTheDocument();
  const keyFieldEle = screen.getByPlaceholderText(/Key/i);
  const valueFieldEle = screen.getByPlaceholderText(/Value/i);
  const descFieldEle = screen.getByPlaceholderText(/Description/i);
  fireEvent.change(keyFieldEle,{target:{value:"product"}})
  expect(keyFieldEle.value).toBe("product");
  fireEvent.change(valueFieldEle,{target:{value:"25"}})
  expect(valueFieldEle.value).toBe("25");
  fireEvent.change(descFieldEle,{target:{value:"nothing"}})
  expect(descFieldEle.value).toBe("nothing");
  const addBtnFieldEle = screen.getByTestId("add");
  expect(descFieldEle.value).toBe("nothing");
  fireEvent.click(addBtnFieldEle);
  const checkEle = screen.getAllByTestId("pMap");
  expect(checkEle.length).toBe(1);
  const key0Ele = screen.getByTestId("key0");
  expect( key0Ele.value).toEqual("product");
  const value0Ele = screen.getByTestId("value0");
  expect( value0Ele.value).toEqual("25");
  const desc0Ele = screen.getByTestId("desc0");
  expect( desc0Ele.value).toEqual("nothing");

  // Headers
  const headerElement = screen.getByTitle("header");
  fireEvent.click(headerElement);
  expect(headerElement.textContent).toEqual("Headers(6)");
  const headTextEle = screen.getByTestId("head");
  expect(headTextEle.textContent).toEqual("Headers");
  const keyHEle = screen.getByText(/KEY/i);
  const valueHEle = screen.getByText(/VALUE/i);
  const descHEle = screen.getByText(/DESCRIPTION/i);
  const bulkHEle = screen.getByText(/BULK EDIT/i);
  expect(keyHEle).toBeInTheDocument();
  expect(valueHEle).toBeInTheDocument();
  expect(descHEle).toBeInTheDocument();
  expect(bulkHEle).toBeInTheDocument();
  const keyFieldHEle = screen.getByPlaceholderText(/KeyH/i);
  const valueFieldHEle = screen.getByPlaceholderText(/ValueH/i);
  const descFieldHEle = screen.getByPlaceholderText(/DescriptionH/i);
  const checkHeaderEle = screen.getAllByTestId("hMap");
  expect(checkHeaderEle.length).toBe(6);
  fireEvent.change(keyFieldHEle,{target:{value:"authorization"}})
  expect(keyFieldHEle.value).toBe("authorization");
  fireEvent.change(valueFieldHEle,{target:{value:"1640320100264"}})
  expect(valueFieldHEle.value).toBe("1640320100264");
  fireEvent.change(descFieldHEle,{target:{value:"nothing"}})
  expect(descFieldHEle.value).toBe("nothing");
  const addBtnFieldHEle = screen.getByTestId("Hadd");
  fireEvent.click(addBtnFieldHEle);
  expect(headerElement.textContent).toEqual("Headers(7)");
  console.log(checkHeaderEle);
 
  
  
  //Authorization
  const authElement = screen.getByRole("button",{name:"auth"});
  fireEvent.click(authElement);
  expect(authElement.textContent).toEqual("Authorization");
  const authTitleEle = screen.getByTestId("authorization");
  expect(authTitleEle.textContent).toEqual("Authorization");
  const paraEle = screen.getByTestId("para");
  expect(paraEle.textContent).toEqual("This request is not inheriting any authorization helper at the moment. Save it in a collection to use the parent's authorization helper.");

  //Body JSON
  const bodyJSON = JSON.stringify({
    "userId": 1000,
    "title": "Rahul",
    "body": "Rahul"
  });

  // Body
  const bodyElement = screen.getByTitle("body");
  fireEvent.click(bodyElement);
  expect(bodyElement.textContent).toBe("Body");
  const bodyTitleEle = screen.getByTestId("bodyTitle");
  expect(bodyTitleEle.textContent).toBe("Body");
  const rawTitleEle = screen.getByTestId("rawTitle");
  expect(rawTitleEle.textContent).toBe("raw - JSON");
  const bodyTextEle = screen.getByTitle("textarea");
  fireEvent.change(bodyTextEle,{target:{value:bodyJSON}})
  expect(bodyTextEle.value).toBe(bodyJSON);

  //Response JSON
  const responseJSON = JSON.stringify({
    "userId": 1000,
    "title": "Rahul",
    "body": "Rahul",
    "id":101
  });

  //Response
 const sendElement = screen.getByRole("button",{name:/Send/i});
 expect(sendElement.textContent).toEqual("Send");
 fireEvent.click(sendElement);
 const responseEle = screen.findByTestId("result");
 const response1 = (await responseEle).textContent;
//console.log(response1);
 expect(response1).toEqual(responseJSON);     

});

//PUT
it('change value of Select PUT works correctly', async() => {
  render(<PostHome />);
  const hostEle = screen.getByDisplayValue('GET');
  expect(hostEle).toBeInTheDocument();
  expect(await screen.findByText(/Query Params/i)).toBeInTheDocument();
  const selectElement = screen.getByTestId("select");
  fireEvent.change(selectElement,{target:{value:"PUT"}});
  expect(selectElement.value).toEqual("PUT");
  const inputElement = screen.getByPlaceholderText(/Enter request URL/i);
  fireEvent.change(inputElement,{target:{value:"https://jsonplaceholder.typicode.com/posts/6"}});
  expect(inputElement.value).toEqual("https://jsonplaceholder.typicode.com/posts/6");

  //Query arams
  const paramsElement = screen.getByRole("button",{name:"params"});
  fireEvent.click(paramsElement);
  expect(paramsElement.textContent).toBe("Params");
  const queryEle1 = screen.getByTitle("query");
  expect(queryEle1.textContent).toBe("Query Params");
  const keyEle = screen.getByText(/KEY/i);
  const valueEle = screen.getByText(/VALUE/i);
  const descEle = screen.getByText(/DESCRIPTION/i);
  const bulkEle = screen.getByText(/BULK EDIT/i);
  expect(keyEle).toBeInTheDocument();
  expect(valueEle).toBeInTheDocument();
  expect(descEle).toBeInTheDocument();
  expect(bulkEle).toBeInTheDocument();
  const keyFieldEle = screen.getByPlaceholderText(/Key/i);
  const valueFieldEle = screen.getByPlaceholderText(/Value/i);
  const descFieldEle = screen.getByPlaceholderText(/Description/i);
  fireEvent.change(keyFieldEle,{target:{value:"product"}})
  expect(keyFieldEle.value).toBe("product");
  fireEvent.change(valueFieldEle,{target:{value:"25"}})
  expect(valueFieldEle.value).toBe("25");
  fireEvent.change(descFieldEle,{target:{value:"nothing"}})
  expect(descFieldEle.value).toBe("nothing");
  const addBtnFieldEle = screen.getByTestId("add");
  expect(descFieldEle.value).toBe("nothing");
  fireEvent.click(addBtnFieldEle);
  const checkEle = screen.getAllByTestId("pMap");
  expect(checkEle.length).toBe(1);
  const key0Ele = screen.getByTestId("key0");
  expect( key0Ele.value).toEqual("product");
  const value0Ele = screen.getByTestId("value0");
  expect( value0Ele.value).toEqual("25");
  const desc0Ele = screen.getByTestId("desc0");
  expect( desc0Ele.value).toEqual("nothing");

  // Headers
  const headerElement = screen.getByTitle("header");
  fireEvent.click(headerElement);
  expect(headerElement.textContent).toEqual("Headers(6)");
  const headTextEle = screen.getByTestId("head");
  expect(headTextEle.textContent).toEqual("Headers");
  const keyHEle = screen.getByText(/KEY/i);
  const valueHEle = screen.getByText(/VALUE/i);
  const descHEle = screen.getByText(/DESCRIPTION/i);
  const bulkHEle = screen.getByText(/BULK EDIT/i);
  expect(keyHEle).toBeInTheDocument();
  expect(valueHEle).toBeInTheDocument();
  expect(descHEle).toBeInTheDocument();
  expect(bulkHEle).toBeInTheDocument();
  const keyFieldHEle = screen.getByPlaceholderText(/KeyH/i);
  const valueFieldHEle = screen.getByPlaceholderText(/ValueH/i);
  const descFieldHEle = screen.getByPlaceholderText(/DescriptionH/i);
  const checkHeaderEle = screen.getAllByTestId("hMap");
  expect(checkHeaderEle.length).toBe(6);
  fireEvent.change(keyFieldHEle,{target:{value:"authorization"}})
  expect(keyFieldHEle.value).toBe("authorization");
  fireEvent.change(valueFieldHEle,{target:{value:"1640320100264"}})
  expect(valueFieldHEle.value).toBe("1640320100264");
  fireEvent.change(descFieldHEle,{target:{value:"nothing"}})
  expect(descFieldHEle.value).toBe("nothing");
  const addBtnFieldHEle = screen.getByTestId("Hadd");
  fireEvent.click(addBtnFieldHEle);
  expect(headerElement.textContent).toEqual("Headers(7)");
  console.log(checkHeaderEle);
 
  
  
  //Authorization
  const authElement = screen.getByRole("button",{name:"auth"});
  fireEvent.click(authElement);
  expect(authElement.textContent).toEqual("Authorization");
  const authTitleEle = screen.getByTestId("authorization");
  expect(authTitleEle.textContent).toEqual("Authorization");
  const paraEle = screen.getByTestId("para");
  expect(paraEle.textContent).toEqual("This request is not inheriting any authorization helper at the moment. Save it in a collection to use the parent's authorization helper.");

  //Body JSON
  const bodyJSON = JSON.stringify({
    "userId": 1000,
    "title": "Rahul Gupta",
    "body": "Rahul Gupta"
  });

  // Body
  const bodyElement = screen.getByTitle("body");
  fireEvent.click(bodyElement);
  expect(bodyElement.textContent).toBe("Body");
  const bodyTitleEle = screen.getByTestId("bodyTitle");
  expect(bodyTitleEle.textContent).toBe("Body");
  const rawTitleEle = screen.getByTestId("rawTitle");
  expect(rawTitleEle.textContent).toBe("raw - JSON");
  const bodyTextEle = screen.getByTitle("textarea");
  fireEvent.change(bodyTextEle,{target:{value:bodyJSON}})
  expect(bodyTextEle.value).toBe(bodyJSON);

  //Response JSON
  const responseJSON = JSON.stringify({
    "userId": 1000,
    "title": "Rahul Gupta",
    "body": "Rahul Gupta",
    "id":6
  });

  //Response
 const sendElement = screen.getByRole("button",{name:/Send/i});
 expect(sendElement.textContent).toEqual("Send");
 fireEvent.click(sendElement);
 const responseEle = screen.findByTestId("result");
 const response1 = (await responseEle).textContent;
//console.log(response1);
 expect(response1).toEqual(responseJSON);     

});

//DELETE
it('change value of Select DELETE works correctly', async() => {
  render(<PostHome />);
  const hostEle = screen.getByDisplayValue('GET');
  expect(hostEle).toBeInTheDocument();
  expect(await screen.findByText(/Query Params/i)).toBeInTheDocument();
  const selectElement = screen.getByTestId("select");
  fireEvent.change(selectElement,{target:{value:"DELETE"}});
  expect(selectElement.value).toEqual("DELETE");
  const inputElement = screen.getByPlaceholderText(/Enter request URL/i);
  fireEvent.change(inputElement,{target:{value:"https://jsonplaceholder.typicode.com/posts/5"}});
  expect(inputElement.value).toEqual("https://jsonplaceholder.typicode.com/posts/5");

  //Query arams
  /*const paramsElement = screen.getByRole("button",{name:"params"});
  fireEvent.click(paramsElement);
  expect(paramsElement.textContent).toBe("Params");
  const queryEle1 = screen.getByTitle("query");
  expect(queryEle1.textContent).toBe("Query Params");
  const keyEle = screen.getByText(/KEY/i);
  const valueEle = screen.getByText(/VALUE/i);
  const descEle = screen.getByText(/DESCRIPTION/i);
  const bulkEle = screen.getByText(/BULK EDIT/i);
  expect(keyEle).toBeInTheDocument();
  expect(valueEle).toBeInTheDocument();
  expect(descEle).toBeInTheDocument();
  expect(bulkEle).toBeInTheDocument();
  const keyFieldEle = screen.getByPlaceholderText(/Key/i);
  const valueFieldEle = screen.getByPlaceholderText(/Value/i);
  const descFieldEle = screen.getByPlaceholderText(/Description/i);
  fireEvent.change(keyFieldEle,{target:{value:"product"}})
  expect(keyFieldEle.value).toBe("product");
  fireEvent.change(valueFieldEle,{target:{value:"25"}})
  expect(valueFieldEle.value).toBe("25");
  fireEvent.change(descFieldEle,{target:{value:"nothing"}})
  expect(descFieldEle.value).toBe("nothing");
  const addBtnFieldEle = screen.getByTestId("add");
  expect(descFieldEle.value).toBe("nothing");
  fireEvent.click(addBtnFieldEle);
  const checkEle = screen.getAllByTestId("pMap");
  expect(checkEle.length).toBe(1);
  const key0Ele = screen.getByTestId("key0");
  expect( key0Ele.value).toEqual("product");
  const value0Ele = screen.getByTestId("value0");
  expect( value0Ele.value).toEqual("25");
  const desc0Ele = screen.getByTestId("desc0");
  expect( desc0Ele.value).toEqual("nothing");
*/
  // Headers
  const headerElement = screen.getByTitle("header");
  fireEvent.click(headerElement);
  expect(headerElement.textContent).toEqual("Headers(6)");
  const headTextEle = screen.getByTestId("head");
  expect(headTextEle.textContent).toEqual("Headers");
  const keyHEle = screen.getByText(/KEY/i);
  const valueHEle = screen.getByText(/VALUE/i);
  const descHEle = screen.getByText(/DESCRIPTION/i);
  const bulkHEle = screen.getByText(/BULK EDIT/i);
  expect(keyHEle).toBeInTheDocument();
  expect(valueHEle).toBeInTheDocument();
  expect(descHEle).toBeInTheDocument();
  expect(bulkHEle).toBeInTheDocument();
  const keyFieldHEle = screen.getByPlaceholderText(/KeyH/i);
  const valueFieldHEle = screen.getByPlaceholderText(/ValueH/i);
  const descFieldHEle = screen.getByPlaceholderText(/DescriptionH/i);
  const checkHeaderEle = screen.getAllByTestId("hMap");
  expect(checkHeaderEle.length).toBe(6);
  fireEvent.change(keyFieldHEle,{target:{value:"authorization"}})
  expect(keyFieldHEle.value).toBe("authorization");
  fireEvent.change(valueFieldHEle,{target:{value:"1640320100264"}})
  expect(valueFieldHEle.value).toBe("1640320100264");
  fireEvent.change(descFieldHEle,{target:{value:"nothing"}})
  expect(descFieldHEle.value).toBe("nothing");
  const addBtnFieldHEle = screen.getByTestId("Hadd");
  fireEvent.click(addBtnFieldHEle);
  expect(headerElement.textContent).toEqual("Headers(7)");
  console.log(checkHeaderEle);
 
  
  
  //Authorization
  const authElement = screen.getByRole("button",{name:"auth"});
  fireEvent.click(authElement);
  expect(authElement.textContent).toEqual("Authorization");
  const authTitleEle = screen.getByTestId("authorization");
  expect(authTitleEle.textContent).toEqual("Authorization");
  const paraEle = screen.getByTestId("para");
  expect(paraEle.textContent).toEqual("This request is not inheriting any authorization helper at the moment. Save it in a collection to use the parent's authorization helper.");

  //Body JSON
  /*const bodyJSON = JSON.stringify({
    "userId": 1000,
    "title": "Rahul Gupta",
    "body": "Rahul Gupta"
  });

  // Body
  const bodyElement = screen.getByTitle("body");
  fireEvent.click(bodyElement);
  expect(bodyElement.textContent).toBe("Body");
  const bodyTitleEle = screen.getByTestId("bodyTitle");
  expect(bodyTitleEle.textContent).toBe("Body");
  const rawTitleEle = screen.getByTestId("rawTitle");
  expect(rawTitleEle.textContent).toBe("raw - JSON");
  const bodyTextEle = screen.getByTitle("textarea");
  fireEvent.change(bodyTextEle,{target:{value:bodyJSON}})
  expect(bodyTextEle.value).toBe(bodyJSON);

  //Response JSON
  const responseJSON = JSON.stringify({
    "userId": 1000,
    "id":5,
    "title": "Rahul Gupta",
    "body": "Rahul Gupta"
  });
  */

  //Response
 const sendElement = screen.getByRole("button",{name:/Send/i});
 expect(sendElement.textContent).toEqual("Send");
 fireEvent.click(sendElement);
 const responseEle = screen.findByTestId("result");
 const response1 = (await responseEle).textContent;
//console.log(response1);
 expect(response1).toEqual("{}");     

});
});
  

  describe("PostHome - Response should be corect title ",()=>{
    it('response renders with correct title', async() => {
     render(<PostHome />);
    const navElement = screen.getByTitle("response");
    expect(navElement).toBeInTheDocument();
    });
    });

