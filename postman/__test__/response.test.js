import React from 'react';
import { 
  render, 
  screen, 
  waitFor ,
  fireEvent,
} from '@testing-library/react';
import "@testing-library/jest-dom";
import Response from '../component/response';

  
describe("Response ",()=>{
    test('response message renders with correct title', async() => {
     render(<Response 
     result={[]}/>);
        const paraEle = screen.getByTitle('para');
        const altEle = screen.getByAltText(/response image/i);
       
        expect(paraEle).toBeInTheDocument();
        expect(altEle).toBeInTheDocument();
     });

    });
