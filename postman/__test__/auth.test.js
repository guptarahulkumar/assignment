import React from 'react';
import { 
  render, 
  screen, 
  waitFor ,
  fireEvent,
} from '@testing-library/react';
import "@testing-library/jest-dom";
import Auth from '../component/auth';

describe("Authorization Button",()=>{
    test('Authorization and text renders with correct title', async() => {
        render(<Auth />);
        const headEle = screen.getByRole('heading');
        const paraEle = screen.getByTitle("para");
        expect(headEle).toBeInTheDocument();
        expect(paraEle).toBeInTheDocument();
    })

});