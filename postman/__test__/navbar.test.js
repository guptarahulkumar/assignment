import React from 'react';
import Navbar from '../component/navbar';
import {Provider} from "react-redux";

import { 
  render as rtlRender, 
  screen, 
  waitFor 
} from '@testing-library/react';
import "@testing-library/jest-dom/extend-expect"
import store from '../store';
const render=component=>rtlRender(
  <Provider store={store}>{component}</Provider>
)
describe('Navbar Header - Postman',()=>{
test('postman renders with correct title', async() => {
  render(<Navbar title="Postman"/>);
  const navElement = screen.getByRole("heading");
  expect(navElement).toBeInTheDocument();
})
});