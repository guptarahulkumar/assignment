var express = require("express");
var app = express();
let axios = require("axios");
app.use(express.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  next();
});
const port = 2450;
app.listen(port, () => console.log(`Listening on ${port}!`));
const {getData}= require("../model/server")
//console.log(getData);
app.post("/get", getData);
  
  
