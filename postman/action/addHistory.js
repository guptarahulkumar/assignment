import { Add } from "./types";

export const addAction=(obj)=>(dispatch)=>
    {
    dispatch({type:Add,payload:obj});
};
