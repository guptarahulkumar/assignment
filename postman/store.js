import { createStore,applyMiddleware, combineReducers } from "redux";
import { addHistoryReducer } from "./reducer";
import thunk from 'redux-thunk';
import { persist } from "./services/reduxPersist";

import { persistStore } from "redux-persist";

const MIGRATION_DEBUG = false;

const persistConfig = {
  key: "persistedStore",
  version: 1,
};

const reducers = combineReducers({
  persistedStore: persist(persistConfig, addHistoryReducer),
});

const store = createStore(
  reducers,
  applyMiddleware(thunk),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export const persistor = persistStore(store);

export default store;
