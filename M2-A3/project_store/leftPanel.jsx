import React,{Component} from "react";
import {Link} from "react-router-dom";
class LeftPanel extends Component{
    handleChange = (e)=>{
        const {currentTarget  : input}=e;
        let options = {...this.props.options};
        if(input.name === "course")
        options[input.name]=this.updateCBs(
            options[input.name],
            input.checked,
            input.value
        );
        else options[input.name] = input.value;
        console.log("LeftPanel",options);
        this.props.onOptionChange(options);
    }
    updateCBs=(inpValues,checked,value)=>{
        let inpArr = inpValues ? inpValues.split(","):[];
        if(checked) inpArr.push(value);
        else{
            let index = inpArr.findIndex((ele)=>ele === value);
            if(index>=0) inpArr.splice(index,1);
        }
        console.log(inpValues,inpArr);
        return inpArr.join(",");
    }
    makeCheckboxes=(arr,values,name,label)=>(
        <React.Fragment>
               <div className="row mx-1">
            {arr.map((opt)=>(
                <div className="col-12 btn-outline-secondary border px-4 pt-3 pb-3" key={opt}>
                    {opt!="All"?
               <Link  style={{textDecoration:"none",color:'black'}} to={`/products?category=${opt}`}>{opt}</Link>:
               <Link style={{textDecoration:"none",color:'black'}} to={`/products`}>{opt}</Link>}
                </div>
            ))}
    </div>
    </React.Fragment>
    );
    render(){
        let {course=""}=this.props.options;
        let {courses}=this.props;
        return(
            <div className="row">
                <div className="col-9">
                {this.makeCheckboxes(courses,course.split(","),"course","Course")}
                </div>
            </div>
        );
    }
};
export default LeftPanel;
