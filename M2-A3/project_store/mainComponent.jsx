import React,{Component} from "react";
import {Route,Switch,Redirect } from"react-router-dom";
import Navbar from "./navbar";
import auth from "./auth";
import Products from "./products";
import Cart from "./cart";
import Login from "./login";
import Logout from "./logout";
import CheckOut from "./checkOut";
import OrderBook from "./orderBook";
import Orders from "./orders";
import ProductsList from "./productsList";
import AddProducts from "./addProducts";
import EditProduct from "./editProduct";
import DeleteProduct from "./deleteProduct";
class MainComponent extends Component{
    state={
        cart:[],
    }
        handleCartIncr=(index)=>{
        let s1= {...this.state};
        s1.cart[index].qty++;
        this.setState(s1);
        
     } 
     handleCartDecr=(index)=>{
        let s1= {...this.state};
        s1.cart[index].qty>1?
        s1.cart[index].qty--:s1.cart.splice(index,1);;
         this.setState(s1);
     } 
    handleCartChange=(data)=>{
       let s1= {...this.state};
       let find=s1.cart.find(p=>p.id===data.id);
       if(!find){
           data.qty=1;
       s1.cart.push(data);
       }
       this.setState(s1);
       
    } 
    handleCart1Change=(data)=>{
        let s1= {...this.state};
        let find=s1.cart.findIndex(p=>p.id===data.id);
        if(find>=0)
        s1.cart.splice(find,1);
        this.setState(s1);
        
     } 
    render(){
        const user = auth.getUser();
        return(
            <div className="container-fluid">
                <Navbar user={user} cart={this.state.cart} />
                <Switch>
                <Route path="/products/:id/edit"
                    render={(props) => 
                        user?
                        ( <EditProduct {...props} />):

                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    /> 
                    <Route path="/products/:id/delete"
                    render={(props) => 
                        user?
                        ( <DeleteProduct {...props} cart={this.state.cart}/>):

                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    /> 
                <Route path="/products"
                    render={(props) => 
                        <Products {...props} cart={this.state.cart} onCartIncr={this.handleCartIncr} onCartDecr={this.handleCartDecr}  onCartChange={this.handleCartChange} onCart1Change={this.handleCart1Change} />
                        }
                    /> 
                     <Route path="/login"
                    render={(props) => 
                        <Login {...props}  />
                        }
                    /> 
                    <Route path="/cart"
                    render={(props) => 
                        <Cart {...props} cart={this.state.cart} onCartIncr={this.handleCartIncr} onCartDecr={this.handleCartDecr} />
                        }
                    /> 
                    <Route path="/addProducts"
                    render={(props) => 
                        user?
                        ( <AddProducts {...props} />):

                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    /> 
                    <Route path="/checkOut"
                    render={(props) => 
                        user?
                        ( <CheckOut {...props} cart={this.state.cart}/>):

                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    /> 
                      
                     <Route path="/orders"
                    render={(props) => 
                        user?
                        ( <Orders {...props} />):

                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    /> 
                   

<Route path="/productsList"
                    render={(props) => 
                        user?
                        ( <ProductsList {...props} />):

                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    /> 

<Route path="/orderBook"
                    render={(props) => 
                        user?
                        ( <OrderBook {...props} cart={this.state.cart}/>):

                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    /> 

                     
                     <Route path="/logout"
                    render={(props) => 
                        <Logout {...props}  />
                        }
                    /> 

                <Redirect from="/" to="/login"/>
                </Switch>
            </div> 
        );
       
    }
}
export default MainComponent;