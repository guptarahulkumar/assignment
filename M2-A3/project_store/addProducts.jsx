import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class AddProducts extends Component{
    state={
        user:{category:"",description:"",imgLink:"",name:"",price:""},
        errors:{},
        categories:["Sunglasses","Watches","Belts","Handbags","Wallets","Formal Shoes","Sport Shoes","Floaters","Sandals"],
    };
    
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        s1.user[input.name] = input.value;
        console.log(s1.user);
        this.setState(s1);
    };
    async postData(url,obj){
        let response = await http.post(url,obj);
        console.log(response);
        this.props.history.push("/products");
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        let {user} = this.state;
        alert("User Added Successfully");
        this.postData("/register",user);
    };

    render(){
      let {category,description,imgLink,name,price}=this.state.user;
      const {errors} = this.state;
      return (
            <div className="container">
               <div className="form-group mt-3">
                   <label>Name</label>
                   <input 
                   type="text"
                   className="form-control"
                   id="name"
                   name="name"
                   value ={name}
                   placeholder="Name.."
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group mt-3">
                   <label>Description</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="description"
                   id="description"
                   value ={description}
                   placeholder="Description.."
                   onChange={this.handleChange}
                   />
                   {errors && errors.description && (<span className="mt-2 text-center text-danger">{errors.description}</span>)}
               </div>
               <div className="form-group mt-3">
                   <label>Price</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="price"
                   id="price"
                   value ={price}
                   placeholder="Price.."
                   onChange={this.handleChange}
                   />
                   {errors && errors.price && (<span className="mt-2 text-center text-danger">{errors.price}</span>)}
               </div>
               <div className="form-group mt-3">
                   <label>Image</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="imgLink"
                   id="imgLink"
                   placeholder="Image url.."
                   value ={imgLink}
                   onChange={this.handleChange}
                   />
                  
               </div>
               <div  className="form-group mt-3 ">
                    <select className="form-control" name ="category" id="category" value={category}  onChange={this.handleChange}>
                        <option selected >Select Category</option>
                        {this.state.categories.map((pr)=>(
                         <option >{pr}</option>
                        ))}
                    </select>
                </div>
                      
               <button className="btn mt-4 btn-primary" onClick={this.handleSubmit}>Add</button>
                
            </div>
        );

    }
}
export default AddProducts;