import React,{Component} from "react";
import {Link} from "react-router-dom";
import {NavDropdown} from 'react-bootstrap';
class Navbar extends Component{
    render(){
       const {user,cart} = this.props;
       console.log(cart);
       return(
        <nav className="navbar navbar-expand-sm navbar-dark bg-secondary">
            <Link className="navbar-brand text-light " to="/products">
              <h5 className="m-2">MyStore</h5>
            </Link>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link className="nav-link" to="/products?category=Watches">
                         Watches
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/products?category=Sunglasses">
                        Sunglasses
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/products?category=Belts">
                       Belts
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/products?category=Handbags">
                        Handbags
                        </Link>
                    </li>
                    <li className="nav-item dropdown">
                            <NavDropdown title="Footwear" id="navbarScrollingDropdown">
                <NavDropdown.Item ><Link className="nav-link text-dark" to='/products?category=Sandals'>Sandals</Link></NavDropdown.Item>
                <NavDropdown.Item  ><Link className="nav-link text-dark" to='/products?category=Sandals'>Formal Shoes</Link></NavDropdown.Item>
                <NavDropdown.Item  ><Link className="nav-link text-dark" to='/products?category=Sandals'>Sport Shoes</Link></NavDropdown.Item>
                <NavDropdown.Item  ><Link className="nav-link text-dark" to='/products?category=Sandals'>Floters</Link></NavDropdown.Item>
               </NavDropdown>
                            </li>
                    
                    {!user && (                            
                            <li className="nav-item">
                                <Link className="nav-link" to="/login">
                                <h6 style={{position:"absolute",right:"100px"}}>Login</h6>
                                </Link>
                            </li>
                            )}
                            {user && (
                             <li style={{position:"absolute",right:"100px"}} className="nav-item dropdown">
                            <NavDropdown title={user.email} id="navbarScrollingDropdown">
                
                <NavDropdown.Item  ><Link className="nav-link text-dark" to='/orders'>My Orders</Link></NavDropdown.Item>
                <NavDropdown.Item  ><Link className="nav-link text-dark" to='/productsList'>Manage Products</Link></NavDropdown.Item>
                <NavDropdown.Item  ><Link className="nav-link text-dark" to='/logout'>Logout</Link></NavDropdown.Item>
               </NavDropdown>
                            </li>)}
                            <li className="nav-item">
                                <Link className="nav-link" to="/cart">
                                <h6 style={{position:"absolute",right:"50px"}}>Cart</h6>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <button className="btn btn-success text-light nav-link rounded" style={{position:"absolute",right:"20px"}}>{cart.length}</button>
                            </li>
                </ul>
            </div>

        </nav>
    );
}
           
                        
        
}
export default Navbar;