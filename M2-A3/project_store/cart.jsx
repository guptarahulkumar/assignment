import React,{Component} from "react";
import {Link} from "react-router-dom";
class Cart extends Component{
    incr=(index)=>{
        this.props.onCartIncr(index);
    
    }
    decr=(index)=>{
        this.props.onCartDecr(index);
    }
    render(){
        const {cart} = this.props;
        return(
            <div className="container">
               <div className="row">
                   <div className="col">
                        <img className="" height="200px" width="100%" src="https://raw.githubusercontent.com/edufectcode/react/main/data/MyStore-sale.jpg"/>
                        <br/>
                        <h4 className="" style={{textAlign:"center"}}>You have {cart.length} items in Your Cart</h4>
                        <p  style={{float:"left"}}> 
                        <b>Cart Value : Rs. 
                            {cart.length>0||
                        cart.qty?cart.reduce((acc,curr)=>{return acc+(parseInt(curr.price)*parseInt(curr.qty))},0)+".00":
                        cart.reduce((acc,curr)=>{return acc+(parseInt(curr.price))},0)+".00"}
                        
                        </b></p>
                        <button className="btn btn-primary" style={{float:"right"}}>
                        <Link style={{textDecoration:"none",color:"white"}} to="/checkOut">Check Out</Link></button>
                    
                        </div>
                        <div className="row bg-dark text-light">
                            <div className="col-2"></div>
                            <div className="col-6">Product Details</div>
                            <div className="col-2">Quantity</div>
                            <div className="col-2">Price</div>

                        </div>
                        {cart.map((p,index)=>(
                            <div className="row mt-1">
                            <div className="col-2"><img className="img-fluid rounded"  src={p.imgLink} /></div>
                            <div className="col-6">
                                {p.name}<br/>
                                {p.category}<br/>
                                {p.description}
                            </div>
                            <div className="col-2">
                                <button className="btn btn-success" onClick={()=>this.incr(index)}>+</button>
                               <span className="m-3">{p.qty}</span>
                                <button className="btn btn-warning" onClick={()=>this.decr(index)}>-</button>
                            </div>

                            <div className="col-2">Rs. {p.qty?p.price*p.qty:p.price}</div>

                        </div>
                        ))}

               </div>
               </div>
        );
        }

}
export default Cart;