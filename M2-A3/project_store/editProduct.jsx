import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class EditProduct extends Component{
    state={
        user:{category:"",description:"",imgLink:"",imgLink1:"",name:"",price:""},
        categories:["Sunglasses","Watches","Belts","Handbags","Wallets","Formal Shoes","Sport Shoes","Floaters","Sandals"],
        imgBtn1:true,
    };
    async fetchData(){
        const {id} = this.props.match.params;
        let response =await http.get(`/Rproducts/${id}`);
        console.log(response);
        let {data}=response;
        this.setState({user:data});
    }
     componentDidMount(){
    this.fetchData();      
   }
   async putData(url,obj){
    let response = await http.put(url,obj);
    console.log(response);
    this.props.history.push("/productsList");
}
   
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        s1.user[input.name] = input.value;
        console.log(s1.user);
        this.setState(s1);
    };
    deleteProduct=()=>{
        const {id} = this.props.match.params;
        this.props.history.push(`/products/${id}/delete`);
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        let {user} = this.state;
        const {id} = this.props.match.params;
        alert("User Added Successfully");
        this.putData(`/products/${id}`,user);
    };
    image=()=>{
        this.setState({imgBtn1:false});
    }
    image1=()=>{
        this.setState({imgBtn1:true});
    }

    render(){
      let {category="",description="",imgLink="",imgLink1="",name="",price="",}=this.state.user;
      const {imgBtn1} = this.state;
      return (
            <div className="container">
                <h2 className="text-center">Edit Product</h2>
                <div className="row">
                <div className="col-4 mx-5">
                <div className="card p-2 m-2" style={{width: "15rem"}}>
                    {imgBtn1 && imgLink1?
  <img src={imgLink1} className="card-img-top" alt="..." onMouseOver={()=>this.image()}/>:
  <img src={imgLink} className="card-img-top" alt="..." onMouseOver={()=>this.image1()}/>}
  <div className="card-body">
      <h4>{name}</h4>
      <p>Rs.{price}</p>
    <p className="card-text">{description.substring(0,20)+"..."}</p>
    
  </div>
                </div>
                </div>
                <div className="col-6">
               <div className="form-group mt-3">
                   <label>Name</label>
                   <input 
                   type="text"
                   className="form-control"
                   id="name"
                   name="name"
                   value ={name}
                   placeholder="Name.."
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group mt-3">
                   <label>Description</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="description"
                   id="description"
                   value ={description}
                   placeholder="Description.."
                   onChange={this.handleChange}
                   />
                   
               </div>
               <div className="form-group mt-3">
                   <label>Price</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="price"
                   id="price"
                   value ={price}
                   placeholder="Price.."
                   onChange={this.handleChange}
                   />
                 
               </div>
               <div className="form-group mt-3">
                   <label>Image1</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="imgLink"
                   id="imgLink"
                   placeholder="Image url.."
                   value ={imgLink}
                   onChange={this.handleChange}
                   />
                  
               </div>
               <div className="form-group mt-3">
                   <label>Image2</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="imgLink1"
                   id="imgLink1"
                   placeholder="Image url.."
                   value ={imgLink1}
                   onChange={this.handleChange}
                   />
                  
               </div>
               <div  className="form-group mt-3 ">
                    <select className="form-control" name ="category" id="category" value={category}  onChange={this.handleChange}>
                        <option selected disabled>Select Category</option>
                        {this.state.categories.map((pr)=>(
                         <option >{pr}</option>
                        ))}
                    </select>
                </div>
                      
               <button className="btn mt-4 btn-primary" onClick={this.handleSubmit}>Save</button>
               <button className="btn mt-4 border-warning" onClick={()=>this.deleteProduct()}>Delete</button>
               </div>
               
               </div>
            </div>
        );

    }
}
export default EditProduct;