import React,{Component} from "react";
import http from "./http";
class DeleteProduct extends Component{
    async componentDidMount(){
        const {id} = this.props.match.params;
        let response = await http.deleteApi(`/products/${id}`);
        this.props.history.push("/productsList");
    }
    render(){
        console.log("Rahul");
        return "";
    }
}
export default DeleteProduct;