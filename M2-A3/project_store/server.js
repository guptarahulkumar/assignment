let express = require("express");
let app=express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header(
        "Access-Control-Allow-Methods",
        "GET,POST,OPTIONS,PUT,PATCH,DELETE,HEAD"
    );
    res.header(
        "Access-Control-Allow-Headers",
        "Origin,X-Requested-With,Content-Type,Accept"
    );
    next();
});
const port = 2410;
app.listen(port,()=>console.log(`Node app listening on port ${port}! `));

let {data} = require("./data.js");
let customers = [{email:"email@test.com",password:"12345678"}];
let orders=[];

app.get("/products",function(req,res){
    res.send(data);
});
app.get("/products/:category",function(req,res){
    let category = req.params.category;
    let product  =data.filter((st)=>st.category===category);
    console.log(product,category);
   res.send(product);
});
app.get("/Rproducts/:id",function(req,res){
    let id = req.params.id;
    console.log(data);
    let product =data.find((s)=>id==s.id);
    console.log(product,id);
   res.send(product);
});

app.post("/register",function(req,res){
    let newid= data.reduce((acc,curr)=>{return acc<curr.id?curr.id:acc},0);
    newid= parseInt(newid)+1;
    console.log(req.body);
    data.push({id:newid,...req.body});
    console.log(data);
    res.send(data);
});
    app.post("/login", function(req, res) {
    //find that customer
    console.log(req.body);
    let custObj = customers.find(
      item => item.email === req.body.email && item.password === req.body.password
    );
    console.log(custObj);
    let resObj = null;
    if (custObj != undefined) {
      resObj = {
        name: custObj.name,
        email: custObj.email,
        role: custObj.role
      };
      res.status(200).send(resObj);
    } else res.status(500).send("Login Unsuccessful");
  });
  app.post("/orders", function(req, res) {
    //find that customer
    console.log(req.body);
    orders.push(req.body);
    console.log(orders);
  res.status(200).send("Order Submitted"); 
  });
  app.get("/orders",function(req,res){
      console.log(orders);
      res.send(orders);
  })
app.put("/products/:id",function(req,res){
    let id= req.params.id;
    let body = req.body;
    console.log(body);
    let index = data.findIndex((st)=>st.id==id);
    if(index>=0){
    let updatedcar = {id:id,...body};
    data[index]=updatedcar;
    res.send(updatedcar);
    }else
    res.status(404).send("No Product found");

});
app.delete("/products/:id",function(req,res){
    let id= req.params.id;
    let index = data.findIndex((st)=>st.id==id);
    data.splice(index,1);
    res.send(data);
})


