import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import http from "./http";
import auth from "./auth";
import LeftPanel from "./leftPanel";
class Products extends Component{
    state={
        products:[],
        courses:["All","Sunglasses","Watches","Belts","Handbags","Wallets","Formal Shoes","Sport Shoes","Floaters","Sandals"],
        imgBtn1:true,
    }
    //Make a request to the server
    //Server will compute the response
    //Send back the response

    async fetchData(){
        let queryParams = queryString.parse(this.props.location.search);
        let searchStr = this.makeSearchString(queryParams);
        let response =await http.get(`/products/${searchStr}`);
        console.log(response);
        let {data}=response;
        this.setState({products:data});
    }
     componentDidMount(){
        this.fetchData();
       
   }
   componentDidUpdate(prevProps,prevState){
       if(prevProps!==this.props){
            this.fetchData();
       }
   }
   callURL=(url,options)=>{
    let searchStrings = this.makeSearchString(options);
    this.props.history.push({
        pathname:url,
        search:searchStrings,
    });
}
makeSearchString=(options)=>{
    let {category}= options;
    let searchStr = "";
    searchStr = this.addToQueryString (searchStr,category);
    return searchStr;
    }
    addToQueryString=(str,paramValue)=>
        ((paramValue)
        ?((str)
            ? (`${paramValue}`)
            :(`${paramValue}`))
        :(str));
        handleOptionChange=(options)=>{
            this.callURL("/products",options);
        } 
addToCart=(index)=>{
    let {cart}=this.props;
    let data = this.state.products[index];
    this.props.onCartChange(data);

}
removeCart=(index)=>{
    let {cart}=this.props;
    let data = this.state.products[index];
    this.props.onCart1Change(data);

}
editProduct=(id)=>{
    this.props.history.push(`/products/${id}/edit`);
}
deleteProduct=(id)=>{
    this.props.history.push(`/products/${id}/delete`);
}
incr=(index)=>{
    this.props.onCartIncr(index);

}
decr=(index)=>{
    this.props.onCartDecr(index);
}
image=(index)=>{

    this.setState({imgBtn1:false});
}
image1=(index)=>{
    this.setState({imgBtn1:true});
}
    render(){
        const {products,courses,imgBtn1}=this.state;
        const {cart} =this.props;
        const user = auth.getUser();
        let queryParams = queryString.parse(this.props.location.search);
        return(
            <div className="container">
               <div className="row">
                   <div className="col"> <img height="200px" width="100%"  src="https://raw.githubusercontent.com/edufectcode/react/main/data/MyStore-sale.jpg"/></div>
               </div>
                   
                   <div className="row">
                       <div className="col-3">
                        <LeftPanel options={queryParams}  courses={courses}  onOptionChange={this.handleOptionChange}/>
                       </div>
                       <div className="col-9">
                       <div className="row mx-2" >
                {products.map((pr,index)=>(
                   <React.Fragment>
                        <div className="card p-2 m-2" style={{width: "15rem"}} >
  {imgBtn1 && pr.imgLink1?<img src={pr.imgLink1} className="card-img-top" alt="..." onMouseOver={()=>this.image(index)}/>:
  <img src={pr.imgLink} className="card-img-top" alt="..." onMouseOver={()=>this.image1(index)}/>
  }
  <div className="card-body">
      <h4>{pr.name}</h4>
      <p>Rs.{pr.price}</p>
    <p className="card-text">{pr.description.substring(0,20)+"..."}</p>
    
  </div>
  {cart.find(p=>p.id===pr.id)?
  <div className="row">
  <div className="col-2 text-center"></div>
  
  <div className="col-8">
      <button className="btn btn-success" onClick={()=>this.incr(cart.findIndex(p=>p.id==pr.id))}>+</button>
      <span className="m-3 text-dark">{cart[cart.findIndex(p=>p.id==pr.id)].qty}</span>
      <button className="btn btn-warning" onClick={()=>this.decr(cart.findIndex(p=>p.id==pr.id))}>-</button>
      </div>
  <div className="col-2"></div>
  </div>
  :<button className="btn btn-success" onClick={()=>this.addToCart(index)}>Add to Cart</button> }
</div>
                   </React.Fragment>
                ))}
            </div>
            </div>
            </div>
                   </div>
        );

    }
}
export default Products;