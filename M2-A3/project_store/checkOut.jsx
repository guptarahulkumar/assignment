import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
import auth from "./auth";
class CheckOut extends Component{
    state={
        delivery:{name:"",address1:"",address2:"",city:"",cart:""},
    }
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        s1.delivery[input.name] = input.value;
        this.setState(s1);
    };
    async login(url,obj){
        try{
        let response = await http.post(url,obj);
        let {data} = response;
        console.log(response);
        //this.props.history.push("/checkOut");
        window.location="/orderBook";
        }
        catch(ex){
            if(ex.response && ex.response.status===400){
                let errors = {};
                errors.email= ex.response.data;
                this.setState({errors:errors});
            }
        }

    }
    handleSubmit = (e)=>{
        e.preventDefault();
        const {delivery} = this.state;
        console.log(delivery);
        delivery.cart=this.props.cart;
       //window.location="/orderBook";
       this.login("/orders",this.state.delivery);
    };
  
    render(){
        const {cart} = this.props;
        console.log(cart);
        const{delivery} = this.state;
        return(
            <div className="container">
               <div className="row">
                   <div className="col">
                        <img className="" height="200px" width="100%" src="https://raw.githubusercontent.com/edufectcode/react/main/data/MyStore-sale.jpg"/>
                        <br/>
                        </div>
                        </div>
                        <div className="row border ">
                            <div className="col-12 text-center"><h2>Summary of your Order</h2></div>
                            <div className="col-12 text-center"><p>Your Order has {cart.length} items</p></div>
                            <div className="col  m-2">
                            <div className="row bg-dark text-light p-2">
                                <div className="col-4 text-center">Name</div>
                                <div className="col-4 text-center">Quantity</div>
                                <div className="col-4 text-center">Value</div>
                            </div>
                            {cart.map(p=>(
                                <div className="row border p-2">
                                <div className="col-4 text-center">{p.name}</div>
                                <div className="col-4 text-center">{p.qty}</div>
                                <div className="col-4 text-center">
                                Rs.{p.price}
                                    </div>
                            </div>
                            ))}
                            <div className="row p-3">
                                <div className="col-4 text-center">Total</div>
                                <div className="col-4 text-center"></div>
                                <div className="col-4 text-center">Rs.{cart.length>0||
                        cart.qty?cart.reduce((acc,curr)=>{return acc+(parseInt(curr.price)*parseInt(curr.qty))},0)+".00":
                        cart.reduce((acc,curr)=>{return acc+(parseInt(curr.price))},0)+".00"}</div>
                            </div>
                            </div>
                        </div>

                        <h2 className="text-center">Delivery Details</h2>
                        <div className="form-group">
               <label>Name</label>
                   <input 
                   type="text"
                   className="form-control"
                   id="name"
                   name="name"
                   value ={delivery.name}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group">
               <label>Address</label>
                   <input 
                   type="text"
                   className="form-control"
                   id="address1"
                   name="address1"
                   placeholder="Line1"
                   value ={delivery.address1}
                   onChange={this.handleChange}
                   />
                    <input 
                   type="text"
                   className="form-control mt-2"
                   id="address2"
                   name="address2"
                   placeholder="Line2"
                   value ={delivery.address2}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group">
               <label>City</label>
                   <input 
                   type="text"
                   className="form-control"
                   id="city"
                   name="city"
                   value ={delivery.city}
                   onChange={this.handleChange}
                   />
                    <button className="btn btn-success m-3" onClick={this.handleSubmit}>Submit</button>
               </div>

                        </div>
                        );
                        }
                        }
export default CheckOut;