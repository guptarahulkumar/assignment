import React,{Component} from "react";
class NewTeam extends Component{
    render(){
        const{team1,tm1,tm2,onSelectTeam1,onSelectTeam2} = this.props;
       return(   
        <React.Fragment>
            {tm1?<h3 className="text-center">Team 1 :{tm1}</h3>:<h3 className="text-center">Choose Team 1</h3>}<br/>
            {team1.map((opt,index)=>(
            <div className="col-2 border bg-warning text-dark text-center m-3" onClick={()=>onSelectTeam1(index)}> 
            {opt}
            </div>
            ))
    }
    {tm2?<h3 className="text-center">Team 2 :{tm2}</h3>:<h3 className="text-center">Choose Team 2</h3>}<br/>
    {team1.map((opt,index)=>(
            <div className="col-2 border bg-warning text-dark text-center m-3" onClick={()=>onSelectTeam2(index)}> 
            {opt}
            </div>
            ))
    }
    
        </React.Fragment>       
        );
            
      
    }
}
export default NewTeam;