import React,{Component} from "react";
class PointsTable extends Component{
    render(){
        const{pointArr,onSortTeam,onSort} = this.props;
        
       return(   
        <React.Fragment>
            <h4 className="text-center">Results of the matches so far</h4>
            <div className="row bg-dark text-white">
                <div className="col-3" onClick={()=>onSortTeam()}>Team</div>
                <div className="col-1" onClick={()=>onSort("played")}>Played</div>
                <div className="col-1" onClick={()=>onSort("won")}>Won</div>
                <div className="col-1" onClick={()=>onSort("lost")}>Lost</div>
                <div className="col-1" onClick={()=>onSort("drawn")}>Drawn</div>
                <div className="col-2" onClick={()=>onSort("goalsFor")}>Goals For</div>
                <div className="col-2" onClick={()=>onSort("goalsAgainst")}>Goals Against</div>
                <div className="col-1" onClick={()=>onSort("points")}>Points</div>
                </div>
            {pointArr.map((opt,index)=>(
            <div className="row border">
            <div className="col-3">{opt.team}</div>
            <div className="col-1">{opt.played}</div>
            <div className="col-1">{opt.won}</div>
            <div className="col-1">{opt.lost}</div>
            <div className="col-1">{opt.drawn}</div>
            <div className="col-2">{opt.goalsFor}</div>
            <div className="col-2">{opt.goalsAgainst}</div>
            <div className="col-1">{opt.points}</div>
            </div>
            ))
    }
            </React.Fragment>       
        );
            
      
    }
}
export default PointsTable;