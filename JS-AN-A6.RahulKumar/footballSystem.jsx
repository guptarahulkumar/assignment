import React,{Component} from "react";
import NavBar from "./navbar";
import NewTeam from "./team";
import Match from "./match";
import AllMatch from "./allMatch";
import PointsTable from "./pointTable";
class FootballSystem extends Component{
    state = {
    team1:["France","England","Brazil","Germany","Argentina"],
    tm1:'',
    tm2:'',
    team:false,
    match:false,
    btn:true,
    score1:0,
    score2:0,
    mt:0,
    allMatch:[],
    all:false,
    point:false,
    pointArr:[],
    point1:true,
    };

    
    handleNewMatch=()=>{
        let s1 ={...this.state};
        s1.team=true;
        s1.all=false;
        s1.point=false;
        this.setState(s1);
    }
    handleSelectTeam1=(index)=>{
        let s1 = {...this.state};
        s1.tm1 =s1.team1[index];
        this.setState(s1);
    }
    handleSelectTeam2=(index)=>{
        let s1 = {...this.state};
        s1.tm2 =s1.team1[index];
        this.setState(s1);
    }
    handleStartMatch=()=>{
        let s1 ={...this.state};
         s1.tm1?(s1.tm2?
            <React.Fragment>
                {s1.tm1===s1.tm2?alert("Select different teams"):
                <React.Fragment>
                {s1.match=true}
                {s1.team=false } 
                {s1.btn=false}
                </React.Fragment>            }
            </React.Fragment>
            :alert("Select Team 2")):alert("Select Team 1");
        this.setState(s1);
    }
    handleIncr1=()=>{
        let s1 = {...this.state};
        s1.score1++;
        this.setState(s1);
    }
    handleIncr2=()=>{
        let s1 = {...this.state};
        s1.score2++;
        this.setState(s1);
    }
    handleMatchCount=()=>{
        let s1 = {...this.state};
        s1.point1=false;
        s1.mt++;
        s1.pointArr.map((opt)=>{
            if(opt.team===s1.tm1){
                opt.played++;
                opt.goalsFor=opt.goalsFor+s1.score1;
                opt.goalsAgainst = opt.goalsAgainst+s1.score2;
                if(s1.score1>s1.score2){
                    opt.won++;
                    opt.points=opt.points+3;
                }else if(s1.score1==s1.score2){
                    opt.drawn++;
                    opt.points=opt.points+1;
                }
                else{
                    opt.lost++;
                    opt.points=opt.points+0;
                }

            }
        });
        s1.pointArr.map((opt)=>{
            if(opt.team===s1.tm2){
                opt.played++;
                opt.goalsFor=opt.goalsFor+s1.score2;
                opt.goalsAgainst = opt.goalsAgainst+s1.score1;
                if(s1.score1<s1.score2){
                    opt.won++;
                    opt.points=opt.points+3;
                }else if(s1.score1==s1.score2){
                    opt.drawn++;
                    opt.points=opt.points+1;
                }else{
                    opt.lost++;
                    opt.points=opt.points+0;
                }
        }});
        let pr={};
        pr.team1=s1.tm1;
        pr.team2=s1.tm2;
        pr.score=s1.score1+'-'+s1.score2;
        if(s1.score1>s1.score2){
            pr.result= s1.tm1+' Won ';
        }else if(s1.score1<s1.score2){
            pr.result= s1.tm2 + ' Won ';
        }else{
            pr.result="Match Drawn";
        }
        s1.allMatch.push(pr);
        pr={};
        s1.btn=true;
        s1.team = false;
        s1.match=false;
        s1.tm1="";
        s1.tm2="";
        s1.score1=0;
        s1.score2=0;

        this.setState(s1);
    }
    handleAllMatch=()=>{
        let s1 ={...this.state};
        s1.btn=true;
        s1.team = false;
        s1.match=false;
        s1.all=true;
        s1.point=false;
        this.setState(s1);
    }
    handlePointsTable=()=>{
        let s1 ={...this.state};
        s1.btn=true;
        s1.team = false;
        s1.match=false;
        s1.all=false;
        s1.point=true;
        s1.point1=false;
        this.setState(s1);
    }
    SortTeam=()=>{
        let s1 = {...this.state};
        s1.pointArr.sort((p1,p2)=>{return p1.team.localeCompare(p2.team)});
        this.setState(s1);
    }
    handleSort=(str)=>{
        let s1 = {...this.state};
        if(str==="played")
        s1.pointArr.sort((p1,p2)=>{return p2.played-p1.played});
        else if(str==="won")
        s1.pointArr.sort((p1,p2)=>{return p2.won-p1.won});
        else if(str==="lost")
        s1.pointArr.sort((p1,p2)=>{return p2.lost-p1.lost});
        else if(str==="drawn")
        s1.pointArr.sort((p1,p2)=>{return p2.drawn-p1.drawn});
        else if(str==="goalsFor")
        s1.pointArr.sort((p1,p2)=>{return p2.goalsFor-p1.goalsFor});
        else if(str==="goalsAgainst")
        s1.pointArr.sort((p1,p2)=>{return p2.goalsAgainst-p1.goalsAgainst});
        else if(str==="points")
        s1.pointArr.sort((p1,p2)=>{return p2.points-p1.points});
        this.setState(s1);
    }
    render(){
        const {books,queue,team1,team2,team,tm1,tm2,match,btn,score1,score2,mt,all,allMatch,point,pointArr,point1}=this.state;
        
        if(point1===true){
            pointArr.length=team1.length;
        let data ={};    
        team1.map((opt)=>{
            data.team=opt;
            data.played=0;
            data.won=0;
            data.lost=0;
            data.drawn=0;
            data.goalsFor=0;
            data.goalsAgainst=0;
            data.points=0;
            pointArr.push(data);
            data={};
            }
        )
        }
    
        return (
            
            <div className="container text-center">
                <NavBar mt={mt}/>
                {btn?
                <React.Fragment>
                <button className="btn btn-primary m-2 text-light" onClick={()=>this.handleAllMatch()}>All Matches</button>
                <button className="btn btn-primary m-2 text-light" onClick={()=>this.handlePointsTable()}>Points Table</button>
                <button className="btn btn-primary m-2 text-light" onClick={()=>this.handleNewMatch()}>New Match</button>
                </React.Fragment>
                :""}
                {point?<PointsTable pointArr={pointArr} onSortTeam={this.SortTeam} onSort={this.handleSort} />:""}
               
                {all?<AllMatch all={allMatch} />:""}
                {team?
                <React.Fragment>
                <div className="row">
                <NewTeam team1={team1} tm1={tm1} tm2={tm2} onSelectTeam1={this.handleSelectTeam1} onSelectTeam2={this.handleSelectTeam2}/>
                </div>
                <button className="btn btn-dark text-light" onClick={()=>this.handleStartMatch()}>Start Match</button>
                </React.Fragment>
                :""}
                {match?
                <div className="row">
                <Match team1={team1} tm1={tm1} tm2={tm2}
                 score1={score1} score2={score2} onIncr1={this.handleIncr1} onIncr2={this.handleIncr2} 
                 onMatch={this.handleMatchCount}
                 />
                </div>
                :""}

             </div>
        );
    }
}
export default FootballSystem; 