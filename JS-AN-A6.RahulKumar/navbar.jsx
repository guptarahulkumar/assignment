import React,{Component} from "react";
class NavBar extends Component{
render(){
let {mt} = this.props;
    return(
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <a className="navbar-brand text-light" href="#">
               <h3 className="m-2">Football Tournament</h3>
            </a>
            <div className="" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <a className="nav-link text-light" href="#">
                            Number of Matches 
                            <span className="badge badge-pill rounded-circle badge-primary text-white bg-primary">{mt}</span>
                        </a>
                    </li>
                    
                    
                </ul>
            </div>
        </nav>
    );
}
}
export default NavBar;