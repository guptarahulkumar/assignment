import React,{Component} from "react";
class AllMatch extends Component{
    render(){
        const{all} = this.props;
        
       return(   
        <React.Fragment>
            <h4 className="text-center">Results of the matches so far</h4>
            <div className="row bg-dark text-white">
                <div className="col-2">Team1</div>
                <div className="col-2">Team2</div>
                <div className="col-4">Score </div>
                <div className="col-4">Result</div>
                </div>
            {all.map((opt,index)=>(
                
           <div className="row border">
           <div className="col-2">{opt.team1}</div>
           <div className="col-2">{opt.team2}</div>
           <div className="col-4">{opt.score} </div>
           <div className="col-4">{opt.result}</div>
            </div>
            ))
    }
            </React.Fragment>       
        );
            
      
    }
}
export default AllMatch;