import React,{Component} from "react";
class Match extends Component{
    render(){
        const{team1,tm1,tm2,score1,score2,onIncr1,onIncr2,onMatch} = this.props;
       return(   
        <React.Fragment>
            <h1 className="text-center">Welcome to an exciting match</h1>
            <div className="row">
                <div className="col-4">
                    <h3>{tm1}</h3>
                    </div>
                    <div className="col-4">
                    <h3>{score1}-{score2}</h3>
                    </div>
                    <div className="col-4">
                    <h3>{tm2}</h3>
                    </div>
            </div>
            <div className="row">
                <div className="col-4">
                    <button className="btn btn-warning" onClick={()=>onIncr1()}>Goal Scored</button>
                    </div>
                    <div className="col-4">
                    </div>
                    <div className="col-4">
                    <button className="btn btn-warning" onClick={()=>onIncr2()}>Goal Scored</button>
                    </div>
            </div>
            <div className="row">
                <div className="col-4">
                    </div>
                    <div className="col-4">
                    <button className="btn btn-warning" onClick={()=>onMatch()}>Match Over</button>
                    </div>
                    <div className="col-4">
                    </div>
            </div>
        </React.Fragment>       
        );
            
      
    }
}
export default Match;