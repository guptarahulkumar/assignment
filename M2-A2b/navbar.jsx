import React,{Component} from "react";
import {Link} from "react-router-dom";
import {NavDropdown} from 'react-bootstrap'; 

class Nav extends Component{
    render(){
       const {user} = this.props;
            return(
                <nav className="navbar navbar-expand-sm navbar-success bg-success">
                    {user && user.role=="admin" &&(
                    <Link className="navbar-brand " to="/admin">
                      <h5 className="mx-3 text-dark">Home</h5>
                    </Link>)}
                    {user && user.role=="student" &&(
                    <Link className="navbar-brand " to="/student">
                      <h5 className="mx-3 text-dark">Student Home</h5>
                    </Link>)}
                    {user && user.role=="faculty" &&(
                    <Link className="navbar-brand " to="/faculty">
                      <h5 className="mx-3 text-dark">Faculty Home</h5>
                    </Link>)}
                    {!user &&(
                    <Link className="navbar-brand " to="/login">
                      <h5 className="mx-3 text-dark">Home</h5>
                    </Link>)}
                    <div className="" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                        {user && user.role=="admin" && (
                            <li className="nav-item">
                               <Link className="nav-link text-dark" to='/addUser'>Register</Link>
                            </li>
                            )}
                        {user && user.role=="admin" &&(
                        <li className="nav-item dropdown" >
                            <NavDropdown  title="Assign" id="navbarScrollingDropdown">
                <NavDropdown.Item ><Link className="nav-link text-dark" to='/studentCourse'>Student to Course</Link></NavDropdown.Item>
                <NavDropdown.Item  ><Link className="nav-link text-dark" to='/facultyCourse'>Faculty to Course</Link></NavDropdown.Item>
               </NavDropdown>
                            </li>
                            )}
                           
                             {user && user.role=="student" && (
                            <li className="nav-item">
                               <Link className="nav-link text-dark" to='/studentDetails'>Student Details</Link>
                            </li>
                            )}
                            {user && user.role=="student" && (
                            <li className="nav-item">
                               <Link className="nav-link text-dark" to='/allClasses'>All Classes</Link>
                            </li>
                            )}
                            {user && user.role=="student" && (
                            <li className="nav-item">
                               <Link className="nav-link text-dark" to='/courseStudent'>All Course</Link>
                            </li>
                            )}
                            {user && user.role=="faculty" && (
                            <li className="nav-item">
                               <Link className="nav-link text-dark" to='/coursesAssigned'>Courses</Link>
                            </li>
                            )}
                            {user && user.role=="faculty" &&(
                            <li className="nav-item dropdown ">
                            <NavDropdown className="text-dark" title="Class Details" id="navbarScrollingDropdown">
                <NavDropdown.Item ><Link className="nav-link text-dark" to='/scheduleClass'>Schedule a Class</Link></NavDropdown.Item>
                <NavDropdown.Item ><Link className="nav-link text-dark" to='/scheduledClass'>All scheduled Class</Link></NavDropdown.Item>
               </NavDropdown>
                            </li>
                            )}
                             
                            {user && user.role=="admin" &&(
                            <li className="nav-item dropdown ">
                            <NavDropdown className="text-dark" title="View" id="navbarScrollingDropdown">
                <NavDropdown.Item ><Link className="nav-link text-dark" to='/allStudent'>All Student</Link></NavDropdown.Item>
                <NavDropdown.Item ><Link className="nav-link text-dark" to='/allFaculty'>All Faculty</Link></NavDropdown.Item>
               </NavDropdown>
                            </li>
                            )}
                            {!user && (                            
                            <li className="nav-item">
                                <Link className="nav-link text-dark" to="/login">
                                <h6 style={{position:"absolute",right:"100px"}}>Log In</h6>
                                </Link>
                            </li>
                            )}
                            {user &&  (
                            <li className="nav-item nav-link">
                                <h6 style={{position:"absolute",right:"100px",color:"white", textTransform:"capitalize"}}>{"Welcome "+ user.name}</h6>
                            </li>
                            )}
                            {user && (
                            <li className="nav-item">
                                <Link className="nav-link text-dark"  to="/logout">
                                <h6 style={{position:"absolute",right:"10px"}}>Log Out</h6>
                                </Link>
                            </li>
                            )}
                        </ul>
                    </div>
                </nav>
            );
        }
}
export default Nav;