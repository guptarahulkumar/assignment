import React,{Component} from "react";
import {Route,Switch,Redirect } from"react-router-dom";
import http from "./http";
import auth from "./auth";
import Nav from "./navbar";
import Login from "./login";
import Logout from "./logout";
import Admin from "./admin";
import Student from "./student";
import Faculty from "./faculty";
import AddUser from "./addUser";
import AllStudent from "./allStudent";
import AllFaculty from "./allFaculty";
import StudentCourse from "./studentCourse";
import FacultyCourse from "./facultyCourse";
import NotAllowed from "./notAllowed";
import AllClasses from "./allClasses";
import CourseFaculty from "./coursesAssigned";
import CourseStudent from "./courseStudent";
import ScheduleClass from "./scheduleClass";
import ScheduledClass from "./scheduledClass";
import StudentDetails from "./studentDetails";

class MainComponent extends Component{
    render(){
        const user = auth.getUser();
        return(
            <div className="container-fluid">
                <Nav user={user} />
                <Switch>
                <Route path="/admin"
                    render={(props) => 
                        user?
                        ( user.role==="admin" ?
                        ( <Admin {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    />
                     <Route path="/student"
                    render={(props) => 
                        user?
                        ( user.role==="student" ?
                        ( <Student {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    />
                    <Route path="/studentDetails"
                    render={(props) => 
                        user?
                        ( user.role==="student" ?
                        ( <StudentDetails {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    />
                     <Route path="/courseStudent"
                    render={(props) => 
                        user?
                        ( user.role==="student" ?
                        ( <CourseStudent {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    />
                       <Route path="/coursesAssigned"
                    render={(props) => 
                        user?
                        ( user.role==="faculty" ?
                        ( <CourseFaculty {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    />
                    <Route path="/allClasses"
                    render={(props) => 
                        user?
                        ( user.role==="student" ?
                        ( <AllClasses {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    />
                    <Route path="/faculty"
                    render={(props) => 
                        user?
                        ( user.role==="faculty" ?
                        ( <Faculty {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                    />
                    <Route path="/addUser"
                    render={(props) => 
                        user?
                        ( user.role==="admin" ?
                        ( <AddUser {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                        
                    />
                     <Route path="/allStudent"
                    render={(props) => 
                        user?
                        ( user.role==="admin" ?
                        ( <AllStudent {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                        
                    />
                    <Route path="/allFaculty"
                    render={(props) => 
                        user?
                        ( user.role==="admin" ?
                        ( <AllFaculty {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                        
                    />
                    <Route path="/studentCourse"
                    render={(props) => 
                        user?
                        ( user.role==="admin" ?
                        ( <StudentCourse {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                        
                    />
                    <Route path="/facultyCourse"
                    render={(props) => 
                        user?
                        ( user.role==="admin" ?
                        ( <FacultyCourse {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                        
                    />
                      <Route path="/scheduleClass"
                    render={(props) => 
                        user?
                        ( user.role==="faculty" ?
                        ( <ScheduleClass {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                        
                    />
                     <Route path="/scheduledClass"
                    render={(props) => 
                        user?
                        ( user.role==="faculty" ?
                        ( <ScheduledClass {...props}/>):
                        (<Redirect from="/" to="/notAllowed"/>)
                        ):
                        (
                            <Redirect from="/" to="/login"/>
                        )}
                        
                    />
                    <Route path="/login" component={Login}/> 
                    <Route path="/logout" component={Logout}/> 
                    <Redirect from="/" to="/login"/>
                </Switch>
            </div> );
    }
}
export default MainComponent;