import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class FacultyCourse extends Component{
    state={
        data:[],
        btn:true,
        index:-1,
        names:[],
}
    async fetchData(){
        let response =await http.get("/getCourses");
        let response1 = await http.get("/getFacultyNames");
        console.log(response);
        console.log(response1);
        let {data}=response;
        this.setState({data:data,names:response1.data});
    }
     componentDidMount(){
         this.fetchData();
        
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
             this.fetchData();
        }
    }
    editId=(id)=>{
        console.log(id);
        this.setState({btn:false,index:id});
        }
        makeCheckboxes=(arr,values,name,label)=>(
            <React.Fragment>
                 <label>{label}<sup className="text-danger"><b>*</b></sup></label>
                {arr.map((opt)=>(
                    <div className="form-check mx-3 ">
                        <input
                        className="form-check-input "
                        value={opt}
                        type="checkbox"
                        name={name}
                        checked={values.find((val)=>val===opt)}
                        onChange={this.handleChange}
                        />
                        <label className="form-check-label" style={{textTransform:"capitalize"}}>{opt}</label>
                    </div>
                ))}
        </React.Fragment>
        );


    render(){
        const {data=[],btn,index,names=[]}=this.state;
        console.log(this.state.data);
        return(
            <div className="container">
                   {btn ?
            (<React.Fragment>
               <h4 className="m-3">Add Faculties to a Course</h4>
                   <div className="row text-dark p-3" >
                    <div className="col-1">Course Id</div>
                    <div className="col-3">Name</div>
                    <div className="col-2">Course Code</div>
                    <div className="col-3">Description</div>
                    <div className="col-2">Faculty Name</div>
                    <div className="col-1">
                      
                        </div>
                </div>
               {data.map((pr,index)=>
               <div className="row border bg-warning p-3" >
               <div className="col-1">{pr.courseId}</div>
               <div className="col-3">{pr.name}</div>
               <div className="col-2">{pr.code}</div>
               <div className="col-3">{pr.description}</div>
               <div className="col-2">{pr.faculty.join(",")}</div>
               <div className="col-1">
                   <button className="btn btn-secondary btn-sm" onClick={()=>this.editId(index)}>Edit</button>
                   </div>
                   </div>
                )}
        </React.Fragment>
            ):(
                <div className="row">
                <h4 className="m-3">Edit the Course</h4>
                <div className="form-group">
          <label>Name</label>
          <input 
          type="text"
          className="form-control"
          id="name"
          name="name"
          value ={data[index].name}
          onChange={this.handleChange}
          />
      </div>
      <div className="form-group">
          <label>Course Code</label>
          <input 
          type="text"
          className="form-control"
          id="code"
          name="code"
          value ={data[index].code}
          onChange={this.handleChange}
          />
      </div>
      <div className="form-group">
          <label>Description</label>
          <input 
          type="text"
          className="form-control"
          id="description"
          name="description"
          value ={data[index].description}
          onChange={this.handleChange}
          />
      </div>
      {console.log(data[index].faculty)}
      {this.makeCheckboxes(names,data[index].faculty[0].split(","),"faculty","Faculties")}
     <div className="col-2"> <button className="btn btn-primary m-2"> <Link to="/admin" className="text-light">Update</Link></button></div>
           </div>
       )}
            </div>
        
        );

    }
}
export default FacultyCourse;