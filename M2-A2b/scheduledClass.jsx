import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
import auth from "./auth";
class ScheduledClass extends Component{
    state={
        data:[],
        btn:true,
}
    async fetchData(){
        const user = auth.getUser();
        let response =await http.get(`/getFacultyClass/${user.name}`);
        console.log(response);
        let {data}=response;
        this.setState({data:data});
    }
     componentDidMount(){
         this.fetchData();
        
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
             this.fetchData();
        }
    }


    render(){
        const {data=[]}=this.state;
        console.log(this.state.data);
        return(
            <div className="container">
               <h4 className="m-3" >All Scheduled Classes</h4>
                   <div className="row text-dark p-3" style={{backgroundColor:"lightgrey"}}>
                    <div className="col-3">Course Name</div>
                    <div className="col-2">Start Time </div>
                    <div className="col-2">End Time</div>
                    <div className="col-3">Topic </div>
                    <div className="col-2"></div>
                </div>
               {data.map(pr=>
               <div className="row p-3 " style={{backgroundColor:"wheat",border:"1px solid hotpink"}}>
               <div className="col-3">{pr.course}</div>
               <div className="col-2">{pr.time}</div>
               <div className="col-2">{pr.endTime}</div>
               <div className="col-3">{pr.topic}</div>
               <div className="col-2">
                   <button className="btn btn-secondary btn-sm"><Link className="text-light" to="/scheduleClass">Edit</Link></button>
               </div>
                   </div>
                )}
                 <button className="btn btn-primary m-3"><Link className="text-light" to="/scheduleClass">Add New Class</Link></button>
            </div>
        );

    }
}
export default ScheduledClass;