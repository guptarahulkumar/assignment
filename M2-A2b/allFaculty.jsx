import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import LeftPanel from "./leftPanel";
import http from "./http";
class AllFaculty extends Component{
    state={
        data:{},
        courses:["ANGULAR","JAVASCRIPT","REACT","BOOTSTRAP","CSS","REST AND MICROSERVICES","NODE"],
}
    async fetchData(){
        let queryParams = queryString.parse(this.props.location.search);
        let searchStr = this.makeSearchString(queryParams);
        let response =await http.get(`/getFaculties?${searchStr}`);
        console.log(response);
        let {data}=response;
        this.setState({data:data});
    }
     componentDidMount(){
         this.fetchData();
        
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
             this.fetchData();
        }
    }
    
    handlePage=(incr)=>{
        let queryParams = queryString.parse(this.props.location.search);
        let {page = "1"} = queryParams;
        let newpage = +page+incr;
        queryParams.page = newpage;
        this.callURL("/allFaculty",queryParams);
    }
    callURL=(url,options)=>{
        let searchStrings = this.makeSearchString(options);
        this.props.history.push({
            pathname:url,
            search:searchStrings,
        });
    }
    makeSearchString=(options)=>{
        let {page=1,course}= options;
        let searchStr = "";
        searchStr = this.addToQueryString (searchStr,"page",page);
        searchStr = this.addToQueryString (searchStr,"course",course);
        return searchStr;
        }
        addToQueryString=(str,paramName,paramValue)=>
            ((paramValue)
            ?((str)
                ? (`${str}&${paramName}=${paramValue}`)
                :(`${paramName}=${paramValue}`))
            :(str));
        
            handleOptionChange=(options)=>{
                this.callURL("/allFaculty",options);
            } 
        
    render(){
        const {page,items=[],totalItems,totalNum,}=this.state.data;
        console.log(this.state.data);
        let queryParams = queryString.parse(this.props.location.search);
        return(
            <div className="container">
                <div className="row">
                    <div className="col-3">
                  <LeftPanel options={queryParams}  courses={this.state.courses}  onOptionChange={this.handleOptionChange}/>
                    </div>
                    <div className="col-9">
               <h4 className="m-3">All Faculties</h4>
               <h6 className="m-3">{page} to {totalNum/totalItems} of {totalNum}</h6>
                   <div className="row border p-3" >
                    <div className="col-4"> Id</div>
                    <div className="col-4">Name</div>
                    <div className="col-4">Courses</div>
                    
                </div>
               {items.map(pr=>
                <div className="row border bg-warning p-3" key={pr.id}>
                    <div className="col-4">{pr.id}</div>
                    <div className="col-4">{pr.name}</div>
                    <div className="col-4">{pr.courses.join(" , ")}</div>
                </div>
                )}
                <div className="row">
                    <div className="col-2">
                        {page>1?<button className="btn btn-primary mt-2 " onClick={()=>this.handlePage(-1)}> Prev</button>:""}
                    </div>
                    <div className="col-8"></div>
                    <div className="col-2">
                    {(page*totalItems)<totalNum?<button className="btn btn-primary mt-2 " onClick={()=>this.handlePage(1)}> Next</button>:""}
                    </div>
                </div>
            </div>
            </div>
                </div>
        );

    }
}
export default AllFaculty;