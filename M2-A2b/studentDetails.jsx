import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
import auth from "./auth";
class StudentDetails extends Component{
    state={
        data:{name:"",dob:"",gender:"",about:""},
        btn:true,
    };
    async componentDidMount(){
        this.fetchData();
    }
    async componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props)
        this.fetchData();
       
   }
    async fetchData(){
        const user = auth.getUser();
            let response = await http.get(`/getStudentDetails/${user.name}`);
            let {data} = response;
            console.log(data);
            if(data.id=""){
                let data={name:"",dob:"",gender:"",about:""};
                this.setState({data:data});
            }else{
                this.setState({data:data,btn:false});
            }
            
    }
    
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        if(input.name==="gender"){
            if(input.checked===true)
            s1.data[input.name] = input.value;
        }
        s1.data[input.name] = input.value;
        this.setState(s1);
    };
    async postData(url,obj){
        let response = await http.post(url,obj);
        console.log(response);
        this.props.history.push("/student");
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        const user = auth.getUser();
        alert("Details Added Successfully");
        console.log(this.state.data);
        this.state.data.name =user.name;
        this.postData("/postStudentDetails",this.state.data);
    };

    render(){
      const {data,btn} = this.state;
      return (
            <div className="container">
                <h4 className="mt-5">Student Details</h4>
                <div className="form-group">
                    <label> Gender<sup className="text-danger"><b>*</b></sup> : </label>
                    <input className="form-check-group m-2" type="radio" name="gender" id="male" value="Male" checked={data.gender}/>Male
                    <input className="form-check-group mx-4 " type="radio" name="gender" id="female" value="Female" checked={data.gender}/>Female
                </div>
                <hr/>
                <div className="form-group">
                   <label>Date Of Birth<sup className="text-danger">*</sup></label>
                   <input 
                   type="date"
                   className="form-control"
                   id="dob"
                   name="dob"
                   value ={data.dob}
                   onChange={this.handleChange}
                   />
               </div>
               
               <div className="form-group mt-3">
                   <label>About MySelf</label>
                   <textarea className="form-control" id="city"
                   name="about"
                   value ={data.about}
                   onChange={this.handleChange}></textarea>
               </div>
               {btn?
               <button className="btn mt-4 btn-primary" onClick={this.handleSubmit}>Add Details</button>:""}
            </div>
        );

    }
}
export default StudentDetails;