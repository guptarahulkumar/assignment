import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
import auth from "./auth";
class Login extends Component{
    state={
        form:{email:"",password:""},
        errors:{},
    };
    
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        console.log(input.value,input);
        if(input.name==="password"){
            let str = input.value;
            console.log(input);
            if(str.length>=7||str.length==0){
                s1.form[input.name] = input.value;
                s1.errors.password="";
                console.log(input);
            }else{
                s1.form[input.name] = input.value;
                s1.errors.password="Password must be of 7 characters";
                console.log("Password must be of 7 characters",str);
                console.log(input);
            }
        }else
        s1.form[input.name] = input.value;
        this.setState(s1);
    };
    async login(url,obj){
        try{
        let response = await http.post(url,obj);
        let {data} = response;
        console.log(response);
        auth.login(data);
        //this.props.history.push("/products");
        if(data.role==="admin")
        window.location="/admin";
        else if(data.role==="student")
        window.location="/student";
        else window.location="/faculty";
        }
        catch(ex){
            if(ex.response && ex.response.status===400){
                let errors = {};
                errors.email= ex.response.data;
                this.setState({errors:errors});
            }
        }

    }
    handleSubmit = (e)=>{
        e.preventDefault();
        const {form} = this.state;
        console.log("Rahul");
        this.login("/login",this.state.form);
    };
   
    render(){
      let {email,password}=this.state.form;
      let {errors =null}=this.state;
      //console.log(errors);
      return (
            <div className="container">
                <div className="row">
                    <div className="col-3"></div>
                    <div className="col-9 m-4">
                    <h2 className=" text-center p-2 m-3">Login</h2>
                        <div className="row">
                            <div className="col-3"> <h6 className="text-center"><label>Email<sup className="text-danger"><b>*</b></sup></label></h6></div>
                            <div className="col-9">
               <div className="form-group">
                   <input 
                   type="email"
                   className="form-control"
                   id="email"
                   name="email"
                   placeholder="Enter your email"
                   value ={email}
                   onChange={this.handleChange}
                   />
                   <p className="text-center "><small>We'll never share your email with anyone else.</small></p>
                   {errors && errors.email && (<span className="bg-danger text-white">{errors.email}</span>)}
               </div>
               </div>
               </div>
               <div className="row">
                            <div className="col-3"> <h6 className="text-center"><label>Password<sup className="text-danger"><b>*</b></sup></label></h6></div>
                            <div className="col-9">
               <div className="form-group">
                   <input 
                   type="password"
                   className="form-control"
                   name="password"
                   id="password"
                   placeholder="Password"
                   value ={password}
                   onChange={this.handleChange}
                   /><br/>
                   {errors && errors.password && (<span className="mt-2 text-center text-light mt-3 mx-5 p-2 bg-danger">{errors.password}</span>)}
               </div>
               </div>
               </div>

               <center><button className="btn btn-primary m-3" onClick={this.handleSubmit}>Login</button></center>
               </div>
                    <div className="col-3"></div>
                </div>
            </div>
        );

    }
}
export default Login;