import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
import auth from "./auth";
class ScheduleClass extends Component{
    state={
        course1:{course:"",time:"",endTime:"",topic:"",facultyName:""},
        coursess : ["ANGULAR","REACT","BOOTSTRAP"],
        edit:false,
    };
    
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        s1.course1[input.name] = input.value;
        console.log(s1.course1);
        this.setState(s1);
    };
    async postData(url,obj){
        let response = await http.post(url,obj);
        console.log(response);
        this.props.history.push("/faculty");
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        let {course1,edit} = this.state;
        const user = auth.getUser();
        course1.facultyName=user.name;
        console.log(course1);
        alert("Schedule Class Successfully");
        this.postData("/postClass",course1);
    };

    render(){
      let {course,time,endTime,topic,facultyName}=this.state.course1;
      return (
            <div className="container">
                <h4 className="mt-5">Schedule a class</h4>
                <div  className="form-group">
                <select className="form-control" id={course} onChange={this.handleChange}>
                <option className="bg-dark text-light p-2" selected disabled>Select a Course</option>
               {this.state.coursess.map((opt)=>(
                  <option className="bg-dark text-light p-2">{opt}</option>
               ))}
               </select>
       </div>
       <div className="form-group mt-4">
                   <label>Time<sup className="text-danger"><b>*</b></sup></label>
                   <input 
                   type="time"
                   className="form-control"
                   id="time"
                   name="time"
                   value ={time}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group mt-4">
                   <label>End Time<sup className="text-danger"><b>*</b></sup></label>
                   <input 
                   type="time"
                   className="form-control"
                   id="endTime"
                   name="endTime"
                   value ={endTime}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group mt-4">
                   <label>Topic<sup className="text-danger"><b>*</b></sup></label>
                   <input 
                   type="text"
                   className="form-control"
                   id="topic"
                   name="topic"
                   value ={topic}
                   placeholder="Enter class topic"
                   onChange={this.handleChange}
                   />
               </div>
              
               <button className="btn mt-4 btn-primary" onClick={this.handleSubmit}>Schedule</button>
                
            </div>
        );

    }
}
export default ScheduleClass;