import React,{Component} from "react";
class LeftPanel extends Component{
    handleChange = (e)=>{
        const {currentTarget  : input}=e;
        let options = {...this.props.options};
        if(input.name === "course")
        options[input.name]=this.updateCBs(
            options[input.name],
            input.checked,
            input.value
        );
        else options[input.name] = input.value;
        console.log("LeftPanel",options);
        this.props.onOptionChange(options);
    }
    updateCBs=(inpValues,checked,value)=>{
        let inpArr = inpValues ? inpValues.split(","):[];
        if(checked) inpArr.push(value);
        else{
            let index = inpArr.findIndex((ele)=>ele === value);
            if(index>=0) inpArr.splice(index,1);
        }
        console.log(inpValues,inpArr);
        return inpArr.join(",");
    }
    makeCheckboxes=(arr,values,name,label)=>(
        <React.Fragment>
             <div className="row border bg-light mt-3">
               <div className="col p-2 " >
                <h5>{label}</h5>
                   </div>
               </div>
               <div className="row ">
            {arr.map((opt)=>(
                <div className="col-12 border p-2">
                <div className="form-check ">
                    <input
                    className="form-check-input "
                    value={opt}
                    type="checkbox"
                    name={name}
                    checked={values.find((val)=>val===opt)}
                    onChange={this.handleChange}
                    />
                    <label className="form-check-label" style={{textTransform:"capitalize"}}>{opt}</label>
                </div>
                </div>
            ))}
    </div>
    </React.Fragment>
    );
    render(){
        let {course=""}=this.props.options;
        let {courses}=this.props;
        return(
            <div className="row">
                <div className="col-9">
                {this.makeCheckboxes(courses,course.split(","),"course","Course")}
                </div>
            </div>
        );
    }
};
export default LeftPanel;
