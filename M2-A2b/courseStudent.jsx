import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
import auth from "./auth";
class CourseStudent extends Component{
    state={
        data:[],
        btn:true,
}
    async fetchData(){
        const user = auth.getUser();
        let response =await http.get(`/getStudentCourse/${user.name}`);
        console.log(response);
        let {data}=response;
        this.setState({data:data});
    }
     componentDidMount(){
         this.fetchData();
        
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
             this.fetchData();
        }
    }


    render(){
        const {data=[]}=this.state;
        console.log(this.state.data);
        return(
            <div className="container">
               <h4 className="m-3" >Courses Assigned</h4>
                   <div className="row text-dark p-3" style={{backgroundColor:"lightgrey"}}>
                    <div className="col-3">Course Id</div>
                    <div className="col-3">Course Name</div>
                    <div className="col-3">Course Code</div>
                    <div className="col-3">Description</div>
                </div>
               {data.map(pr=>
               <div className="row border p-3" style={{backgroundColor:"lightskyblue"}}>
               <div className="col-3">{pr.courseId}</div>
               <div className="col-3">{pr.name}</div>
               <div className="col-3">{pr.code}</div>
               <div className="col-3">{pr.description}</div>
                   </div>
                )}
                
            </div>
        );

    }
}
export default CourseStudent;