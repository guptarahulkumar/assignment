import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class AddUser extends Component{
    state={
        user:{name:"",password:"",cnfpassword:"",email:"",role:""},
        errors:{},
    };
    
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        if(input.name==="password"){
            let str = input.value;
            console.log(input);
            if(str.length>=7||str.length==0){
                s1.user[input.name] = input.value;
                s1.errors.password="";
                console.log(input);
            }else{
                s1.user[input.name] = input.value;
                s1.errors.password="Password can not be blank. Minimum length should be 7 characters";
                console.log("Password must be of 7 characters",str);
                console.log(input);
            }
        }else if(input.name==="cnfpassword"){
            if(s1.user.password==input.value||input.value.length==0){
            s1.user[input.name] = input.value;
                s1.errors.cnfpassword="";
        }else{
            s1.user[input.name] = input.value;
            s1.errors.cnfpassword="Password not matched";
                console.log("Password must be of 7 characters");
        }
    }else if(input.name==="role"){
        if(input.checked)
        s1.user[input.name] = input.value;

    }else{
        s1.user[input.name] = input.value;

    }
        this.setState(s1);
    };
    async postData(url,obj){
        let response = await http.post(url,obj);
        console.log(response);
        this.props.history.push("/admin");
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        let {user,edit} = this.state;
        alert("User Added Successfully");
        this.postData("/register",user);
    };

    render(){
      let {name,password,cnfpassword,email,role}=this.state.user;
      const {errors} = this.state;
      return (
            <div className="container">
                <h4 className="mt-5">Register</h4>
               <div className="form-group">
                   <label>Name<sup className="text-danger"><b>*</b></sup></label>
                   <input 
                   type="text"
                   className="form-control"
                   id="name"
                   name="name"
                   value ={name}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group">
                   <label>Password<sup className="text-danger"><b>*</b></sup></label>
                   <input 
                   type="password"
                   className="form-control"
                   name="password"
                   id="password"
                   value ={password}
                   onChange={this.handleChange}
                   />
                   {errors && errors.password && (<span className="mt-2 text-center text-danger">{errors.password}</span>)}
               </div>
               <div className="form-group">
                   <label>Confirm Password<sup className="text-danger"><b>*</b></sup></label>
                   <input 
                   type="password"
                   className="form-control"
                   name="cnfpassword"
                   id="cnfpassword"
                   value ={cnfpassword}
                   onChange={this.handleChange}
                   />
                   {errors && errors.cnfpassword && (<span className="mt-2 text-center text-danger">{errors.cnfpassword}</span>)}
               </div>
               <div className="form-group">
                   <label>Email<sup className="text-danger"><b>*</b></sup></label>
                   <input 
                   type="email"
                   className="form-control"
                   name="email"
                   id="email"
                   value ={email}
                   onChange={this.handleChange}
                   />
                  
               </div>
               <div className="form-group mt-3">
                   <label>Role<sup className="text-danger"><b>*</b></sup></label><br/>

                   <input 
                   type="radio"
                   className="form-check-control"
                   name="role"
                   id="student"
                   value="student"
                   onChange={this.handleChange}
                   />Student<br/>
                   <input 
                   type="radio"
                   className="form-check-control"
                   name="role"
                   value="faculty"
                   id="faculty"
                   onChange={this.handleChange}
                   />Faculty
                   
               </div>
                      
               <button className="btn mt-4 btn-primary" onClick={this.handleSubmit}>Create</button>
                
            </div>
        );

    }
}
export default AddUser;