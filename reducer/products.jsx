import React,{Component} from "react";
import {connect} from  "react-redux";
class Products extends Component{
   addCart=(index)=>{
       console.log(index);
    this.props.dispatch(addAction(index));
    this.props.history.push("/products");
   }
render(){
    return (
       <div className="container">
           <h3>Products</h3>
           {this.props.products.map((p,index)=>(
               <div className="row mt-5" key={p.name}>  
                   <div className="col">      
               <h6>{p.name}-{p.price}X{p.quantity}</h6>
               <button  onClick={()=>this.addCart(index)}>Add to cart</button>
               </div>
               </div>
           ))}
           <hr/>
           <h3>Your Cart</h3>
           {this.props.cart?this.props.cart.map((p,index)=>(
               <div className="row mt-5">  
                   <div className="col">      
               <h6>{p.name}- {p.price} X {p.quantity}</h6>
               <button onClick={()=>this.addCart(index)}>Checkout</button>
               </div>
               </div>
           )):
           <React.Fragment>
           <i>Please add some products to cart.</i><br/>
           Total : $0.00<br/>
           <button className="mt-5" disabled>Checkout</button>
           </React.Fragment>}
       </div>
    );
}

}
const addAction=(index)=>({
    type:"SELL",
    payload:index,
});
const mapStateToProps=(state)=>{
        return {products:state.products};
};
export default connect(mapStateToProps)(Products);