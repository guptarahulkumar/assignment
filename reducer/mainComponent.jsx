import React,{Component} from "react";
import {Route,Switch,Redirect} from "react-router-dom";
import Products from "./products";
class MainComponent extends Component{
   render(){
       return(
           <div className="container">
               <Switch>
                   <Route path="/products" component={Products}/>
                   <Redirect from="/"  to="/products"/>
               </Switch>
           </div>
       );
   }


}

export default MainComponent;