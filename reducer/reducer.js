const initialState = {
    products:[
        {name:"iPad 4 Mini",price:"$500.01",quantity:2},
        {name:"H & M T-Shirt White",price:"$10.99",quantity:10},
        {name:"Charli XCX - Sucker CD",price:"$19.99",quantity:5},
    ],
    cart:[],
    
};
const reducer  = (state  = initialState,action)=>{
    if(action.type==="SELL"){
         return sellReducer(state,action);
    }
    return state;
};
const  sellReducer=(state,action)=>{
    const {products,cart}= state;
    let product = products[action.payload];
    console.log(product);
    let products1 = null;
    if(product){
        products1= products.map((s,index)=>index===action.payload?
        {...s.quantity=s.quantity-1}:s
        );
    }
    else products1 = [...products];
    let cart1 =cart.push(product);
    return {...state,products:products1,cart:cart1};
};
export default reducer;