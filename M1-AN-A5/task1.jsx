import React,{Component} from "react";
class JsonTask extends Component{
    state={
        products:[{"product":"Pepsi", "sales":[2,5,8,10,5]},
        {"product":"Coke", "sales":[3,6,5,4,11,5]},
        {"product":"5Star", "sales":[10,14,22]},
        {"product":"Maggi", "sales":[3,3,3,3,3]},
        {"product":"Perk", "sales":[1,2,1,2,1,2]},
        {"product":"Bingo", "sales":[0,1,0,3,2,6]},
        {"product":"Gems", "sales":[3,3,1,1]}],
        arr : [],
        Index:-1,
        view:0,
    };
    arrCon=()=>{
        let s1 = {...this.state};
        s1.products.map(st=> {
            let data={};
            let {product,sales} = st;
            let str = sales.reduce((acc,curr)=>{return acc+curr},0);
            data.product = product;
            data.totalSales = str;
            s1.arr.push(data);
        });
       
    }
    handleProduct=()=>{
        let s1 = {...this.state};
        s1.arr.length=7;
        s1.arr=s1.arr.sort((p1,p2)=>{return p1.product.localeCompare(p2.product)});
        this.setState(s1);
    }
    handleTotalSalesAsc=()=>{
        let s1 = {...this.state};
       s1.arr=s1.arr.sort((p1,p2)=>{return (+p1.totalSales)-(+p2.totalSales)});
        this.setState(s1);
       
    }
    handleTotalSalesDesc=()=>{
        let s1 = {...this.state};
       s1.arr=s1.arr.sort((p1,p2)=>{return (+p2.totalSales)-(+p1.totalSales)});
       
        this.setState(s1);
       
    }
    handleDetail=(index)=>{
        let s1 ={...this.state};
     s1.Index =index;
     s1.view=1;
        this.setState(s1);

    }
    
    render(){
        
       let {arr,Index,products,view} = this.state;
    
       this.arrCon();
       arr.length=7;
        return(
            
            <div className="container">
                <button className="btn btn-primary btn-sm text-white m-3" onClick={()=>this.handleProduct()}>
                    Sort By Product</button>
                <button className="btn btn-primary btn-sm text-white  m-3" onClick={()=>this.handleTotalSalesAsc()}>
                    Total Sales Asc</button>
                <button className="btn btn-primary btn-sm text-white  m-3" onClick={()=>this.handleTotalSalesDesc()}>Total Sales Desc</button>
                        <div className="row border bg-dark text-white">
                              <div className="col-4 border">Product</div>
                              <div className="col-4 border text-center">Total Sales</div>
                              <div className="col-4 border text-center"></div>
                        </div>  
                        {arr.map((st,index)=> {
                            let {product,totalSales} = st;
                            
                        return(
                          <div 
                          className="row border "
                          >
                              <div className="col-4 border">{product}</div>
                              <div className="col-4 border text-center">{totalSales}</div>
                              <div className="col-4 border text-center">
                                  <button className="btn btn-warning btn-sm text-white" onClick={()=>this.handleDetail(index)}>Details</button>
                              </div>
                          </div>  
                        );
                        }
                        )} 
                        {view?(
                            <React.Fragment>
                                <h3>Product Detail  <br/> {products[Index].product}</h3>
                                <ul>
                                   {products[Index].sales.map(opt=><li>{opt}</li>)}
                                </ul>
                            </React.Fragment>
                        ):""} 
            </div>

        );
    }
}

export default JsonTask;