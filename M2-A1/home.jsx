import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
class Home extends Component{
    state={
         image:"https://media.istockphoto.com/photos/row-of-books-on-a-shelf-multicolored-book-spines-stack-in-the-picture-id1222550815?b=1&k=20&m=1222550815&s=170667a&w=0&h=MTxBeBrrrYtdlpzhMpD1edwLYQf3OPgkNeDEgIzYJww=",
    }
    handleSearch=()=>{
        let input = document.getElementById("search").value;
        this.state.search= input;
        console.log(this.state.search);
        let queryParams = queryString.parse(this.props.location.search);
        this.handleOptionChange(queryParams);
    }
    callURL=(url,options)=>{
        let searchStrings = this.makeSearchString(options);
        this.props.history.push({
            pathname:url,
            search:searchStrings,
        });
    }
    makeSearchString=(options)=>{
        let {search,page,section}= options;
       search=this.state.search;
        let searchStr = "";
        searchStr = this.addToQueryString (searchStr,"q",search);
        console.log(searchStr);
        return searchStr;
        }
        addToQueryString=(str,paramName,paramValue)=>
            ((paramValue)
            ?((str)
                ? (`${str}&${paramName}=${paramValue}`)
                :(`${paramName}=${paramValue}`))
            :(str));
        
    handleOptionChange=(options)=>{ 
        this.callURL("/books",options);

    }
    render(){
    return (
        <div className="container">
        <div className="row">
            <div className="col-4"></div>
            <div className="col-4 mt-5"><img className="rounded-circle" src={this.state.image} /></div>
            <div className="col-4"></div>
        </div>
         <div className="row">
         <div className="col-1"></div>
         <div className="col-7 mt-5"> 
         <div className="form-group">
                        <input 
                        type="text"
                        className="form-control"
                        id="search"
                        name="search"
                        placeholder="Enter Search Text"
                        
                        />
                        </div>
                        </div>
                        
         <div className="col-1 mt-5"><button className="btn btn-primary" onClick={()=>this.handleSearch()} >Submit</button></div>
         </div>
         </div>
    
    );
}
}
export default Home;