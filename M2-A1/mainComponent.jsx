import React,{Component} from "react";
import {Route,Switch,Redirect } from"react-router-dom";
import Nav from "./navbar";
import Home from "./home";

import queryString from "query-string";
import MyBooks from "./mybook";
import Settings from "./setting";
import Books from "./books";

class MainComponent extends Component{

    render(){
        return(
           <React.Fragment>
                <Nav />
                <Switch>
                    <Route path="/books" component={Books}/> 
                    <Route path="/home" component={Home}/> 
                    <Route path="/mybooks" component={MyBooks}/> 
                    <Route path="/settings" component={Settings}/> 
                    <Redirect from="/" to="/home"/>
                </Switch>
                </React.Fragment>
           
        );
       
    }
}
export default MainComponent;