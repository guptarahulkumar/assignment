
import React,{Component} from "react";
import {Link} from "react-router-dom";
class Nav extends Component{
    render(){
            return(
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <Link className="navbar-brand text-dark " to="/home">
                      <h3 className="mx-4"><i class="fas fa-book-open"></i></h3>
                    </Link>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto " style={{fontWeight:"bold"}}>
                            <li className="nav-item ">
                                <Link className="nav-link"  to="/books?q=Harry%20Potter">
                                 Harry Potter
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/books?q=Agatha%20Christie">
                                Agatha Christie
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/books?q=MahaDevi%20Verma">
                                MahaDevi Verma
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/books?q=Tulsidas">
                               Tulsidas
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/mybooks">
                               My Books
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/settings">
                               Settings
                                </Link>
                            </li>
                        </ul>
                    </div>

                </nav>
            );
        }
}
export default Nav;