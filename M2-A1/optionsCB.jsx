import React,{Component} from "react";
class OptionsCB extends Component{
    handleChange = (e)=>{
        const {currentTarget  : input}=e;
        let options = {...this.props.options};
        if(input.name === "language" || input.name === "filter"||input.name==="printType")
        options[input.name]=this.updateCBs(
            options[input.name],
            input.checked,
            input.value
        );
        else options[input.name] = input.value;
        console.log("OptionsCB",options);
        this.props.onOptionChange(options);
    }

    updateCBs=(inpValues,checked,value)=>{

        return value;
    }
    makeCheckboxes=(arr,values,name,label)=>(
        <React.Fragment>
             <div className="row border bg-light mt-5">
               <div className="col p-2 " >
                {label}
                   </div>
               </div>
               <div className="row ">
            {arr.map((opt)=>(
                <div className="col-12 border p-2">
                <div className="form-check ">
                    <input
                    className="form-check-input "
                    value={opt}
                    type="radio"
                    name={name}
                    checked={values.find((val)=>val===opt)}
                    onChange={this.handleChange}
                    />
                    <label className="form-check-label" style={{textTransform:"capitalize"}}>{opt}</label>
                </div>
                </div>
            ))}
    </div>
    </React.Fragment>
    );
    render(){
       
        let {orderBy,languages,filters,printTypes,search1,onOptionChange}=this.props;
        let {language="",filter="",printType=""}=this.props.options;
        return(
            <div className="row ">
                <div className="col-12">
                {this.makeCheckboxes(languages,language.split(","),"language","Language")}
                </div>
                <div className="col-12">
                {this.makeCheckboxes(filters,filter.split(","),"filter","Filter")}
                </div>
                <div className="col-12">
                {this.makeCheckboxes(printTypes,printType.split(","),"printType","Print Type")}
                </div>

                <div className="col-12">
                <div className="row mt-5">
               <div className="col-12 p-2 border bg-light">Order By</div>
               <div className="col-12 p-2 border  ">
                <div  className="form-group">
                    <select className="form-control" name ="orderBy" id="orderby" value={orderBy}  onChange={this.handleChange}>
                    <option value="">None</option>
                        {orderBy.map((pr)=>(
                         <option >{pr}</option>
                        ))}
                    </select>
                </div>
                </div>
                </div>
                </div>
            </div>
        );
    }
};
export default OptionsCB;
