import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import OptionsCB from "./optionsCB";
import http from "./http";
import react from "react";
class Books extends Component{
    state={
        data:{},
        mybooks:[],
        start:1,
        end:10,
        search:this.props.location.search,
    OrderBy:["newest","relevance"],
    languages:["en","fr","hi","es","zh"],
    filters:["Full Volume","Partial Volume","Free Google e-Books","Paid Google e-Books"],
    printTypes:["All","Books","Magazines"],
    Index:-1,
}

    async fetchData(){
        let queryParams = queryString.parse(this.props.location.search);

       let searchStr = this.makeSearchString(queryParams);
        console.log(searchStr);
        let response =await http.get(`${searchStr}`);
        console.log(response,`${searchStr}`);
        let {data}=response;
        this.setState({data:data});
    }
     componentDidMount(){
         this.fetchData();
        
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
             this.fetchData();
        }
    } 
    shouldComponentUpdate(prevProps,prevState) {
        if(prevProps!==this.props){
            this.fetchData();
       }
       return true;
    } 
    handlePage=(incr)=>{
        let queryParams = queryString.parse(this.props.location.search);
        let {startIndex = "0",maxIndex="8",search=this.state.search,language="",filter="",printType=""} = queryParams;
        let s1 = {...this.state};
        if(incr===1){
            s1.start=s1.start+maxIndex;
            queryParams.startIndex= queryParams.startIndex+maxIndex;
            queryParams.maxIndex =  queryParams.maxIndex+maxIndex;
            s1.end = s1.end+maxIndex;
        }else{
            s1.start=s1.start-maxIndex;
            queryParams.startIndex= queryParams.startIndex-maxIndex;
            queryParams.maxIndex =  queryParams.maxIndex-maxIndex;
            s1.end = s1.end-maxIndex;
        }
        this.callURL("/books",queryParams);
       
    }
    callURL=(url,options)=>{
        let searchStrings = this.makeSearchString(options);
        this.props.history.push({
            pathname:url,
            search:searchStrings,
        });
    }
   

    makeSearchString=(options)=>{
        let {search,startIndex,maxIndex,language,filter,printType}= options;

       search=this.state.search;
        let searchStr = "";
        searchStr = this.addToQueryString (searchStr,"q",search);
        searchStr = this.addToQueryString (searchStr,"startIndex",startIndex);
        searchStr = this.addToQueryString (searchStr,"maxIndex",maxIndex);
        searchStr = this.addToQueryString (searchStr,"langRestrict",language);
        searchStr = this.addToQueryString (searchStr,"filter",filter);
        searchStr = this.addToQueryString (searchStr,"printType",printType);
        console.log(searchStr);
        return searchStr;
        }
        addToQueryString=(str,paramName,paramValue)=>
            ((paramValue)
            ?((str)
                ? (`${str}&${paramName}=${paramValue}`)
                :(`${paramName}=${paramValue}`))
            :(str));
        
        handleOptionChange=(options)=>{
            options.search=this.state.search;
            options.startIndex = "0";   
            options.maxIndex="8";
            this.callURL("/books",options);

        }
        addMybook=(index)=>{
            let s1 = {...this.state};
            let book = s1.data.items[index];
            s1.mybooks.push(book);
            s1.Index=index;
            this.setState(s1);
        }
        removebook=(index)=>{
            let s1 = {...this.state};
            let book = s1.data.items[index];
            let index1=s1.mybooks.findIndex(pr=>pr.volumeInfo.title===s1.data.items[index].volumeInfo.title);
            s1.mybooks.splice(index,1);
            s1.Index=-1;
            this.setState(s1);
        }
    render(){  
        console.log(this.state.search,"Rahul");
        const {data,OrderBy,sections,search,languages,filters,printTypes,mybooks,Index,start,end} = this.state;
        console.log(mybooks);
        let queryParams = queryString.parse(this.props.location.search);
        const {items=[],totalItems}= data;
     return (
        <div className="container">
             <div className="row">
               <div className="col-3">
               <OptionsCB options={queryParams} search1={search} languages={languages} orderBy={OrderBy} filters={filters} printTypes={printTypes} onOptionChange={this.handleOptionChange}/>
               </div>

               <div className="col-9 p-1">
                   <h3 className="text-center ">{search.substring(3).replace("%20"," ")} Books</h3>
        <h6 className="mx-5"> {start} to {end}</h6>
            <div className="row mx-5 text-center">
                    {items.map((pr,index)=>
                <div className="col-3 border border-white" style={{backgroundColor: "lightgreen",fontWeight:"bold"}}>
                  <p className="text-justify">
                      <img className="image m-2" src={pr.volumeInfo.imageLinks.smallThumbnail}/><br/>
                  <h6>{pr.volumeInfo.title}</h6>
                  {pr.volumeInfo.authors}</p>
                  <a style={{textDecoration:"none"}} href={pr.volumeInfo.publisher}>{pr.volumeInfo.categories}</a><br/>


                  {console.log((mybooks.findIndex(opt=>opt.volumeInfo.title===pr.volumeInfo.title)))}
                  {mybooks.findIndex(opt=>opt.id===pr.id)?
                  (
                  <React.Fragment>
                       <button className="btn btn-primary btn-sm" onClick={()=>this.addMybook(index)}>Add to MyBooks</button>
                  </React.Fragment>):
                   <React.Fragment>
                       <button className="btn btn-secondary btn-sm" onClick={()=>this.removebook(index)}>Remove from MyBooks</button>
                 
                  </React.Fragment>}
                  
                    </div>
            )}
            </div>
           
          
            <div className="row">
                <div className="col-2 mx-5">
                    {start>1?<button className="btn btn-primary btn-sm" onClick={()=>this.handlePage(-1)}> Prev</button>:""}
                </div>
                <div className="col-5"></div>
                <div className="col-2 mx-3">
                {end<totalItems?<button className="btn btn-primary btn-sm" onClick={()=>this.handlePage(1)}> Next</button>:""}
                </div>
            </div>
            </div>
            </div>
            </div>
     );
    }
}
export default Books;