import React,{Component} from "react";
import {Link} from "react-router-dom";
class Settings extends Component{
    state={
        Value:10,
        print:true,
        language:true,
        filter:true,
        orderBy:true,
    }

    handleChange = (e)=>{
        const {currentTarget  : input}=e;
        let s1={...this.state};
        if(input.name==="printType"){
            s1.print===true?s1.print=false:s1.print=true;
        }
        if(input.name==="language"){
            s1.language===true?s1.language=false:s1.language=true;
        }
        if(input.name==="filter"){
            s1.filter===true?s1.filter=false:s1.filter=true;
        }
        if(input.name==="orderBy"){
            s1.orderBy===true?s1.orderBy=false:s1.orderBy=true;
        }
        this.setState(s1);

    }

    render(){
        let {Value,print,language,filter,orderBy}=this.state;
    
        return(
          <React.Fragment>
                <h5 className="text-danger mt-5">Select Options for Filtering on Left Panel</h5>
                <div className="form-check mx-5 ">
                    <input
                    className="form-check-input "
                    type="checkbox"
                    name="printType"
                    checked={print}
                    onChange={this.handleChange}
                    />
                    <label className="form-check-label" style={{textTransform:"capitalize"}}>
                        printType--(Restrict to books or magazines).
                    </label>
                </div>
                <div className="form-check mx-5 ">
                    <input
                    className="form-check-input "
                    type="checkbox"
                    name="language"
                    checked={language}
                    onChange={this.handleChange}
                    />
                    <label className="form-check-label" style={{textTransform:"capitalize"}}>
                        languages--(Restricts the volumes returned to those that are tagged with the specified language).
                    </label>
                </div>
                <div className="form-check mx-5 ">
                    <input
                    className="form-check-input "
                    type="checkbox"
                    name="filter"
                    checked={filter}
                    onChange={this.handleChange}
                    />
                    <label className="form-check-label" style={{textTransform:"capitalize"}}>
                        filter--(Filter search results by volume type and availabilty).
                    </label>
                </div>
                <div className="form-check mx-5 ">
                    <input
                    className="form-check-input "
                    type="checkbox"
                    name="orderBy"
                    checked={orderBy}
                    onChange={this.handleChange}
                    />
                    <label className="form-check-label" style={{textTransform:"capitalize"}}>
                        orderBy--(Order of the volume search results).
                    </label>
                </div>
                <h5 className="text-success m-3">No of entries on a page</h5>
                <div className="row">
                    <div className="col-4 mx-4">
                <div className="form-group">
                        <input 
                        type="text"
                        className="form-control"
                        id="entry"
                        name="entry"
                        value={this.state.Value}
                        onChange={this.handleChange}
                        />
                        </div>
                        </div>
                        </div>
                </React.Fragment>
                );
    }
    
}
export default Settings ;