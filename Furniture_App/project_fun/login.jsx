import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
import auth from "./auth";
class Login extends Component{
    state={
        form:{email:"",password:""},
        errors:{},
    };
    
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        console.log(input.value,input);
        if(input.name==="password"){
            let str = input.value;
            console.log(input);
            if(str.length>=7||str.length==0){
                s1.form[input.name] = input.value;
                s1.errors.password="";
                console.log(input);
            }else{
                s1.form[input.name] = input.value;
                s1.errors.password="Password must be of 7 characters";
                console.log("Password must be of 7 characters",str);
                console.log(input);
            }
        }else
        s1.form[input.name] = input.value;
        this.setState(s1);
    };
    async login(url,obj){
        try{
        let response = await http.post(url,obj);
        let {data} = response;
        console.log(response);
        auth.login(data);

        //this.props.history.push("/checkOut");
        window.location="/products";
        }
        catch(ex){
            if(ex.response && ex.response.status===500){
               
                    alert("Invalid username & password");
                let errors = {};
                errors.email= ex.response.data;
                this.setState({errors:errors});
            }
        }

    }
    handleSubmit = (e)=>{
        e.preventDefault();
        const {form} = this.state;
        console.log("Rahul");
        this.login("/login",this.state.form);
    };
   
    render(){
      let {email,password}=this.state.form;
      let {errors =null}=this.state;
      //console.log(errors);
      return (
            <div className="container">
                <div className="row">
                    <div className="col-9">
                    <h2 className=" text-center">Login</h2>   
               <div className="form-group">
               <label>Email address</label>
                   <input 
                   type="email"
                   className="form-control"
                   id="email"
                   name="email"
                   placeholder="Enter your email"
                   value ={email}
                   onChange={this.handleChange}
                   />
                   <p className="text-center "><small>We'll never share your email with anyone else.</small></p>
                  
               </div>
               </div>
               </div>
               <div className="row">
                            <div className="col-9">
               <div className="form-group">
               <label>Password</label>
                   <input 
                   type="password"
                   className="form-control"
                   name="password"
                   id="password"
                   placeholder="Password"
                   value ={password}
                   onChange={this.handleChange}
                   /><br/>
                   
               </div>
               </div>
               </div>

              <button className="btn btn-primary m-3" onClick={this.handleSubmit}>Login</button>
               </div>
        );

    }
}
export default Login;