import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import http from "./http";
import auth from "./auth";
import LeftPanel from "./leftPanel";
class Products extends Component{
    state={
        products:[],
        courses:["Dining","Drawing","Bedroom","Study"],
        imgBtn1:true,
        show:{},
    }
    //Make a request to the server
    //Server will compute the response
    //Send back the response

    async fetchData(){
      
        let searchStr = "";
        console.log(`/products/${searchStr}`);
        let response =await http.get(`/products/${searchStr}`);
        console.log(response);
        let {data}=response;
        this.setState({products:data});
    }
     componentDidMount(){
        this.fetchData();
       
   }
   componentDidUpdate(prevProps,prevState){
       if(prevProps!==this.props){
            this.fetchData();
       }
   }
   callURL=(url,options)=>{
    let searchStrings = this.makeSearchString(options);
    console.log(url,searchStrings);
    this.props.history.push({
        pathname:url+'/'+searchStrings,
        search:"",
    });
}
makeSearchString=(options)=>{
    let {category}= options;
    let searchStr = "";
    searchStr = this.addToQueryString (searchStr,category);
    console.log(searchStr);
    return searchStr;
    }
    addToQueryString=(str,paramValue)=>
        ((paramValue)
        ?((str)
            ? (`${paramValue}`)
            :(`${paramValue}`))
        :(str));
        handleOptionChange=(options)=>{
            console.log(options);
            this.callURL("/products",options);
        } 
addToCart=(prodCode)=>{
    let {cart}=this.props;
    let index= this.state.products.findIndex(p=>{return p.prodCode===prodCode});
    console.log(index);
    let data = this.state.products[index];
    this.props.onCartChange(data);

}
removeCart=(index)=>{
    let {cart}=this.props;
    let data = this.state.products[index];
    this.props.onCart1Change(data);

}
editProduct=(prodCode)=>{
    this.props.history.push(`/products/${prodCode}/edit`);
}
deleteProduct=(prodCode)=>{
    this.props.history.push(`/products/${prodCode}/delete`);
}
incr=(index)=>{
    this.props.onCartIncr(index);

}
decr=(index)=>{
    this.props.onCartDecr(index);
}
showImg=(index)=>{
    let s1 ={...this.state};
    let product = s1.products[index];
    s1.show=product;
    this.setState(s1);
}
    render(){
        const {products,courses,imgBtn1,show}=this.state;
        const {cart} =this.props;
        const user = auth.getUser();
        let queryParams = queryString.parse(this.props.location.search);
        return(
            <div className="container">
                   <div className="row">
                       <div className="col-2">
                        <LeftPanel options={queryParams}  courses={courses}  onOptionChange={this.handleOptionChange}/>
                       </div>
                       <div className="col-6">
                       <div className="row mx-2" >
                {products.map((pr,index)=>(
                   <React.Fragment>
                        <div className="card p-2 m-2" style={{width: "15rem"}} >
                            <Link to={`/products/${pr.category}/${pr.prodCode}`}>      
  <img src={pr.img} className="card-img-top" alt="..." onClick={()=>this.showImg(index)}/></Link>
</div>
                   </React.Fragment>
                ))}
            </div>
            </div>
            <div className="col-4 mt-5">
                {user&&(user.role==="admin")&& show.img?
                <React.Fragment>
                    <Link className="btn btn-secondary m-3" to={`/products/${show.prodCode}/edit`}>Edit Product</Link>
                    <Link className="btn btn-secondary m-3" to={`/products/${show.prodCode}/delete`}>Delete Product</Link>
                </React.Fragment>
                :""}
                    {show.img?
                    (<React.Fragment>
                    <img width="300px" src={show.img}/>
                    <h3>{show.title}</h3>
                   {show.desc.map(p=>{return <p className="m-0">{p}</p>})}
                        <h5>Item in product</h5>
                        {show.ingredients.map(p=>
                            (<p className="m-0">{"-"+p.ingName+":"+p.qty}</p>)
                            )}
                            {user&&(user.role==="user")?cart.find(p=>p.prodCode===show.prodCode)?
  <div className="row">
  <div className="col-2 text-center"></div>
  <div className="col-8">
      <button className="btn btn-success" onClick={()=>this.incr(cart.findIndex(p=>p.prodCode==show.prodCode))}>+</button>
      <span className="m-3 text-dark">{cart[cart.findIndex(p=>p.prodCode==show.prodCode)].qty}</span>
      <button className="btn btn-warning" onClick={()=>this.decr(cart.findIndex(p=>p.prodCode==show.prodCode))}>-</button>
      </div>
  <div className="col-2"></div>
  </div>
  :<button className="btn btn-success" onClick={()=>this.addToCart(show.prodCode)}>Add to Cart</button> :""}
                    </React.Fragment>
                    )
                    :<h4 className="text-dark">Choose a product! </h4>}
                       </div>
            </div>
                   </div>
        );

    }
}
export default Products;