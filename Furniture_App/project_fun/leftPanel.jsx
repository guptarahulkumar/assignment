import React,{Component} from "react";
import {Link} from "react-router-dom";
class LeftPanel extends Component{
    handleChange = (e)=>{
        const {currentTarget  : input}=e;
        let options = {...this.props.options};
        if(input.name === "category"){
        options[input.name]=this.updateCBs(
            options[input.name],
            input.checked,
            input.value
        );
        console.log("LeftPanel",options);
        this.props.onOptionChange(options);
        }   
    }
    updateCBs=(inpValues,checked,value)=>{
        return value;
    }
    makeCheckboxes=(arr,values,name,label)=>(
        <React.Fragment>
             <div className="row border bg-light mt-5">
               <div className="col p-2 " >
                {label}
                   </div>
               </div>
               <div className="row ">
            {arr.map((opt)=>(
                <div className="col-12 border p-2">
                <div className="form-check ">
                    <input
                    className="form-check-input "
                    value={opt}
                    type="radio"
                    name={name}
                    checked={values.find((val)=>val===opt)}
                    onChange={this.handleChange}
                    />
                    <label className="form-check-label" style={{textTransform:"capitalize"}}>{opt}</label>
                </div>
                </div>
            ))}
    </div>
    </React.Fragment>
    );
  
    render(){
        let {category=""}=this.props.options;
        let {courses}=this.props;
        return(
            <div className="row">
                <div className="col-9">
                {this.makeCheckboxes(courses,category.split(","),"category","Options")}
                </div>
            </div>
        );
    }
};
export default LeftPanel;
