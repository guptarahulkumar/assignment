import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class AddProducts extends Component{
    state={
        user:{prodCode:"",title:"",img:"",category:"",name:"",desc:[],ingredients:[]},
        errors:{},
        categories:["Drawing","Dining","Bedroom","Study"],
    };
    
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        if(input.name==="desc"){
            s1.user.desc=input.value;
        }
         if(input.name==="ingredients"){
            s1.user.ingredients.push(input.value);
                    }
        else{
        s1.user[input.name] = input.value;
        console.log(s1.user);
        }
        this.setState(s1);
    };
    async postData(url,obj){
        let response = await http.post(url,obj);
        console.log(response);
        this.props.history.push("/products");
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        let {user} = this.state;
        alert("Product Added Successfully");
        this.postData("/register",user);
    };

    render(){
      let {prodCode,title,img,category,name,desc,ingredients}=this.state.user;
      const {errors} = this.state;
      return (
            <div className="container">
                 <div className="row">
                <div className="col-6">
              
               <div className="form-group mt-3">
                   <label>ProductCode</label>
                   <input 
                   type="text"
                   className="form-control"
                   id="prodCode"
                   name="prodCode"
                   value ={prodCode}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group mt-3">
                   <label>Name</label>
                   <input 
                   type="text"
                   className="form-control"
                   id="title"
                   name="title"
                   value ={title}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group mt-3">
                   <label>Image URL</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="img"
                   id="img"
                   value ={img}
                   onChange={this.handleChange}
                   />
                   
               </div>
               <div  className="form-group mt-3 ">
                    <select className="form-control" name ="category" id="category" value={category}  onChange={this.handleChange}>
                        <option >Select Category</option>
                        {this.state.categories.map((pr)=>(
                         <option >{pr}</option>
                        ))}
                    </select>
                </div>
                <button className="btn mt-4 btn-secondary">Add Description</button>
                <div className="row">
           <div className="col-10">
            <div className="form-group mt-3">
            <input 
            type="text"
            className="form-control"
            name="desc"
             id="desc"
            value ={desc}
             onChange={this.handleChange}
            />
             </div>
            </div>
           
                <div className="col-2  mt-3"><h2 className="btn btn-danger">X</h2></div>
                </div>
                
               <button className="btn mt-4 btn-secondary" >Add Item Shipped with product</button>
                       <div className="row">
                           <div className="col-7">
                    <div className="form-group mt-3">
                    <input 
                    type="text"
                    className="form-control"
                    name="ingName"
                    id="ingName"
                    value ={ingredients.ingName}
                    onChange={this.handleChange}
                    />
                </div>
                </div>
                <div className="col-3">
                 <div className="form-group mt-3">
                 <input 
                 type="text"
                 className="form-control"
                 name="ingName"
                 id="ingQty"
                 value ={ingredients.qty}
                 onChange={this.handleChange}
                 />
                 </div>
             </div>
             <div className="col-2  mt-3"><h2 className="btn btn-danger">X</h2></div>
             </div>
                      
               <button className="btn mt-4 btn-primary" onClick={this.handleSubmit}>Add</button>
               </div>
               <div className="card p-2 m-2" style={{width: "25rem"}}>
  {img?<img src={img} className="image" alt="..."/>:""}
  </div>
    
  </div>
            </div>
        );

    }
}
export default AddProducts;