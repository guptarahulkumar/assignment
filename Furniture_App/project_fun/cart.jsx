import React,{Component} from "react";
import {Link} from "react-router-dom";
class Cart extends Component{
    incr=(index)=>{
        this.props.onCartIncr(index);
    }
    decr=(index)=>{
        this.props.onCartDecr(index);
    }
    render(){
        const {cart} = this.props;
        return(
            <div className="container">
               <div className="row">
                <div className="col">
                        <h4 className="" style={{textAlign:"center"}}>Products in Shopping Cart</h4>
                        </div>
                        {cart.map((p,index)=>(
                            <div className="row mt-1 border">
                            <div className="col-2"><img className="img-fluid rounded" width="70px"  src={p.img} /></div>
                            <div className="col-6">
                                <h6>{p.title}</h6><br/>
                            </div>
                            <div className="col-4">
                            <button className="btn btn-danger btn-small" onClick={()=>this.decr(index)}>-</button>
                            <span className="m-3">{p.qty}</span>
                                <button className="btn btn-success btn-small" onClick={()=>this.incr(index)}>+</button>    
                            </div>
                        </div>
                        ))}
                        <h4 className="" style={{textAlign:"center"}}>List of Items in Cart</h4>
                        <div className="row">
                        <div className='col-3'></div>
                        <div className='col-6 '>
                                <div className='row bg-dark text-light text-center'>
                                    <div className="col-6">Item Name</div>
                                    <div className="col-6">Count</div>
                                </div>
                        </div>
                        </div>
                        {cart.map((p,index)=>(
                            
                            <div className="row">
                        <div className='col-3'></div>
                        <div className='col-6 '>
                            {p.ingredients.map(pr=>(
                                <div className='row  text-center'>
                                <div className="col-6 border">{pr.ingName}</div>
                                <div className="col-6 border">{p.qty*pr.qty}</div>
                            </div>
                            ))}
                                
                        </div>
                        </div>
                        ))}

               </div>
               </div>
        );
        }

}
export default Cart;