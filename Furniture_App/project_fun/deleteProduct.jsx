import React,{Component} from "react";
import http from "./http";
class DeleteProduct extends Component{
    async componentDidMount(){
        const {prodCode} = this.props.match.params;
        let response = await http.deleteApi(`/products/${prodCode}`);
        this.props.history.push("/products");
    }
    render(){
        console.log("Rahul");
        return "";
    }
}
export default DeleteProduct;