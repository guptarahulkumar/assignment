import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class EditProduct extends Component{
    state={
        user:{prodCode:"",title:"",img:"",category:"",name:"",desc:[],ingredients:[]},
        categories:["Drawing","Dining","Bedroom","Study"],
    };
    async fetchData(){
        const {prodCode} = this.props.match.params;
        let response =await http.get(`/Rproducts/${prodCode}`);
        console.log(response);
        let {data}=response;
        this.setState({user:data});
    }
     componentDidMount(){
    this.fetchData();      
   }
   async putData(url,obj){
    let response = await http.put(url,obj);
    console.log(response);
    this.props.history.push("/products");
}
   
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        s1.user[input.name] = input.value;
        console.log(s1.user);
        this.setState(s1);
    };
    deleteProduct=()=>{
        const {prodCode} = this.props.match.params;
        this.props.history.push(`/products/${prodCode}/delete`);
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        let {user} = this.state;
        const {prodCode} = this.props.match.params;
        alert("Product Added Successfully");
        this.putData(`/products/${prodCode}`,user);
    };
    render(){
      let {prodCode="",title="",img="",category="",name="",desc=[],ingredients=[]}=this.state.user;
      return (
            <div className="container">
                <h2 className="text-center">Edit Product</h2>
                <div className="row">
                <div className="col-6">
              
               <div className="form-group mt-3">
                   <label>ProductCode</label>
                   <input 
                   type="text"
                   className="form-control"
                   id="prodCode"
                   name="prodCode"
                   value ={prodCode}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group mt-3">
                   <label>Name</label>
                   <input 
                   type="text"
                   className="form-control"
                   id="title"
                   name="title"
                   value ={title}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group mt-3">
                   <label>Image URL</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="img"
                   id="img"
                   value ={img}
                   onChange={this.handleChange}
                   />
                   
               </div>
               <div  className="form-group mt-3 ">
                    <select className="form-control" name ="category" id="category" value={category}  onChange={this.handleChange}>
                        <option selected disabled>Select Category</option>
                        {this.state.categories.map((pr)=>(
                         <option >{pr}</option>
                        ))}
                    </select>
                </div>
                <button className="btn mt-4 btn-secondary">Add Description</button>
                
                {desc.map((p,index)=>(
                    <div className="row">
                    <div className="col-10">
                    <div className="form-group mt-3">
                    <input 
                    type="text"
                    className="form-control"
                    name={p}
                    id={p}
                    value ={p}
                    onChange={this.handleChange}
                    />
                </div>
                </div>
                <div className="col-2  mt-3"><h2 className="btn btn-danger">X</h2></div>
                </div>
                ))}
               <button className="btn mt-4 btn-secondary" >Add Item Shipped with product</button>
             
               {ingredients.map((p,index)=>(
                   <React.Fragment>
                       <div className="row">
                           <div className="col-7">
                    <div className="form-group mt-3">
                    <input 
                    type="text"
                    className="form-control"
                    name={p.ingName}
                    id={p.ingName}
                    value ={p.ingName}
                    onChange={this.handleChange}
                    />
                </div>
                </div>
                <div className="col-3">
                 <div className="form-group mt-3">
                 <input 
                 type="text"
                 className="form-control"
                 name={p.ingName}
                 id={p.qty}
                 value ={p.qty}
                 onChange={this.handleChange}
                 />
                 </div>
             </div>
             <div className="col-2  mt-3"><h2 className="btn btn-danger">X</h2></div>
             </div>
             </React.Fragment>
               ))}
               
                      
               <button className="btn mt-4 btn-primary" onClick={this.handleSubmit}>Save</button>
               <button className="btn mt-4 border-warning" onClick={()=>this.deleteProduct()}>Delete</button>
               </div>
               <div className="col-4 mx-5">
                <div className="card p-2 m-2" style={{width: "25rem"}}>
  <img src={img} className="image" alt="..."/>
  
    
  </div>
                </div>
                </div>
               </div>
          
        );

    }
}
export default EditProduct;