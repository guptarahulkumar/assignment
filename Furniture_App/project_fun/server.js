let express = require("express");
let app=express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header(
        "Access-Control-Allow-Methods",
        "GET,POST,OPTIONS,PUT,PATCH,DELETE,HEAD"
    );
    res.header(
        "Access-Control-Allow-Headers",
        "Origin,X-Requested-With,Content-Type,Accept"
    );
    next();
});
const port = 2410;
app.listen(port,()=>console.log(`Node app listening on port ${port}! `));

let {data} = require("./data.js");
let users = [{email:"admin@admin.com",password:"admin123",role:"admin"},{email:"user@user.com",password:"user123",role:"user"}];
let orders=[];

app.get("/products/:category",function(req,res){
    let category = req.params.category;
    console.log(req.params);
    if(category){
        let product  =data.filter((st)=>st.category===category);
        console.log(product,category);
       res.send(product);
    }else
    res.send(data);
});
app.get("/products",function(req,res){

    res.send(data);
});


app.get("/Rproducts/:prodCode",function(req,res){
    let prodCode = req.params.prodCode;
    console.log(data);
    let product =data.find((s)=>prodCode==s.prodCode);
    console.log(product,prodCode);
   res.send(product);
});

app.post("/register",function(req,res){
    console.log(req.body);
    data.push({...req.body});
    console.log(data);
    res.send(data);
});
    app.post("/login", function(req, res) {
    //find that customer
    console.log(req.body);
    let custObj = users.find(
      item => item.email === req.body.email && item.password === req.body.password
    );
    console.log(custObj);
    let resObj = null;
    if (custObj != undefined) {
      resObj = {
        name: custObj.name,
        email: custObj.email,
        role: custObj.role
      };
      res.status(200).send(resObj);
    } else res.status(500).send("Login Unsuccessful");
  });
  app.post("/orders", function(req, res) {
    //find that customer
    console.log(req.body);
    orders.push(req.body);
    console.log(orders);
  res.status(200).send("Order Submitted"); 
  });
  app.get("/orders",function(req,res){
      console.log(orders);
      res.send(orders);
  })
app.put("/products/:prodCode",function(req,res){
    let prodCode= req.params.prodCode;
    let body = req.body;
    console.log(body);
    let index = data.findIndex((st)=>st.prodCode==prodCode);
    if(index>=0){
    let updatedcar = {prodCode:prodCode,...body};
    data[index]=updatedcar;
    res.send(updatedcar);
    }else
    res.status(404).send("No Product found");

});
app.delete("/products/:prodCode",function(req,res){
    let prodCode= req.params.prodCode;
    let index = data.findIndex((st)=>st.prodCode==prodCode);
    data.splice(index,1);
    res.send(data);
})


