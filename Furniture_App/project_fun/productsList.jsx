import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import http from "./http";
import auth from "./auth";
class ProductsList extends Component{
    state={
        products:[],
        search:"",
        main:[],
    }
    //Make a request to the server
    //Server will compute the response
    //Send back the response

    async fetchData(){
        let queryParams = queryString.parse(this.props.location.search);
        let searchStr = this.makeSearchString(queryParams);
        let response =await http.get(`/products/${searchStr}`);
        console.log(response);
        let {data}=response;
        this.setState({products:data});
    }
     componentDidMount(){
        this.fetchData();
       
   }
   componentDidUpdate(prevProps,prevState){
       if(prevProps!==this.props){
            this.fetchData();
       }
   }
   callURL=(url,options)=>{
    let searchStrings = this.makeSearchString(options);
    this.props.history.push({
        pathname:url,
        search:searchStrings,
    });
}
makeSearchString=(options)=>{
    let {category}= options;
    let searchStr = "";
    searchStr = this.addToQueryString (searchStr,category);
    return searchStr;
    }
    addToQueryString=(str,paramValue)=>
        ((paramValue)
        ?((str)
            ? (`${paramValue}`)
            :(`${paramValue}`))
        :(str));
        handleOptionChange=(options)=>{
            this.callURL("/products",options);
        } 
addToCart=(index)=>{
    let {cart}=this.props;
    let data = this.state.products[index];
    this.props.onCartChange(data);

}
removeCart=(index)=>{
    let {cart}=this.props;
    let data = this.state.products[index];
    this.props.onCart1Change(data);

}
editProduct=(id)=>{
    this.props.history.push(`/products/${id}/edit`);
}
deleteProduct=(id)=>{
    this.props.history.push(`/products/${id}/delete`);
}
handleChange=(e)=>{
    const {currentTarget : input}= e;
    let s1 = {...this.state};
    s1.search = input.value;
    if(s1.main.length==0)
    {
        s1.main=s1.products;   
    }
    else
    s1.products=s1.main.filter(p=>p.name.startsWith(s1.search));
    this.setState(s1);
};
    render(){
        const {products,search}=this.state;
        const {cart} =this.props;
        const user = auth.getUser();
        let queryParams = queryString.parse(this.props.location.search);
        return(
            <div className="container">
                <div className="row">
                    <div className="col">
               <button className="btn btn-success mt-3 mb-3" ><Link  style={{textDecoration:"none",color:"white"}} to="/addProducts">Add a Product</Link></button><br/>
               <div className="form-group">
                   <input 
                   type="text"
                   className="form-control mb-3"
                   id="search"
                   name="search"
                   value={search}
                   placeholder="Search..."
                   onChange={this.handleChange}
                   />
               </div>
               Showing products 1 - {products.length}
               </div>
                </div>
               <div className="row bg-dark text-light">

                        <div className="col-1 border">#</div>
                        <div className="col-3 border">Title</div>
                        <div className="col-3 border">Category</div>
                        <div className="col-2 border">Price</div>
                        <div className="col-3 border "></div>
                </div>
                   
                {products.map((p,index)=>(
                    <React.Fragment>
                        {((index+1) % 2===0)?
                        <div className="row" key={p.id}>
                        <div className="col-1 border">{p.id}</div>
                        <div className="col-3 border">{p.name}</div>
                        <div className="col-3 border">{p.category}</div>
                        <div className="col-2 border">{p.price}</div>
                        <div className="col-3 border">
                            <button className="btn text-primary" onClick={()=>this.editProduct(p.id)}>Edit</button>
                            <button className="btn text-primary" onClick={()=>this.deleteProduct(p.id)}>Delete</button>
                        </div>
                          </div>
                          :
                          <div className="row bg-light" key={p.id}>
                           <div className="col-1 border">{p.id}</div>
                        <div className="col-3 border">{p.name}</div>
                        <div className="col-3 border">{p.category}</div>
                        <div className="col-2 border">{p.price}</div>
                        <div className="col-3 border">
                            <button className="btn text-primary" onClick={()=>this.editProduct(p.id)}>Edit</button>
                            <button className="btn text-primary" onClick={()=>this.deleteProduct(p.id)}>Delete</button>
                        </div>
                          </div>
                          }

                          </React.Fragment>
                ))}
          
            </div>
        );

    }
}
export default ProductsList;