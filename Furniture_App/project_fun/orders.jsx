import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
import auth from "./auth";
class Orders extends Component{
    state={
        products:[],
    }
    //Make a request to the server
    //Server will compute the response
    //Send back the response

    async fetchData(){
        let response =await http.get(`/orders`);
        console.log(response);
        let {data}=response;
        this.setState({products:data});
    }
     componentDidMount(){
        this.fetchData();
       
   }
   componentDidUpdate(prevProps,prevState){
       if(prevProps!==this.props){
            this.fetchData();
       }
    }


    render(){
        const {products}=this.state;
        console.log(products);
        const user = auth.getUser();
        return(
            <div className="container">
               <div className="row">
                   <div className="col">
                        <img className="" height="200px" width="100%" src="https://raw.githubusercontent.com/edufectcode/react/main/data/MyStore-sale.jpg"/>
                        <br/>
                        </div>
                        </div>
                        <div className="row border ">
                            <div className="col-12 text-center"><h2>List of your Orders</h2></div>
                            <div className="col-12 text-center"><p>Your Order has {products.length} items</p></div>
                            <div className="col  m-2">
                            <div className="row bg-dark text-light p-2">
                                <div className="col-3 text-center">Name</div>
                                <div className="col-2 text-center">City</div>
                                <div className="col-3 text-center">Address</div>
                                <div className="col-2 text-center">Amount</div>
                                <div className="col-2 text-center">Item</div>

                            </div>
                            {products.map(p=>(
                                <div className="row border p-2">
                                <div className="col-3 text-center">{p.name}</div>
                                <div className="col-2 text-center">{p.city}</div>
                                <div className="col-3 text-center">
                                {p.address1+','+p.address2}
                                    </div>
                                    <div className="col-2 text-center">
                                        Rs.{p.cart.reduce((acc,curr)=>
                                        {
                                            return acc+(parseInt(curr.price)*parseInt(curr.qty));
                                            },0)}.00</div>
                                <div className="col-2 text-center">
                                {p.cart.reduce((acc,curr)=>
                                        {
                                            return acc+(parseInt(curr.qty));
                                            },0)}
                                </div>
                            </div>
                            ))}
                            </div>
                        </div>
                   </div>
        );

    }
}
export default Orders;