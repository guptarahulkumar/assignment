import React,{Component} from "react";
import {Link} from "react-router-dom";
import {NavDropdown} from 'react-bootstrap';
class Navbar extends Component{
    render(){
       const {user,cart} = this.props;
       console.log(cart);
       return(
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <Link className="navbar-brand text-light " to="/products">
              <h5 className="m-2">FurnitureStore</h5>
            </Link>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link className="nav-link" to="/products">
                        Products
                        </Link>
                    </li>
                    {user && user.role==="user" && (                            
                            <li className="nav-item">
                                <Link className="nav-link" to="/cart">
                                Cart
                                </Link>
                            </li>
                            )}
                               {user && user.role==="admin" && (                            
                            <li className="nav-item">
                                <Link className="nav-link" to="/addProducts">
                               Add a New Products
                                </Link>
                            </li>
                            )}
                    {!user && (                            
                            <li className="nav-item">
                                <Link className="nav-link" to="/login">
                                <h6 style={{position:"absolute",right:"100px"}}>Signin</h6>
                                </Link>
                            </li>
                            )}
                               {user && (                            
                            <li className="nav-item">
                                <Link className="nav-link" to="/logout">
                                <h6 style={{position:"absolute",right:"100px"}}>SignOut</h6>
                                </Link>
                            </li>
                            )}
                </ul>
            </div>

        </nav>
    );
}
           
                        
        
}
export default Navbar;