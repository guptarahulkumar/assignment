import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import OptionsCB from "./optionsCB";
import http from "./http";
class Purchases extends Component{
    state={
        data:[],
        products : [
            {show:"Product1",value:1},
            {show:"Product2",value:2},
            {show:"Product3",value:3},
            {show:"Product4",value:4},
            {show:"Product5",value:5},
            {show:"Product6",value:6},
            {show:"Product7",value:7},
            {show:"Product8",value:8},
        ],
        shops : [
            {show:"Store1",value:1},
            {show:"Store2",value:2},
            {show:"Store3",value:3},
            {show:"Store4",value:4},
        ],
        sort : [
            {show:"Quantity by Ascending Order",value:"QtyAsc"},
            {show:"Quantity by Descending Order",value:"QtyDesc"},
            {show:"Value by Ascending Order",value:"ValueAsc"},
            {show:"Value by Descending Order",value:"ValueDesc"},
        ],
    }
    async fetchData(){
        let queryParams = queryString.parse(this.props.location.search);
        let searchStr = this.makeSearchString(queryParams);
        let response =await http.get(`/purchases?${searchStr}`);
        console.log(response);
        let {data}=response;
        this.setState({data:data});
    }
     componentDidMount(){
         this.fetchData();
        
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
             this.fetchData();
        }
    }
    makeSearchString=(queryParams)=>{
        let {product,shop,sort}= queryParams;
        let searchStr = "";
        searchStr = this.addToQueryString (searchStr,"shop",shop);
        searchStr = this.addToQueryString (searchStr,"product",product);
        searchStr = this.addToQueryString (searchStr,"sortby",sort);
        console.log(searchStr);
        return searchStr;
        }
        addToQueryString=(str,paramName,paramValue)=>
        ((paramValue)
        ?((str)
            ? (`${str}&${paramName}=${paramValue}`)
            :(`${paramName}=${paramValue}`))
        :(str));
        callURL=(url,options)=>{
            let searchStrings = this.makeSearchString(options);
            this.props.history.push({
                pathname:url,
                search:searchStrings,
            });
        }

        handleOptionChange=(options)=>{
            this.callURL("/purchases",options);
        } 

    sort=(num)=>{
        console.log("sort");
        let s1 ={...this.state};
        if(num==1){
            s1.data.sort((p1,p2)=>{return (+p1.purchaseId)-(+p2.purchaseId)});
        }
        if(num==2){
            s1.data.sort((p1,p2)=>{return (+p1.shopId)-(+p2.shopId)});
        }
        if(num==3){
            s1.data.sort((p1,p2)=>{return (+p1.productid)-(+p2.productid)});
        }
        if(num==4){
            s1.data.sort((p1,p2)=>{return (+p1.quantity)-(+p2.quantity)});
        }
        if(num==5){
            s1.data.sort((p1,p2)=>{return (+p1.price)-(+p2.price)});
        }
        this.setState(s1);
    }
    render(){
        let queryParams = queryString.parse(this.props.location.search);
        let {data,products,shops,sort}=this.state;
     return (
        <div className="container-fluid text-dark ">
                         <h3 className="text-center  mx-5"> Purchases Table</h3>
                         <div className="row m-2 text-center border ">
                         <div className="col-3">
                         <h5 className="text-center bg-info p-2 mt-2 text-light mx-5">Left Panel</h5>
                   <OptionsCB options={queryParams} products={products} shops={shops} sort={sort}  onOptionChange={this.handleOptionChange}/>
                   </div>
                   <div className="col-9">
                       <div className="row bg-dark text-light m-1">
                            <div className="col-2 border border-danger" onClick={()=>this.sort(1)}>Purchase Id</div>
                            <div className="col-2 border border-danger" onClick={()=>this.sort(2)}>Shop Id</div>
                            <div className="col-3 border border-danger" onClick={()=>this.sort(3)}>Product Id</div>
                            <div className="col-3 border border-danger" onClick={()=>this.sort(4)}>Quantity</div>
                            <div className="col-1 border border-danger" onClick={()=>this.sort(5)}>Price</div>
                            <div className="col-1 border border-danger">Value</div>
                            </div>
                        {data.map(pr=>
                        <div className="row m-1 bg-light text-center">
                            <div className="col-2 border border-dark">{pr.purchaseId}</div>
                            <div className="col-2 border border-dark">{pr.shopId}</div>
                            <div className="col-3 border border-dark">{pr.productid}</div>
                            <div className="col-3 border border-dark">{pr.quantity}</div>
                            <div className="col-1 border border-dark">{pr.price}</div>
                            <div className="col-1 border border-dark">{pr.quantity*pr.price}</div>
                            </div>
                   
                )}
                
     </div>
     </div>
     </div>
     );
    }
}
export default Purchases;