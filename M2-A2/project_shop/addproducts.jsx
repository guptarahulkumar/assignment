import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class AddProducts extends Component{
    state={
        product:{productName:"",category:"",description:""},
        edit:false,
    }   
    async componentDidMount(){
        this.fetchData();
    }
    async componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props)
        this.fetchData();
       
   }
   async fetchData(){
    const {productId} = this.props.match.params;
    if(productId){
        console.log(productId);
        let response = await http.get(`/products/${productId}`);
        let {data} = response;
        this.setState({product:data,edit:true});
    }else{
        console.log(productId);
        let   product={productName:"",category:"",description:""};
        this.setState({product:product,edit:false});
    }
}
    
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        s1.product[input.name] = input.value;
        this.setState(s1);
    };
    async postData(url,obj){
        let response = await http.post(url,obj);
        console.log(response);
        this.props.history.push("/products");
    }
    async putData(url,obj){
        let response = await http.put(url,obj);
        console.log(response);
        this.props.history.push("/products");
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        const {product,edit} = this.state;
        edit?this.putData(`/products/${product.productId}`,product):
        this.postData("/addproducts",product);
    };
   


    render(){
      let {productName,category,description}=this.state.product;
      return (
            <div className="container">
               <div className="form-group">
                   <label>Product Name</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="productName"
                   placeholder="Enter Product Name"
                   value ={productName}
                   onChange={this.handleChange}
                   readOnly={this.state.edit}
                   />
               </div>
               <div className="form-group">
                   <label>Category</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="category"
                   placeholder="Enter Category"
                   value ={category}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group">
                   <label>Description</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="description"
                   placeholder="Enter Description"
                   value ={description}
                   onChange={this.handleChange}
                   />
               </div>
               <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                
            </div>
        );

    }
}
export default AddProducts;