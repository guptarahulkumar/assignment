import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class ProductPurchases extends Component{
    state={
        data:[],
}
    async fetchData(){
       const {productId}=this.props.match.params;
        let response =await http.get(`/purchases/products/${productId}`);
        console.log(response);
        let {data}=response;
        this.setState({data:data});
    }
     componentDidMount(){
         this.fetchData();
        
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
             this.fetchData();
        }
    }
  

    sort=(num)=>{
        console.log("sort");
        let s1 ={...this.state};
        if(num===1){
            s1.data.sort((p1,p2)=>{return (+p1.purchaseId)-(+p2.purchaseId)});
        }
        if(num===2){
            s1.data.sort((p1,p2)=>{return (+p1.productid)-(+p2.productid)});
        }
        if(num===3){
            s1.data.sort((p1,p2)=>{return (+p1.productid)-(+p2.productid)});
        }
        if(num===4){
            s1.data.sort((p1,p2)=>{return (+p1.quantity)-(+p2.quantity)});
        }
        if(num===5){
            s1.data.sort((p1,p2)=>{return (+p1.price)-(+p2.price)});
        }
        this.setState(s1);
    }
    render(){
        let {data}=this.state;
        const {productId}=this.props.match.params;
        return (
        <div className="container-fluid bg-info ">
                         <h3 className="text-center mx-5"> Purchases Table of Product {productId} </h3>
                         <div className="row m-2 text-center bg-dark text-light border ">
                            <div className="col-2 border border-danger" onClick={()=>this.sort(1)}>Purchase Id</div>
                            <div className="col-3 border border-danger" onClick={()=>this.sort(3)}>Shop Id</div>
                            <div className="col-3 border border-danger" onClick={()=>this.sort(3)}>Product Id</div>
                            <div className="col-3 border border-danger" onClick={()=>this.sort(4)}>Quantity</div>
                            <div className="col-1 border border-danger" onClick={()=>this.sort(5)}>Price</div>
                            </div>
                        {data.map(pr=>
                        <div className="row m-1 bg-light text-center">
                            <div className="col-2 border border-dark">{pr.purchaseId}</div>
                            <div className="col-3 border border-dark">{pr.shopId}</div>
                            <div className="col-3 border border-dark">{pr.productid}</div>
                            <div className="col-3 border border-dark">{pr.quantity}</div>
                            <div className="col-1 border border-dark">{pr.price}</div>
                            </div>
                   
                )} 
     </div>
     );
    }
}
export default ProductPurchases;