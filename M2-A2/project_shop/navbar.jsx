
import React,{Component} from "react";
import {Link} from "react-router-dom";
import {NavDropdown} from 'react-bootstrap';
class Nav extends Component{
    render(){
            return(
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <Link className="navbar-brand " to="/">
                      <h5 className="m-2">MyShopPortal</h5>
                    </Link>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto ">
                        <li className="nav-item dropdown">
                            <NavDropdown title="Shops" id="navbarScrollingDropdown">
                <NavDropdown.Item ><Link className="nav-link text-dark" to='/shops'>View</Link></NavDropdown.Item>
                <NavDropdown.Item  ><Link className="nav-link text-dark" to='/addshops'>Add</Link></NavDropdown.Item>
               </NavDropdown>

                            </li>
                            <li className="nav-item dropdown">
                            <NavDropdown title="Products" id="navbarScrollingDropdown">
                <NavDropdown.Item ><Link className="nav-link text-dark" to='/products'>View</Link></NavDropdown.Item>
                <NavDropdown.Item ><Link className="nav-link text-dark" to='/addproducts'>Add</Link></NavDropdown.Item>
               </NavDropdown>

                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/purchases">
                                Purchases
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/addpurchase">
                               Add Purchases
                                </Link>
                            </li>
                           
                        </ul>
                        </div>

                </nav>
            );
        }
}
export default Nav;