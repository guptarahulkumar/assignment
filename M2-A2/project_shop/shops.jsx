import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class Shops extends Component{
    state={
        data:{},
}
    async fetchData(){
        let response =await http.get(`/shops`);
        console.log(response);
        let {data}=response;
        this.setState({data:data});
    }
     componentDidMount(){
         this.fetchData();   
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
             this.fetchData();
        }
    }
    sort=(num)=>{
        console.log("sort");
        let s1 ={...this.state};
        if(num===1){
            s1.data.shops.sort((p1,p2)=>{return (+p1.shopId)-(+p2.shopId)});
        }
        if(num===2){
            s1.data.shops.sort((p1,p2)=>{return p1.name.localeCompare(p2.name)});
        }
        this.setState(s1);
    }
    render(){
        let {data}=this.state;
        console.log(data.shops);
       let {shops=[]}= data;
     return (
        <div className="container-fluid bg-info">
                         <h3 className="text-center mx-5">Shops Table</h3>
                         <div className="row bg-dark text-light mx-5 text-center border ">
                            <div className="col-3 border border-danger"onClick={()=>this.sort(1)}>Shop Id</div>
                            <div className="col-3 border border-danger" onClick={()=>this.sort(2)}>Shop Name</div>
                            <div className="col-3 border border-danger"></div>
                            <div className="col-3 border border-danger"></div>
                            </div>
                            {shops.map(pr=>
                        <div className="row mx-5 text-center bg-light">
                            <div className="col-3 border border-dark">{pr.shopId}</div>
                            <div className="col-3 border border-dark">{pr.name}</div>  
                            <div className="col-3 border border-dark">
                            <Link className="btn btn-secondary btn-sm m-1" to={`/purchases/shops/${pr.shopId}`}>Purchases</Link>
                            </div>                           
                             <div className="col-3 border border-dark">
                            <Link className="btn btn-success btn-sm m-1" to={`/totalpurchases/shop/${pr.shopId}`}>Total Purchases</Link></div>   
                            </div>
                )}       
                        
     </div>
     );
    }
}
export default Shops;