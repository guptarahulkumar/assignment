import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class AddShops extends Component{
    state={
        shop:{name:"",rent:""},
    }
    
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        s1.shop[input.name] = input.value;
        this.setState(s1);
    };
    async postData(url,obj){
        let response = await http.post(url,obj);
        console.log(response);
        this.props.history.push("/shops");
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        const {shop} = this.state;
        this.postData("/addshops",shop);
    };
   


    render(){
      let {name,rent}=this.state.shop;
      return (
            <div className="container">
               <div className="form-group">
                   <label>Shop Name</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="name"
                   placeholder="Enter Shop Name"
                   value ={name}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group">
                   <label>Rent</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="rent"
                   placeholder="Enter Rent"
                   value ={rent}
                   onChange={this.handleChange}
                   />
               </div>
               <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                
            </div>
        );

    }
}
export default AddShops;