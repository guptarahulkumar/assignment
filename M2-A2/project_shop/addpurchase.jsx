import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class AddPurchase extends Component{
    state={
        purchase:{shopId:"",productid:"",quantity:"",price:""},
    }
    
    handleChange=(e)=>{
        const {currentTarget : input}= e;
        let s1 = {...this.state};
        s1.purchase[input.name] = input.value;
        this.setState(s1);
    };
    async postData(url,obj){
        let response = await http.post(url,obj);
        console.log(response);
        this.props.history.push("/purchases");
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        const {purchase} = this.state;
        this.postData("/addpurchase",purchase);
    };
   


    render(){
      let {shopId,productid,quantity,price}=this.state.purchase;
      return (
            <div className="container">
               <div className="form-group">
                   <label>Shop Id</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="shopId"
                   placeholder="Enter Shop Id"
                   value ={shopId}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group">
                   <label>Product Id</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="productid"
                   placeholder="Enter Product Id"
                   value ={productid}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group">
                   <label>Quantity</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="quantity"
                   placeholder="Enter Quantity"
                   value ={quantity}
                   onChange={this.handleChange}
                   />
               </div>
               <div className="form-group">
                   <label>Price</label>
                   <input 
                   type="text"
                   className="form-control"
                   name="price"
                   placeholder="Enter Price"
                   value ={price}
                   onChange={this.handleChange}
                   />
               </div>
               <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                
            </div>
        );

    }
}
export default AddPurchase;