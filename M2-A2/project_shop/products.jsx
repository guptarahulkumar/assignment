import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class Products extends Component{
    state={
        data:{},
}
    async fetchData(){
        let response =await http.get(`/products`);
        console.log(response);
        let {data}=response;
        this.setState({data:data});
    }
     componentDidMount(){
         this.fetchData();   
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
             this.fetchData();
        }
    }
    sort=(num)=>{
        console.log("sort");
        let s1 ={...this.state};
        if(num===1){
            s1.data.products.sort((p1,p2)=>{return (+p1.productId)-(+p2.productId)});
        }
        if(num===2){
            s1.data.products.sort((p1,p2)=>{return p1.productName.localeCompare(p2.productName)});
        }
        if(num===3){
            s1.data.products.sort((p1,p2)=>{return p1.category.localeCompare(p2.category)});
        }
        if(num===4){
            s1.data.products.sort((p1,p2)=>{return p1.description.localeCompare(p2.description)});
        }
        this.setState(s1);
    }
    render(){
        let {data}=this.state;
        console.log(data.products);
       let {products=[]}= data;
     return (
        <div className="container-fluid bg-info">
                         <h3 className="text-center mx-5">Products Table</h3>
                         <div className="row bg-dark text-light mx-5 text-center border m-1 ">
                            <div className="col-1 border border-danger"onClick={()=>this.sort(1)}>Product Id</div>
                            <div className="col-2 border border-danger" onClick={()=>this.sort(2)}>Product Name</div>
                            <div className="col-2 border border-danger"onClick={()=>this.sort(3)}>Category</div>
                            <div className="col-2 border border-danger" onClick={()=>this.sort(4)}>Description</div>
                            <div className="col-1 border border-danger"></div>
                            <div className="col-2 border border-danger"></div>
                            <div className="col-2 border border-danger"></div>
                            </div>
                            {products.map(pr=>
                        <div className="row mx-5 text-center bg-light">
                            <div className="col-1 border border-dark">{pr.productId}</div>
                            <div className="col-2 border border-dark">{pr.productName}</div>     
                            <div className="col-2 border border-dark">{pr.category}</div>
                            <div className="col-2 border border-dark">{pr.description}</div>
                            <div className="col-1 border border-dark">
                            <Link className="btn btn-warning btn-sm m-1" to={`/products/${pr.productId}/edit`}>Edit</Link>
                            </div>
                            <div className="col-2 border border-dark">
                            <Link className="btn btn-secondary btn-sm m-1" to={`/purchases/products/${pr.productId}`}>Purchases</Link>
                            </div>
                            <div className="col-2 border border-dark">   
                            <Link className="btn btn-success btn-sm m-1" to={`/totalpurchases/product/${pr.productId}`}>Total Purchases</Link>
                                </div>
                            </div>
                )}       
                        
     </div>
     );
    }
}
export default Products;