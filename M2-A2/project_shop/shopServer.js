const { query } = require("express");
let express = require("express");
let app=express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header(
        "Access-Control-Allow-Methods",
        "GET,POST,OPTIONS,PUT,PATCH,DELETE,HEAD"
    );
    res.header(
        "Access-Control-Allow-Headers",
        "Origin,X-Requested-With,Content-Type,Accept"
    );
    next();
});
let mysql = require("mysql");
let connData={
    host : "localhost",
    user : "root",
    password:"",
    database :"testdb",
};
const port = 2410;
app.listen(port,()=>console.log(`Node app listening on port ${port}! `));

let {shopData} = require("./shopData.js");

app.get("/shops",function(req,res){
    let arr1 = shopData;
    console.log(arr1);
    res.send(arr1);
});
app.get("/shops/:shopId",function(req,res){
    let shopId = req.params.shopId;
    let shop  =shopData.shops.find((st)=>st.shopId==shopId);
    if(shop) res.send(shop);
    else  res.status(404).send("No shop found");
});
app.post("/addshops",function(req,res){
    let body = req.body;
    console.log(body);
    let maxid = shopData.shops.reduce((acc,curr)=>(curr.shopId>=acc?curr.shopId:acc),0);
    let newid= maxid +1;
    let newShop = {shopId:newid,...body};
    shopData.shops.push(newShop);
    res.send(newShop);

});
app.get("/products",function(req,res){
    let arr1 = shopData;
    console.log(arr1);
    res.send(arr1);
});
app.get("/products/:productId",function(req,res){
    let productId = req.params.productId;
    let product  =shopData.products.find((st)=>st.productId==productId);
    if(product) res.send(product);
    else  res.status(404).send("No product found");
});
app.post("/addproducts",function(req,res){
    let body = req.body;
    console.log(body);
    let maxid = shopData.products.reduce((acc,curr)=>(curr.productId>=acc?curr.productId:acc),0);
    let newid= maxid +1;
    let newproduct = {productId:newid,...body};
    shopData.products.push(newproduct);
    res.send(newproduct);

});
app.put("/products/:productId",function(req,res){
    let productId= req.params.productId;
    let body = req.body;
    console.log(body);
    let index = shopData.products.findIndex((st)=>st.productId==productId);
    if(index>=0){
    let updatedproduct = {...body};
    shopData.products[index]=updatedproduct;
    res.send(updatedproduct);
    }else
    res.status(404).send("No Product found");

});
app.get("/purchases",function(req,res){
    console.log("GET /purchases",req.query);
    let shop = req.query.shop;
    let product= req.query.product;
    let sortby = req.query.sortby;
    let arr1 = shopData.purchases;
   console.log(shop,product,sortby);
   if(shop){
    let shopArr=shop.split(",");
    arr1 =arr1.filter((st)=>shopArr.find((c1)=>c1==st.shopId));
}
   if(product){
    let productArr=product.split(",");
    arr1 =arr1.filter((st)=>productArr.find((c1)=>c1==st.productid));
  }
  if(sortby=="QtyAsc"){
    arr1= arr1.sort((p1,p2)=>{return (+p1.quantity)-(+p2.quantity)});
  }
  if(sortby=="QtyDesc"){
    arr1= arr1.sort((p1,p2)=>{return (+p2.quantity)-(+p1.quantity)});
  }
  if(sortby=="ValueAsc"){
    arr1= arr1.sort((p1,p2)=>{
        let value1 = parseInt(p1.quantity)*parseInt(p1.price);
        let value2 = parseInt(p2.quantity)*parseInt(p2.price);
        return (+p1.value1)-(+p2.value2)});
  }
  if(sortby=="ValueDesc"){
    arr1= arr1.sort((p1,p2)=>{
        let value1 = parseInt(p1.quantity)*parseInt(p1.price);
        let value2 = parseInt(p2.quantity)*parseInt(p2.price);
        return (+p2.value2)-(+p1.value1)});
  }
  console.log(arr1);
    res.send(arr1);
});

app.get("/purchases/shops/:shopId",function(req,res){
    let shopId= req.params.shopId;
    console.log(shopId);
    let arr1 = shopData.purchases;
    const arr2 =arr1.filter(pr=>{return pr.shopId==shopId});
    console.log(arr2);
    res.send(arr2);
});
app.get("/purchases/products/:productId",function(req,res){
    let productId= req.params.productId;
    console.log(productId);
    let arr1 = shopData.purchases;
    const arr2 =arr1.filter(pr=>{return pr.productid==productId});
    console.log(arr2);
    res.send(arr2);
});

app.get("/totalpurchases/shop/:shopId",function(req,res){
    let shopId= req.params.shopId;
    //console.log(shopId);
    let arr1 = shopData.purchases;
    const arr2 =arr1.filter(pr=>{return pr.shopId==shopId});
    //console.log(arr2);
    let arr3=[];
    arr2.map(pr=>{
        let arrFilter=arr3.filter(opt=>{return opt.id==pr.productid});
        if(arrFilter.length===0){
        let arr=arr2.filter(opt=>{return opt.productid==pr.productid});
        //console.log(arr);
        let sum = arr.reduce((acc,curr)=>{return acc+curr.quantity},0);
        //console.log(arr,sum);
        let json={};
        json.id=pr.productid;
        json.total =sum;
        json.value=sum*pr.price;
        arr3.push(json);
        }
    });
    console.log(arr3);
    res.send(arr3);
});
app.get("/totalpurchases/product/:productId",function(req,res){
    let productId= req.params.productId;
    //console.log(productId);
    let arr1 = shopData.purchases;
    const arr2 =arr1.filter(pr=>{return pr.productid==productId});
    //console.log(arr2);
    let arr3=[];
    arr2.map(pr=>{
        let arrFilter=arr3.filter(opt=>{return opt.id==pr.shopId});
        if(arrFilter.length===0){
        let arr=arr2.filter(opt=>{return opt.shopId==pr.shopId});
        //console.log(arr);
        let sum = arr.reduce((acc,curr)=>{return acc+curr.quantity},0);
        //console.log(arr,sum);
        let json={};
        json.id=pr.shopId;
        json.total =sum;
        json.value=sum*pr.price;
        arr3.push(json);
        }
    });
    console.log(arr3);
    res.send(arr3);
});

app.post("/addpurchase",function(req,res){
    let body = req.body;
    console.log(body);
    let maxid = shopData.purchases.reduce((acc,curr)=>(curr.purchaseId>=acc?curr.purchaseId:acc),0);
    let newid= maxid +1;
    console.log(newid);
    let newpurchase = {purchaseId:newid,...body};
    shopData.purchases.push(newpurchase);
    res.send(newpurchase);

});

app.put("/shops/:empCode",function(req,res){
    let empCode= req.params.empCode;
    let body = req.body;
    console.log(body);
    let index = shopsData.findIndex((st)=>st.empCode==empCode);
    if(index>=0){
    let updatedshop = {...body};
    shopsData[index]=updatedshop;
    res.send(updatedshop);
    }else
    res.status(404).send("No shop found");

});

app.delete("/shops/:empCode",function(req,res){
    let empCode= req.params.empCode;
    console.log(empCode,shopsData);
    let index= shopsData.findIndex(pr=>pr.empCode==empCode);
    console.log(index);
    if(index>=0){
    let deletedshop = shopsData.splice(index,1);
    res.send(deletedshop);
    }
    else res.status(404).send("No shop found");
});
