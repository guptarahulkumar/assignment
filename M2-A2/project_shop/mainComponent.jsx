import React,{Component} from "react";
import {Route,Switch,Redirect } from"react-router-dom";
import Nav from "./navbar";
import AddShops from "./addShops";
import Shops from "./shops";
import AddProducts from "./addproducts";
import AddPurchase from "./addpurchase";
import Products from "./products";
import Purchases from "./purchases";
import ShopPurchases from "./shopPurchases";
import ProductPurchases from "./productPurchases";
import TotalShopPurchases from "./totalPurchaseShop";
import TotalProductPurchases from "./totalPurchaseProduct";
import Home from "./home";

class MainComponent extends Component{
    render(){
        return(
           <React.Fragment>
                <Nav />
                <Switch>
                <Route path="/addshops" component={AddShops}/> 
                <Route path="/shops" component={Shops}/> 
                <Route path="/products/:productId/edit" component={AddProducts}/> 
                <Route path="/addproducts" component={AddProducts}/> 
                <Route path="/addpurchase" component={AddPurchase}/> 
                <Route path="/products" component={Products}/> 
                <Route path="/purchases/shops/:shopId" component={ShopPurchases}/>
                <Route path="/totalpurchases/shop/:shopId" component={TotalShopPurchases}/>
                <Route path="/totalpurchases/product/:productId" component={TotalProductPurchases}/>
                <Route path="/purchases/products/:productId" component={ProductPurchases}/>
                <Route path="/purchases" component={Purchases}/> 
                <Route path="/home" component={Home}/>
                    <Redirect from="/" to="/home"/>
                  
                </Switch>
                </React.Fragment>
           
        );
       
    }
}
export default MainComponent;