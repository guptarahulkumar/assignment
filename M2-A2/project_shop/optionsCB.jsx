import React,{Component} from "react";
class OptionsCB extends Component{
    handleChange = (e)=>{
        const {currentTarget  : input}=e;
        let options = {...this.props.options};
        if(input.name==="product")
        options[input.name]=this.updateCBs(
            options[input.name],
            input.checked,
            input.value
        );
        else options[input.name] = input.value;
        console.log("OptionsCB",options);
        this.props.onOptionChange(options);
    }
   
    updateCBs=(inpValues,checked,value)=>{
        let inpArr = inpValues ? inpValues.split(","):[];
        if(checked) inpArr.push(value);
        else{
            let index = inpArr.findIndex((ele)=>ele === value);
            if(index>=0) inpArr.splice(index,1);
        }
        console.log(inpValues,inpArr);
        return inpArr.join(",");
    }
    makeDD=(arr,name,label)=>(
        <React.Fragment>
        <div className="row border mt-3">
        <div className="col bg-dark text-light p-2 " >
         {label}
            </div>
        </div>
        <div className="row mt-3">
               <div  className="form-group">
                <select className="form-control" name ={name} id={name} onChange={this.handleChange}>
                <option className="bg-dark text-light p-2 ">{label}</option>
               {arr.map((opt)=>(
                  <option className="bg-dark text-light p-2 " value={opt.value}>{opt.show}</option>
               ))}
               </select>
       </div>
       </div>
       </React.Fragment>
    );
    makeRadio1=(arr,values,name,label)=>(
        <React.Fragment>
             <div className="row border mt-3">
               <div className="col bg-dark text-light p-2 " >
                {label}
                   </div>
               </div>
               <div className="row ">
            {arr.map((opt)=>(
                <div className="col-12  border p-2">
                <div className="form-check ">
                    <input
                    className="form-check-input "
                    value={opt.value}
                    type="checkbox"
                    name={name}
                    checked={values.find((val)=>val===opt)}
                    onChange={this.handleChange}
                    />
                    <label className="form-check-label" style={{textTransform:"capitalize"}}>{opt.show}</label>
                </div>
                </div>
            ))}
    </div>
    </React.Fragment>
    );
    
    render(){
       
        let {products,shops,sort,onOptionChange}=this.props;
        let {product="",shop="",sort1=""}=this.props.options;
        return(
            <div className="row">
                 <div className="col m-2">
                {this.makeDD(shops,"shop"," Select Shop")}
                </div>
                <div className="col-12 m-2">
                {this.makeRadio1(products,product.split(","),"product","Product")}
                </div>
               
                <div className="col m-2">
                {this.makeDD(sort,"sort"," Select Sort")}
                </div>
            </div>
        );
    }
};
export default OptionsCB;
