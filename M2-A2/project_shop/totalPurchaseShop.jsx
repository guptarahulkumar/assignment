import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "./http";
class TotalShopPurchases extends Component{
    state={
        data:[],
}
    async fetchData(){
       const {shopId}=this.props.match.params;
        let response =await http.get(`/totalpurchases/shop/${shopId}`);
        console.log(response);
        let {data}=response;
        this.setState({data:data});
    }
     componentDidMount(){
         this.fetchData();
        
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
             this.fetchData();
        }
    }
  

    sort=(num)=>{
        console.log("sort");
        let s1 ={...this.state};
        if(num===1){
            s1.data.sort((p1,p2)=>{return (+p1.id)-(+p2.id)});
        }
        if(num===2){
            s1.data.sort((p1,p2)=>{return (+p1.total)-(+p2.total)});
        }
        if(num===3){
            s1.data.sort((p1,p2)=>{return (+p1.value)-(+p2.value)});
        }
        this.setState(s1);
    }
    render(){
        let {data}=this.state;
        const {shopId}=this.props.match.params;
        return (
        <div className="container-fluid bg-warning ">
                         <h3 className="text-center mx-5"> Total Purchases Table of Shop {shopId} </h3>
                         <div className="row m-2 text-center bg-dark text-light border ">
                            <div className="col-4 border border-danger" onClick={()=>this.sort(1)}>Product Id</div>
                            <div className="col-4 border border-danger" onClick={()=>this.sort(2)}>Quantity</div>
                            <div className="col-4 border border-danger" onClick={()=>this.sort(3)}>Value</div>
                           
                            </div>
                        {data.map(pr=>
                        <div className="row m-1 bg-light text-center">
                            <div className="col-4 border border-dark">{pr.id}</div>
                            <div className="col-4 border border-dark">{pr.total}</div>
                            <div className="col-4 border border-dark">{pr.value}</div>
                            
                            </div>
                   
                )} 
     </div>
     );
    }
}
export default TotalShopPurchases;