import React from "react";
import { Component } from "react";
class TaskForm extends Component{
    state={
        product : this.props.product,
        errors:{},
        brands:this.props.brands,
    };
    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let s1 = {...this.state};
        s1.product[input.name]= input.value;
        this.setState(s1);
    }
    handleChange2=(e)=>{
        const {currentTarget:input}=e;
        let s1 = {...this.state};
       s1.product[input.name]=input.value;
       this.setState(s1);
        
    }
    handleBrand=()=>{
        let s1 = {...this.state};
        if(s1.product.category==='Food'){
            s1.brands = ["Nestle", "Haldiram", "Pepsi", "Coca Cola", "Britannia", "Cadburys",];
        }else if(s1.product.category==='Personal Care'){
            s1.brands = ["P&G", "Colgate", "Parachute",
            "Gillete", "Dove",];
        }else{
            s1.brands = ["Levis", "Van Heusen", "Manyavaar", "Zara"];
        }
        console.log(s1.brands);
        this.setState(s1);
    }
    handleChange1=(e)=>{
        
        const {currentTarget:input}=e;
        let s1 = {...this.state};
        s1.product[input.name]= input.checked;
        this.setState(s1);
    }
    validateAll=()=>{
        let {code,price}=this.state.product;
        let errors ={};
        errors.code = this.validateCode(code);
        errors.price = this.validatePrice(price);
        return errors;
    }
    handleSubmit=(e)=>{
        e.preventDefault();
          this.props.onSubmit(this.state.product);
    }
    handleValidate=(e)=>{
        let {currentTarget:input}=e;
        let s1 = {...this.state};
        switch(input.name){
            case "code":
                s1.errors.code = this.validateCode(input.value);
                break;
                case "price":
                s1.errors.price = this.validatePrice(input.value);
                break;
                default:
                break;
        }
        this.setState(s1);
    }
    isFormValid=()=>{
        let errors = this.validateAll();
    return this.isValid(errors);   
 }
 isValid=(errors)=>{
    let keys = Object.keys(errors);
    let count = keys.reduce((acc,curr)=>(errors[curr]?acc+1:acc),0);
    return count === 0;
}
    validateCode=(code)=>
    !code?"Code Must be Entered":"";
validatePrice=(price)=>
    !price?"Price Must be Entered":price>0?"":"Price must be greater 0.";

    render(){
        let {onBack,prod,onChangeBrand,product,onSubmit} = this.props;
        console.log(product);
        const {code,brand,category,price,specialOffer,limitedStock,quantity}=product;
        let{errors,brands}=this.state;
        return (
           <React.Fragment>
               <h5>{this.props.edit?"Edit Details of Product":"Enter  Details of Product"}</h5>
                <div className="form-group">
                    <label>Product Code</label>
                    <input type="text" className="form-control" id="code" name="code"  value={code} onChange={this.handleChange} onBlur={this.handleValidate}
                   />
                   {errors.code ? <span className="text-danger">{errors.code}</span> :""}
                </div>
                <div className="form-group">
                    <label>Price</label>
                    <input type="number" className="form-control" id="price" name="price" value={price} onChange={this.handleChange} onBlur={this.handleValidate}/>
                    {errors.price ? <span className="text-danger">{errors.price}</span> :""}
                </div>

                <div className="form-check-group">
                    <label>Category</label><br/>
                    <input type="radio" className="form-check-control m-2" name="category" value="Food"   onChange={this.handleChange2}  checked={category === "Food"?"checked":""}/>Food
                    <input type="radio" className="form-check-control m-2"  name="category" value="Personal Care"  onChange={this.handleChange2}   checked={category === "Personal Care"}/>Personal Care 
                    <input type="radio" className="form-check-control m-2" name="category" value="Apparel"  onChange={this.handleChange2} checked={category === "Apparel"}/>Apparel
                </div>


                <div  className="form-group"> 
                    <select className="form-control" name ="brand" value={brand} onChange={this.handleChange} onMouseOver={this.handleBrand}>
                        <option value="">Select Brand</option>
                        {brands.map((pr)=>(
                         <option>{pr}</option>
                        ))}
                    </select>
                    <small>Choose other info about the product</small>
                </div>
                <div className="form-check-group">
                    <input type="checkbox" className="form-check-control m-2" name="specialOffer" onChange={this.handleChange1} checked={specialOffer === true}/>Special Offer<br/>
                    <input type="checkbox" className="form-check-control m-2" name="limitedStock" onChange={this.handleChange1} checked={limitedStock === true}/>Limited Stock
                </div>
                <button className="btn btn-primary m-2" onClick={this.handleSubmit} disabled={!this.isFormValid()} >
                    {this.props.edit?"Edit":"Add Product"}</button>
                    <br/>
                    <button className="btn btn-primary m-2" onClick={()=>onBack()}>Go Back to Home Page</button>
                    </React.Fragment>
        );
    }
}
export default TaskForm