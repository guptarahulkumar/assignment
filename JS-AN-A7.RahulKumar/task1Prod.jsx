import React,{Component} from "react";
class TaskProd extends Component{
    
    render(){
        const {prod,onEditProduct} = this.props;
        return(
            <React.Fragment>
                {prod.map((opt,index)=>(
                 <div className="col-3 bg-light border text-dark text-center">
                        <p>Code : {opt.code}<br/>
                        Brand : {opt.brand}<br/>
                        Category : {opt.category}<br/>
                        Price : {opt.price}<br/>
                        Quantity : {opt.quantity}<br/>
                        SpecialOffer : {opt.specialOffer?"Yes":"No"}<br/>
                        Limited Stock : {opt.limitedStock?"Yes":"No"}<br/>
                        <button className="btn btn-warning" onClick={()=>onEditProduct(index)}>Edit Details</button>
                        </p>
                    </div>
                ))}
            </React.Fragment>
        );
    }

}
export default TaskProd;