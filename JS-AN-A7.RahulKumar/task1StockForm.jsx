import React from "react";
import { Component } from "react";
class TaskStockForm extends Component{
    state={
        stock : this.props.stock,
        year:this.props.year,
        month:this.props.month,
        date:this.props.date,
        dateLimit:31,
        errors:{},
    };
    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let s1 = {...this.state};
        s1.stock[input.name]= input.value;
        if(input.name==='month'){
            let index= s1.month.findIndex(opt=>{return input.value===opt});
            if(input.value==="February"){
                if(s1.stock.year%4===0){
                    s1.date=[];
                    s1.dateLimit=28;
                    console.log(s1.stock.year);
                }else{
                    s1.date=[];
                    s1.dateLimit=29;
                    console.log(s1.stock.year);
                }
            }else
            if(input.value==="August"){
            s1.date=[];
            s1.dateLimit=31;
        }
        else if(index%2===1){
                s1.date=[];
                s1.dateLimit=30;
            }else{
                s1.date=[];
                s1.dateLimit=31;
            }
        }
        
        this.setState(s1);
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        console.log(this.state.stock);
          this.props.onSubmit1(this.state.stock); 
    }
    handleYear=()=>{
        let s1={...this.state};
        for(let i=0; i<=126;i++){
        s1.year.push(i+1900);
        }
        s1.year.length=126;
     this.setState(s1);
    }
    handleMonth=()=>{
        let s1={...this.state};
        for(let i='January'; i<='December';i++){
            s1.month.push(i);
        }
        this.setState(s1);
    }
    handleDate=()=>{
        let s1={...this.state};
        for(let i=1; i<=s1.dateLimit;i++){
            s1.date.push(i);
        }
        this.setState(s1);
    }
    
    render(){
        let {onBack,prod,stock} = this.props;
        let{year,month,date}=this.state;
        const {code,quantity}=stock;
        return (
           <React.Fragment>
               <h3>Select the product whose stocks have been receieved</h3>
               <div  className="form-group">
                    <select className="form-control" name ="code" id="code" value={code}  onChange={this.handleChange}>
                        <option value="">Select Product Code</option>
                        {prod.map((pr)=>(
                         <option>{pr.code}</option>
                        ))}
                    </select>
                </div>
                <div className="form-group">
                    <label>Stocks Received</label>
                    <input type="number" className="form-control m-2" id="quantity" value={quantity} name="quantity" onChange={this.handleChange} />
                </div>
                
                <div  className="form-group m-2">
                <div className="row">
                    <div className="col-3">
                    <select className="form-control" name ="year" onMouseOver={this.handleYear} onChange={this.handleChange}>
                        <option value="">Select Year</option>
                        {year.map((pr)=>(
                         <option>{pr}</option>
                        ))}
                    </select>
                    </div>
                    <div className="col-3">
                    <select className="form-control" name ="month" onMouseOver={this.handleMonth} onChange={this.handleChange} >
                        <option value="">Select Month</option>
                        {month.map((pr)=>(
                         <option>{pr}</option>
                        ))}
                    </select>
                    </div>
                    <div className="col-3">
                    <select className="form-control" name ="date" onMouseOver={this.handleDate} onChange={this.handleChange}>
                        <option value="">Select Date</option>
                        {date.map((pr)=>(
                         <option>{pr}</option>
                        ))}
                        
                    </select>
                    </div>
                    </div>
                </div>
               
                <button className="btn btn-primary m-2" onClick={this.handleSubmit} >
                    Submit</button>
                    <br/>
                    <button className="btn btn-primary m-2" onClick={()=>onBack()}>Go Back to Home Page</button>
                    </React.Fragment>
        );
    }
}
export default TaskStockForm