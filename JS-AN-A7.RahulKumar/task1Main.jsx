import React,{Component} from "react";
import TaskNavBar from "./task1NavBar"
import TaskProd from "./task1Prod";
import TaskForm from "./task1Form";
import TaskStockForm from "./task1StockForm";
class TaskMain extends Component{
    state={
        prod:[
            {
            code: "PEP1253", price: 20, brand: "Pepsi", category: "Food",
            specialOffer: false, limitedStock: false, quantity: 25
            },
            {
            code: "MAGG021", price: 25, brand: "Nestle", category: "Food",
            specialOffer: true, limitedStock: true, quantity: 10
            },
            {
            code: "LEV501", price: 1000, brand: "Levis", category: "Apparel",
            specialOffer: true, limitedStock: true, quantity: 3
            },
            {
            code: "CLG281", price: 60, brand: "Colgate", category: "Personal Care",
            specialOffer: true, limitedStock: true, quantity: 5
            },
            {
            code: "MAGG451", price: 25, brand: "Nestle", category: "Food",
            specialOffer: true, limitedStock: true, quantity: 0
            },
            {
            code: "PAR250", price: 40, brand: "Parachute", category: "Personal Care",
            specialOffer: true, limitedStock: true, quantity: 5
            }
           ],
           brands:["Nestle", "Haldiram", "Pepsi", "Coca Cola", "Britannia", "Cadburys", "P&G", "Colgate", "Parachute",
           "Gillete", "Dove", "Levis", "Van Heusen", "Manyavaar", "Zara"],
           btn : false,
           btn1:true,
           btn2:false,
           year:[],
           month:["January","February","March","April","May","June","July","August","September","October","November","December"],
           date:[],
           view:0,
    editProdIndex:-1,
           
        
    };
    handleNewProduct=()=>{
        let s1 = {...this.state};
        s1.btn = true;
        s1.btn1=false;
        s1.btn2=false;
        this.setState(s1);
    }
    handleNewProduct1=()=>{
        let s1 = {...this.state};
        s1.btn = false;
        s1.btn1=false;
        s1.btn2=true;
        this.setState(s1);
    }
    handleBack=()=>{
        let s1 = {...this.state};
        s1.btn1 = true;
        s1.btn=false;
        s1.btn2=false;
        s1.editProdIndex=-1;
        this.setState(s1);
    }
    
    handleProduct=(product)=>{
        console.log("In Handle product",product);
        let s1 ={...this.state};
        s1.editProdIndex>=0?
        (s1.prod[s1.editProdIndex]=product)
        :s1.prod.push(product);
        s1.editProdIndex=-1;
        s1.btn1 = true;
        s1.btn=false;
        s1.btn2=false;
        this.setState(s1);
    }
    handleStock=(stock)=>{
        console.log("In Handle product",stock);
        let s1 ={...this.state};
       const find =s1.prod.find(opt=>{return opt.code===stock.code});
       let findIndex =s1.prod.findIndex(opt=>{return opt.code===stock.code});
       s1.prod[findIndex].quantity = parseInt(s1.prod[findIndex].quantity)+parseInt(stock.quantity);
        s1.btn1 = true;
        s1.btn=false;
        s1.btn2=false;
        this.setState(s1);
    }
    editProduct=(index)=>{
        let s1 = {...this.state};
        s1.btn = true;
        s1.btn1=false;
        s1.btn2=false;
        s1.editProdIndex = index;
        console.log(s1.editProdIndex);
        this.setState(s1);
    }
  
    render(){
        let product =  {code:"",price:"",brand:"",category:"",specialOffer:false,limitedStock:false,quantity:"0"};
        let stock = {code:"",quantity:"",year:0};
        const {prod,btn,btn1,brands,btn2,year,month,date,editProdIndex,view} = this.state;
      
        return(
            <div className="container">
            <TaskNavBar prod={prod} />
            <div className="row m-3">
            {btn1?<TaskProd prod={prod} onEditProduct={this.editProduct}/>:""}
            </div>
            {btn? <TaskForm onBack={this.handleBack} brands={brands} prod={prod} 
            product={editProdIndex>=0?prod[editProdIndex]: product}
            onSubmit={this.handleProduct}
            edit = {editProdIndex>=0}/>:""}
            {btn2? <TaskStockForm onBack={this.handleBack} stock={stock}
            onSubmit1={this.handleStock}
             prod={prod}  year={year} month={month} date={date}/>:""}
            
            {btn1?
            <React.Fragment>
            <button className="btn btn-primary m-2" onClick={()=>this.handleNewProduct()}>Add New Product</button>
            <button className="btn btn-primary" onClick={()=>this.handleNewProduct1()}>Receive Stocks</button>
            </React.Fragment>:""}
            </div>
        );
    }
}
export default TaskMain;