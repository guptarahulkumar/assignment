import React,{Component} from "react";
class TaskNavBar extends Component{
render(){
let {prod} = this.props;
    return(
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <a className="navbar-brand text-light" href="#">
               <h3 className="m-2">ProdStoreSys</h3>
            </a>
            <div className="" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <a className="nav-link" href="#">
                            Products 
                            <span className="badge badge-pill rounded-circle badge-primary text-white  bg-secondary">{prod.length}</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">
                            Quantity 
                            <span className="badge badge-pill rounded-circle badge-primary text-white  bg-secondary">{prod.reduce((acc,curr)=>{return acc+curr.quantity},0)}</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">
                            Value 
                            <span className="badge badge-pill rounded-circle badge-primary text-white bg-secondary">{prod.reduce((acc,curr)=>{return acc+curr.price*curr.quantity},0)}</span>
                        </a>
                    </li>
                    
                    
                </ul>
            </div>
        </nav>
    );
}
}
export default TaskNavBar;