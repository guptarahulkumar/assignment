import React,{Component} from "react";
import {Route,Switch,Redirect ,Link} from"react-router-dom";
import queryString from "query-string";
import {Carousel} from 'react-bootstrap';
import auth from "./auth";
import http from "./http";
import Caurosel from "./caurosel";
import Navbar from "./navbar";
import Navbar1 from "./navbar1";
import LeftPanel from "./leftPanel";
class Home extends Component{
    state={
      movies:[],
      languages:["Hindi","English","Punjabi","Tamil"],
      formats:["2D","3D","4DX"],
      genres:["Action","Adventure","Biography","Comedy"],
      formBtn:"",
    }
    async fetchData(){
        let queryParams = queryString.parse(this.props.location.search);
        let searchStr = this.makeSearchString(queryParams);
        const{city} = this.props.match.params;
        let response =await http.get(`/movies/${city}?${searchStr}`);
        let {data}=response;
        this.setState({movies:data});
    }
     componentDidMount(){
        this.fetchData();  
   }
   componentDidUpdate(prevProps,prevState){
       if(prevProps!==this.props)
            this.fetchData();
   }
   callURL=(url,options)=>{
    let searchStrings = this.makeSearchString(options);
    console.log(url,searchStrings);
    this.props.history.push({
        pathname:url,
        search:searchStrings,
    });
}
makeSearchString=(options)=>{
    let {lang,format,genre}= options;
    let searchStr = "";
    searchStr = this.addToQueryString (searchStr,"lang",lang);
    searchStr = this.addToQueryString (searchStr,"format",format);
    searchStr = this.addToQueryString (searchStr,"genre",genre);
    return searchStr;
    }
    addToQueryString=(str,paramName,paramValue)=>
    ((paramValue)
    ?((str)
        ? (`${str}&${paramName}=${paramValue}`)
        :(`${paramName}=${paramValue}`))
    :(str));
    
    handleOptionChange=(options)=>{
        console.log(options);
        const{city} = this.props.match.params;
        this.callURL(`/movies/${city}`,options);

    }
    handleForm=()=>{
        let s1= {...this.state};
        s1.formBtn=true;
        console.log(s1.formBtn);
        this.setState(s1);
    }
    render(){
        let queryParams = queryString.parse(this.props.location.search);
        const {movies,languages,formats,genres,formBtn}=this.state;
        const{city} = this.props.match.params;
        const user = auth.getUser();
        return(
            <React.Fragment>
                 <div className="container-fixed m-0">
                <Navbar user={user} formBtn={formBtn} onForm={this.handleForm}/>
                </div>
<Caurosel/>               
<Navbar1 />
<div className="container-fixed bg-light">
<div className="row">
    <div className="col-4">
    <div className="row">
    <div className="col-1"></div>
    <div className="col-11">
    <div className="card p-2" style={{width: "17rem"}} > 
                             
  <img src="https://i.ibb.co/Hry1kDH/17443322900502723126.jpg" className="img" alt="..." />
  </div>
  <LeftPanel options={queryParams} languages={languages} formats={formats} genres={genres} onOptionChange={this.handleOptionChange}/>
  </div>
  </div>
    </div>
    <div className="col-7">
        <div className="row">
            {movies.map((p,index)=>(
                <div className="col-3 mx-4">
                      <React.Fragment>
                        <div className="card" style={{width: "14rem"}} >
                            <Link to={`/movies/${city}/${index}`}>      
  <img src={p.img} className="card-img-top rounded" alt="..."/></Link>
  </div>
  <div className="row bg-white">
      <div className="col-5">
  <i className="fas fa-heart text-danger "></i> <small>{p.rating}</small>
  </div>
  <div className="col-7"><small>{p.title}</small></div>
  </div>
  <div className="row bg-white">
      <div className="col-5">
  <small className="text-muted">{p.votes}votes</small>
  </div>
  <div className="col-7"> <small className="text-muted">{p.desc}</small></div>
  </div>

                   </React.Fragment>
                    </div>
            ))}
            </div>
    </div>
</div>
</div>
</React.Fragment>
        );
    }
}
export default Home;