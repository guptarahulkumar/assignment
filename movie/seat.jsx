import React,{Component} from "react";
import {Route,Switch,Redirect ,Link} from"react-router-dom";
import auth from "./auth";
import http from "./http";
class Seat extends Component{
    state={
        seatsData:[],
        movies:{},
        btn:false,
        tickets:{title:"",movieHall:"",tickets:[],amount:0,time:"",date:""},
    }
    async fetchData(){
        const {index,city,prIndex,qIndex,dat} = this.props.match.params;
        let response =await http.get(`/seats`);
        let response1 =await http.get(`/movies/${city}/${index}`);
        console.log(response);
        let {data}=response1;
        this.setState({seatsData:response.data,movies:data});
    }
     componentDidMount(){
        this.fetchData();  
   }
   g=()=>{
       let s1={...this.state};
       s1.tickets={title:"",movieHall:"",tickets:[],amount:0,time:"",date:""};
       this.setState(s1);
   }
   payBtn=()=>{
    let s1= {...this.state};
    const {index,city,prIndex,qIndex,dat} = this.props.match.params;
    s1.tickets.name=s1.movies.title;
    console.log(s1.movies.showTiming[dat][prIndex].name);
    s1.tickets.hall = s1.movies.showTiming[dat][prIndex].name;
    
   }
   btnShow=(row,seat,price)=>{
       let s1= {...this.state};
       const {index,city,prIndex,qIndex,dat} = this.props.match.params;
       if(!s1.tickets.tickets.find(p=>p==(row+seat))){
        s1.tickets.amount=parseInt(s1.tickets.amount)+price;
       s1.tickets.tickets.push(row+seat);
       console.log(s1.tickets);
       s1.btn=true;
       }else{
           s1.tickets.tickets.splice(s1.tickets.tickets.findIndex(p=>p==(row+seat)),1);
           s1.tickets.amount=parseInt(s1.tickets.amount)-price;
       }
       this.setState(s1);
   }
   btnShow1=(row,seat,price)=>{
    let s1= {...this.state};
    const {index,city,prIndex,qIndex,dat} = this.props.match.params;
       if(!s1.tickets.tickets.find(p=>p==(row+seat))){
        s1.tickets.amount=parseInt(s1.tickets.amount)+price;
       s1.tickets.tickets.push(row+seat);
       console.log(s1.tickets);
       s1.btn=true;
       }
       else{
        s1.tickets.tickets.splice(s1.tickets.tickets.findIndex(p=>p==(row+seat)),1);
        s1.tickets.amount=parseInt(s1.tickets.amount)-price;
    }
    this.setState(s1);
}
async postData(url,obj){
    console.log(obj);
    let response = await http.post(url,obj);
    this.props.history.push("/pay",response.data);
}
handleSubmit = (e)=>{
    e.preventDefault();
    let {tickets,movies}= this.state;
    const {index,city,prIndex,qIndex,dat} = this.props.match.params;
    tickets.title=movies.title;
    console.log(movies.showTiming[dat][prIndex].name);
    tickets.movieHall = movies.showTiming[dat][prIndex].name;
    tickets.time=movies.showTiming[dat][prIndex].timings[qIndex].name;
    tickets.date= parseInt(dat)+new Date().getDate()+" Dec";
    alert("Seat Booked Successfully");
    this.postData("/seat",tickets);
};
   render(){
    const {index,city,prIndex,qIndex,dat} = this.props.match.params;
    const {seatsData,movies,btn,tickets} = this.state;
    console.log(seatsData[dat],movies);
    const{seats=[]}=seatsData;
    const{seatList=[]}= seats;
    const d = new Date();
    return(
        <React.Fragment>
           <div className="container-fixed bg-dark">
            <h3 className="mx-3 pt-3 text-white">
                <Link to={`/movies/${city}/${index}`}><i className="fas fa-angle-left mx-4 text-white" style={{textDecoration:"none",}}></i></Link>
                {movies.title}</h3>
                {(movies.showTiming===undefined)?"":
        movies.showTiming[dat].map((qr,qrIndex)=>(
            <div className="container-fixed">
            {(prIndex==qrIndex)?(
                                <div className="row text-white">
                                    {console.log(qr.name)}
                                    <div className="col-6 mx-5 px-3">
                                        {qr.name}
                                        </div>
                                        </div>
                                 
            ):""}
            </div>
        ))}
            <p className="text-light btn" style={{position:"absolute",right:"50px",top:"20px"}}>
                <span className="btn text-light"> {tickets.tickets.length} tickets </span>
                <Link to={`/movies/${city}/${index}`}><button> x</button> </Link>
                </p>
            </div>
            <div className="container-fixed bg-light text-dark">
            {(movies.showTiming===undefined)?"":
        movies.showTiming[dat].map((qr,qrIndex)=>(
            <div className="container-fixed">
            {(prIndex==qrIndex)?(
                                <div className="row py-4">
                                    {console.log(qr.name)}
                                        {qr.timings.map((p,pIndex)=>(
                                            <div className="col">
                                      {(qIndex==pIndex)?
                                      <React.Fragment>
                                       {parseInt(d.getDate())+parseInt(dat)} Dec , 
                                          <Link className="btn btn-small border btn-success" 
                                          to={`/bookMovie/${city}/${index}/buyTicket/${qrIndex}/${pIndex}/${dat}?time=${parseInt(d.getDate())+parseInt(dat)}`} style={{fontSize:"12px"}}
                                          onClick={()=>this.g()}
                                          >
                                              {p.name}
                                              </Link> 
                                              </React.Fragment> 
                                      :<Link className="btn btn-small border border-success btn-outline-success " 
                                      to={`/bookMovie/${city}/${index}/buyTicket/${qrIndex}/${pIndex}/${dat}?time=${parseInt(d.getDate())+parseInt(dat)}`} style={{fontSize:"12px"}}
                                      onClick={()=>this.g()}
                                      >
                                          {p.name}
                                          </Link> }
                                        </div>)
                                        )}
                                        </div>
                                 
            ):""}
            </div>
        ))}
            </div>




            <div className="container bg-light">
                {console.log(seatsData)}
                RECLINER-Rs.420.00
                <hr/>
                {seatsData.map((s1,index)=>(
                      <React.Fragment>
                          {qIndex%3==index?s1.seats.map((s2)=>(
                              <React.Fragment>
                                   <div className="row">
                                    <div className="col-1">
                                        {(s2.price==420)?
                                        <p>{s2.rowName}</p>
                                        :""
                                        }
                                        </div>
                                        <div className="col-11">
                                  {s2.seatList.map((s3)=>(
                                      <span>
                                          {(s2.price==420)?((s3.seatNo==8)||(s3.seatNo==19))?
                                                 tickets.tickets.find(p=>p==(s2.rowName+s3.seatNo))?
                                                 <button className=" btn btn-sm border btn-primary" 
                                                 style={{fontSize:"10px",marginLeft:"100px"}} 
                                                 onClick={()=>this.btnShow1(s2.rowName,s3.seatNo,s2.price)}>
                                                     {s3.seatNo}
                                                     </button>
                                                 :<button className=" btn btn-sm border" 
                                                 style={{fontSize:"10px",marginLeft:"100px"}} 
                                                 onClick={()=>this.btnShow1(s2.rowName,s3.seatNo,s2.price)}>
                                                     {s3.seatNo}
                                                     </button>
                                                     :
                                                     tickets.tickets.find(p=>p==(s2.rowName+s3.seatNo))?
                                                 <button 
                                                 className=" btn btn-sm border btn-primary" 
                                                 style={{fontSize:"10px"}} 
                                                 onClick={()=>this.btnShow(s2.rowName,s3.seatNo,s2.price)}>
                                                     {s3.seatNo}
                                                     </button>
                                                 :<button 
                                                 className=" btn btn-sm border" 
                                                 style={{fontSize:"10px"}} 
                                                 onClick={()=>this.btnShow(s2.rowName,s3.seatNo,s2.price)}>
                                                     {s3.seatNo}
                                                     </button>
                                                 :""}
                                      </span>
                                  ))}
                                  </div>
                                  </div>
                                  </React.Fragment>
                          )):""}
                          </React.Fragment>
                  ))}
                  
                  GOLD-Rs.250.00
                  <hr/>
                  {seatsData.map((s1,index)=>(
                      <React.Fragment>
                          {qIndex%3==index?s1.seats.map((s2)=>(
                              <React.Fragment>
                                   <div className="row" >
                                    <div className="col-1">
                                        {(s2.price==250)?
                                        <p>{s2.rowName}</p>
                                        :""
                                        }
                                        </div>
                                        <div className="col-11">
                                  {s2.seatList.map((s3)=>(
                                      <span>
                                          {(s2.price==250)?((s3.seatNo==8)||(s3.seatNo==19))?
                                          tickets.tickets.find(p=>p==(s2.rowName+s3.seatNo))?
                                                 <button 
                                                 className=" btn btn-sm border btn-primary" 
                                                 style={{fontSize:"10px",marginLeft:"100px"}} 
                                                 onClick={()=>this.btnShow1(s2.rowName,s3.seatNo,s2.price)}>
                                                     {s3.seatNo}</button>
                                                 :
                                                 <button 
                                                 className=" btn btn-sm border" 
                                                 style={{fontSize:"10px",marginLeft:"100px"}} 
                                                 onClick={()=>this.btnShow1(s2.rowName,s3.seatNo,s2.price)}>
                                                     {s3.seatNo}</button>
                                                     :
                                                     tickets.tickets.find(p=>p==(s2.rowName+s3.seatNo))?
                                                 <button 
                                                 className=" btn btn-sm border btn-primary" 
                                                 style={{fontSize:"10px"}} 
                                                 onClick={()=>this.btnShow(s2.rowName,s3.seatNo,s2.price)}>
                                                     {s3.seatNo}
                                                     </button>:
                                                      <button 
                                                      className=" btn btn-sm border" 
                                                      style={{fontSize:"10px"}} 
                                                      onClick={()=>this.btnShow(s2.rowName,s3.seatNo,s2.price)}>
                                                          {s3.seatNo}
                                                          </button>
                                                 :""}
                                      </span>
                                  ))}
                                  </div>
                                  </div>
                                  </React.Fragment>
                          )):""}
                          </React.Fragment>
                  ))}
               
                
                
                
                
                <div className="row">
                                    {btn&&(tickets.amount!=0)?
                                    <React.Fragment>
                                    <button className="btn btn-fixed btn-primary px-4" onClick={this.handleSubmit}>Pay Rs. {tickets.amount}.00</button>
                                    </React.Fragment>
                                    :""}
                                    </div>
            </div>
            <center><span >
            <svg xmlSpace="preserve"
            enable-background="new 0 0 100 100"
            viewBox="0 0 260 20" y="0px" x="0px" 
            width="260px" height="20px" 
            xmlnsXlink="http://www.w3.org/1999/xlink" 
            xmlns="http://www.w3.org/2000/svg" 
            version="1.1" style={{fill: "rgba(0, 0, 0, 0.6)"}}>
                <g fill="none" fill-rule="evenodd" opacity=".3">
                    <g fill="#E1E8F1">
                        <path id="da" d="M27.1 0h205.8L260 14.02H0z"></path>
                    </g>
                    <path stroke="#4F91FF" stroke-width=".65" d="M27.19.33L1.34 13.7h257.32L232.81.32H27.2z"></path>
                    <path fill="#8FB9FF" d="M28.16 2.97h203.86l17.95 9.14H10.35z"></path>
                    <g fill="#E3ECFA"><path id="db" d="M0 13.88h260l-3.44 6.06H3.44z"></path></g>
                    <path stroke="#4F91FF" stroke-width=".65" d="M.56 14.2l3.075.41h252.74l3.07-5.4H.56z"></path></g>
                    </svg>
            </span>
            <p className="lead">All Eyes this way please!</p>
            </center>
            </React.Fragment>
    );
   }
}
export default Seat;