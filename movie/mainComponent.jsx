import React,{Component} from "react";
import {Route,Switch,Redirect } from"react-router-dom";
import Navbar from "./navbar";
import auth from "./auth";
import http from "./http";
import Seat from "./seat";
import Home from "./home";
import Book from "./book";
import Payment from "./pay";


class MainComponent extends Component{
    state={
      
    }
    render(){
        const user = auth.getUser();
        return(
            <React.Fragment>
                <Switch>
                <Route path="/bookMovie/:city/:index/buyTicket/:prIndex/:qIndex/:dat"
                    render={(props) => 
                    <Seat {...props}/>
                } 
                    /> 
                <Route path="/home/:city"
                    render={(props) => 
                    <Home {...props}/>} 
                    /> 
                    <Route path="/movies/:city/:index"
                    render={(props) => 
                    <Book {...props}/>} 
                    /> 
                    <Route path="/movies/:city"
                    render={(props) => 
                    <Home {...props}/>} 
                    /> 
                    <Route path="/pay"
                    render={(props) => 
                    <Payment {...props}/>} 
                    /> 

                    <Redirect to="/home/NCR"/>
            
                </Switch>
            </React.Fragment>
        );  
    }
}
export default MainComponent;