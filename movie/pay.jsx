import React,{Component} from "react";
import {Route,Switch,Redirect ,Link} from"react-router-dom";
import auth from "./auth";
import http from "./http";
class Payment extends Component{

   render(){
       console.log(this.props.location.state);
       const {state} = this.props.location;
       const {index,city,prIndex,qIndex,dat} = this.props.match.params;
       const d = new Date();
       return (
           <React.Fragment>
        <div className="container-fixed bg-dark">
        <h3 className="mx-3 pt-3 text-white">
            <Link 
            to={`/`}>
            <i className="fas fa-angle-left mx-4 text-white" style={{textDecoration:"none",}}></i></Link>
            {state.title}</h3>
                            <div className="row text-white">   
                                <div className="col-6 mx-5 px-3">
                                    {state.movieHall}
                                    </div>
                                    </div>
        <p className="text-light btn" style={{position:"absolute",right:"50px",top:"20px"}}>
            <Link to="/">
                <button> x</button> </Link>
            </p>
            </div>
            <div className="row" style={{backgroundColor:"#dbdbdb"}}>
                <div className="col-8 bg-light m-2">
                <img src="https://i.ibb.co/SK0HfNT/bookasmile-03.png" />
                </div>
                <div className="col-3 mr-2 mx-2 p-3 bg-white">
                    <p className="text-danger"> BOOKING SUMMARY</p>
                    <div className="row">
                <div className="col-6"><p>Movie </p></div>
                <div className="col-6"><p>{state.title} </p></div>
                </div>
                <div className="row">
                <div className="col-6"><p>Movie Hall </p></div>
                <div className="col-6"><p>{state.movieHall} </p></div>
                </div>
                <div className="row">
                <div className="col-6"><p>Total Tickets </p></div>
                <div className="col-6"><p>{state.tickets.length} </p></div>
                </div>
                <div className="row">
                <div className="col-6"><p>Tickets </p></div>
                <div className="col-6"><p>{state.tickets.map(p=>(<span> {p} </span>))} </p></div>
                </div>
                <div className="row">
                <div className="col-6"><p>Date </p></div>
                <div className="col-6"><p>{state.date} </p></div>
                </div>
                <div className="row">
                <div className="col-6"><p>Time</p></div>
                <div className="col-6"><p>{state.time} </p></div>
                </div>
                <div className="row" style={{backgroundColor:"wheat"}}>
                <div className="col-6"><p>Amount Paid </p></div>
                <div className="col-6"><p>Rs.{state.amount} </p></div>
                </div>
                <div className="row">
                <img src="https://i.ibb.co/CVHYxVK/images-q-tbn-ANd9-Gc-Qq-PT1-GB7-Cpvo3-WDDCi-Wt-Vto-Q-SLqp-Z9-B1x-D3-D69-WTj-MPyl-Chnd.png"/>
                    </div>
                    <div className="row">
                        <p style={{fontSize:"10px",textAlign:"justify"}}>You can cancel the tickets 4 hour(s) before the show. Refunds will be done according to Cancellation Policy.</p>
                        </div>
                </div>
            </div>
        </React.Fragment>

       );
   }
}
export default Payment;