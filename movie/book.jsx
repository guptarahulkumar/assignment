import React,{Component} from "react";
import {Route,Switch,Redirect ,Link} from"react-router-dom";
import Navbar from "./navbar";
import auth from "./auth";
import http from "./http";
class Book extends Component{
    state={
        movies:{},
        dat:0,
        filterBtn1:false,
        filterBtn2:false,
        priceArr : ["Rs. 101-200","Rs. 201-300","Rs. 300-350"],
        showArr:["Morning","Afternoon","Evening","Night"],
        options:{},
        heartArr:[],
    }
    async fetchData(){
        const {index} = this.props.match.params;
        const {city} = this.props.match.params;
        let response =await http.get(`/movies/${city}/${index}`);
        console.log(response);
        let {data}=response;
        this.setState({movies:data});
    }
     componentDidMount(){
        this.fetchData();  
   }
   handleChange = (e)=>{
    const {currentTarget  : input}=e;
    let options = {...this.state.options};
    options[input.name]=this.updateCBs(
        options[input.name],
        input.checked,
        input.value);
    console.log("OptionsCB",options);
    this.setState(options);
}

updateCBs=(inpValues,checked,value)=>{
    let inpArr = inpValues ? inpValues.split(","):[];
    if(checked) inpArr.push(value);
    else{
        let index = inpArr.findIndex((ele)=>ele === value);
        if(index>=0) inpArr.splice(index,1);
    }
    console.log(inpValues,inpArr);
    return inpArr.join(",");
}
   dateFunction=(dat)=>{
       let s1= {...this.state};
       s1.dat = dat;
       this.setState(s1);
   }
   filterPrice=()=>{
       this.setState({filterBtn1:true,filterBtn2:false});
   }
filterShow=()=>{
    this.setState({filterBtn2:true,filterBtn1:false});
}
filterShow1=()=>{
 this.setState({filterBtn2:false,filterBtn1:false});
}
makeCheckboxes=(arr,values,name)=>(
    <React.Fragment>
    <ul type="none" >
        {arr.map((p)=>(
            <li><input type="checkbox" 
            name={name} 
            
            value={p}
            checked={values.find((val)=>val===p)}
                    onChange={this.handleChange}/>{p}</li>
        ))
            }
    </ul>
    </React.Fragment>
);
heart=(prIndex)=>{
    let s1= {...this.state};
    prIndex++;
    if(!s1.heartArr.find(p=>p==prIndex)){
    s1.heartArr.push(prIndex);
    }else{
        s1.heartArr.splice(s1.heartArr.findIndex(p=>p==prIndex),1);
    }
    this.setState(s1);
}
    render(){
        const {movies,dat,filterBtn1,filterBtn2,priceArr,showArr,options,heartArr} = this.state;
        const{price="",show=""}= options;
        const {city} = this.props.match.params;
        //console.log(movies);
        const {index} = this.props.match.params;
        const{showTiming=[]}=movies;
        const d = new Date();
        const user = auth.getUser();
        return(
            <React.Fragment>
                 <div className="container-fixed m-0">
                <Navbar user={user} />
                </div>
            <div className="container-fixed bg-secondary "  onMouseOver={()=>this.filterShow1()}>
            <h3 className="mx-3 pt-3 text-white">{movies.title}</h3>
            <i className="fas fa-heart text-danger mx-3"></i>
            <span className="text-light" style={{fontSize:"20px",fontWeight:"bold"}}>{movies.rating}</span>
            <button className="btn border text-light mx-3 px-3 pt-0 pb-0" style={{borderRadius:"40%"}}>ACTION</button>
             <button className="btn border text-light mx-3 px-3 pt-0 pb-0" style={{borderRadius:"40%"}}>THRILLER</button>
             <br/>
             <small className="text-white mx-3">{movies.votes} votes</small>
            </div>
                <div className="row bg-light">
                    <div className="col-5 mt-2 mx-3">
                        <button className="btn btn-outline-success mx-2 p-1" onClick={()=>this.dateFunction(0)}>{d.getDate()} Today</button>
                        <button className="btn btn-outline-success mx-2 p-1" onClick={()=>this.dateFunction(1)}>{d.getDate()+1} Dec</button>
                        <button className="btn btn-outline-success mx-2 p-1" onClick={()=>this.dateFunction(2)}>{d.getDate()+2} Dec</button>
                        </div>
                        <div className="col-2 pt-3 pb-3 text-muted" onMouseOver={()=>this.filterPrice()} >
                            <small>Filter Price</small><i className="fas fa-angle-down text-dark"></i>
                            {filterBtn1?
                            (
                                <React.Fragment>
                                    {this.makeCheckboxes(priceArr,price.split(","),"price")}
                                    </React.Fragment>
                            )
                            :""}
                        </div>
                        <div className="col-2 pt-3 pb-3 text-muted" onMouseOver={()=>this.filterShow()} >
                         <small>Filter Showtime </small><i className="fas fa-angle-down text-dark"></i>
                         {filterBtn2?
                            (
                                <React.Fragment>
                                    {this.makeCheckboxes(showArr,show.split(","),"show")}
                                    </React.Fragment>
                            )
                           :"" }
                        </div>
                        <div className="col-3">
                        </div>
                </div>
                <div className="row " onMouseOver={()=>this.filterShow1()}>
                <div className="col-9 m-0">
                <div className="row">
            <div className="col-6 bordor " style={{backgroundColor:"pink"}}>
            <h6 className="m-2"><i className="fas fa-mobile-alt" style={{fontSize:"40px"}}></i></h6>
            <small>M-Ticket Available</small>
                </div>
                <div className="col-6 border" style={{backgroundColor:"pink"}}>
                <h6 className="m-2"><i className="fas fa-hamburger" style={{fontSize:"40px"}}></i></h6>
                <small>Food Available</small>
                </div>
                </div>
           
            {(movies.showTiming===undefined)?"":movies.showTiming[dat].map((pr,prIndex)=>(
               
                    <div className="row mt-1">
                        <div className="col-1 mx-3 mt-2" onClick={()=>this.heart(prIndex)}>
                            {heartArr.find(p=>p==prIndex+1)?
                        <i class="fas fa-heart text-danger"></i>
                        : <i class="far fa-heart"></i>
                            }
                            </div>
                            <div className="col-4">
                            <div className="row">
                            <div className="col-10">
                                <small><b>{pr.name}</b></small>
                                </div>
                                </div>
                                <div className="row">
                            <div className="col-3">
                                <i className="fas fa-mobile-alt text-success" ></i> <small style={{fontSize:"10px"}}>M-Ticket</small>
                                </div>
                                <div className="col-3">
                                <i className="fas fa-hamburger text-warning"></i> <small style={{fontSize:"10px"}}>F&B</small>
                                </div>
                                </div>
                                </div>
                                <div className="col-6">
                                    <div className="row">
                                    <div className="col-12">
                                       
                                       {pr.timings.map((q,qIndex)=>(
                                               <Link className="btn mx-1 btn-small border text-primary" to={`/bookMovie/${city}/${index}/buyTicket/${prIndex}/${qIndex}/${dat}?time=${parseInt(d.getDate())+parseInt(dat)}`} style={{fontSize:"12px"}}>{q.name}</Link>
                                       ))}
                                       <br/>
                                       <small className="text-danger m-0 p-0" style={{fontSize:"40px"}}>.</small><small style={{fontSize:"12px"}}>Cancellation available</small>
                                        </div>
                                    
                                   
                                        </div>
                                    </div>
                        </div>
                
            ))}
                </div>
                <div className="col-3">
                    <img src="https://i.ibb.co/JqbbCJz/1331654202504679967.jpg"/>
                </div>
            </div>
            </React.Fragment>
        );
    }
}
export default Book;