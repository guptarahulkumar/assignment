import React,{Component} from "react";
import {Link} from "react-router-dom";
class LeftPanel extends Component{
    handleChange = (e)=>{
        const {currentTarget  : input}=e;
        let options = {...this.props.options};
        options[input.name]=this.updateCBs(
            options[input.name],
            input.checked,
            input.value);
        console.log("OptionsCB",options);
        this.props.onOptionChange(options);
    }

    updateCBs=(inpValues,checked,value)=>{
        let inpArr = inpValues ? inpValues.split(","):[];
        if(checked) inpArr.push(value);
        else{
            let index = inpArr.findIndex((ele)=>ele === value);
            if(index>=0) inpArr.splice(index,1);
        }
        console.log(inpValues,inpArr);
        return inpArr.join(",");
    }
    makeCheckboxes=(arr,values,name,label)=>(
        <React.Fragment>
            <label className="form-check-label font-weight-bold">
                <h5>{label}</h5>
            </label>
            {arr.map((opt)=>(
                <div className="form-check mx-5">
                    <input
                    className="form-check-input "
                    value={opt}
                    type="checkbox"
                    name={name}
                    checked={values.find((val)=>val===opt)}
                    onChange={this.handleChange}
                    />
                    <label className="form-check-label">{opt}</label>
                </div>
            ))}
        </React.Fragment>
    );
    render(){
        let {lang="",format="",genre=""}=this.props.options;
        let {languages,formats,genres}=this.props;
        return(
            <div className="row">
                <div className="col-9">
                {this.makeCheckboxes(languages,lang.split(","),"lang","Language")}
                </div>
                <div className="col-9">
                {this.makeCheckboxes(formats,format.split(","),"format","Format")}
                </div>
                <div className="col-9">
                {this.makeCheckboxes(genres,genre.split(","),"genre","Genre")}
                </div>
            </div>
        );
    }
};
export default LeftPanel;
