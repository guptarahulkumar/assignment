import React,{Component} from "react";
import {Link} from "react-router-dom";
import {NavDropdown} from 'react-bootstrap';
class Navbar1 extends Component{
    render(){
       const {user} = this.props;
       return(
           <div className="container-fluid m-0">
        <nav className="navbar navbar-expand-sm navbar-light bg-light">
            <Link className="navbar-brand  " to="/home">
              <h3 className="m-2">Movies</h3>
            </Link>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link className="nav-link" to="/home">
                        Now Showing
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/home">
                        Coming Soon
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/home">
                        Exclusive
                        </Link>
                    </li>
                </ul>
            </div>

        </nav>
        </div>
    );
}
           
                        
        
}
export default Navbar1;